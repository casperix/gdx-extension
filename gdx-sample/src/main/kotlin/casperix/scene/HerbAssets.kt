package casperix.scene

import casperix.gdx.graphics.Texture
import casperix.math.axis_aligned.Box2d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.renderer.billboard.BillboardData
import casperix.renderer.billboard.BillboardShadowCastTextureAttribute
import casperix.renderer.core.DebugModeAttribute
import casperix.renderer.shadow.DiscardAlphaTestAttribute
import casperix.renderer.util.addAttribute
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

object HerbAssets {
	val herbBaseMaterial = Material(
		TextureAttribute.createDiffuse(Texture("herb_atlas_albedo_gray.png", true).apply { setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear) }),
		BillboardShadowCastTextureAttribute.create(Texture("herb_atlas_shadow.png", true).apply { setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear) }),
		TextureAttribute.createNormal(Texture("herb_atlas_normal.png", true).apply { setFilter(Texture.TextureFilter.MipMapLinearLinear, Texture.TextureFilter.Linear) }),
	)

	private val herbDebugMaterial = herbBaseMaterial
		.addAttribute(DebugModeAttribute(true))

	private val herbMaterial = herbBaseMaterial
		.addAttribute(BlendingAttribute(true, 1f))
		.addAttribute(DiscardAlphaTestAttribute(true, 0.02f))

	private val herbMaterial1 = herbMaterial
		.addAttribute(ColorAttribute.createDiffuse(Color.GREEN))


	private val herbMaterial2 = herbMaterial
		.addAttribute(ColorAttribute.createDiffuse(Color.RED))

	private val herbMaterial3 = herbMaterial
		.addAttribute(ColorAttribute.createDiffuse(Color.BLUE))

	private val herbMaterialList = listOf(herbMaterial1, herbMaterial2, herbMaterial3)

	val herbMainList = herbMaterialList.flatMap { createTemplates(it) }
	val herbDebugList = createTemplates(herbDebugMaterial)

	fun createTemplates(material: Material): List<BillboardData> {
		return listOf(
			BillboardData(material, Box2d.byDimension(Vector2d(0.0, 0.75), Vector2d(0.25, 0.25)), Vector2d(1.0), Vector3d(0.0, 0.17, -0.5)),
			BillboardData(material, Box2d.byDimension(Vector2d(0.25, 0.75), Vector2d(0.25, 0.25)), Vector2d(1.0), Vector3d(0.0, 0.17, -0.5)),
			BillboardData(material, Box2d.byDimension(Vector2d(0.50, 0.75), Vector2d(0.25, 0.25)), Vector2d(1.0), Vector3d(0.0, 0.17, -0.5)),
		)
	}
}