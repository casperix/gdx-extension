package casperix.scene

import casperix.AppEnvironment
import casperix.DemoScene
import casperix.math.vector.Vector3d
import casperix.misc.clamp
import casperix.renderer.billboard.BillboardObject
import casperix.renderer.billboard.BillboardRenderer
import casperix.settings.SceneSettings.debugMode
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import kotlin.math.*
import kotlin.random.Random

class BillboardScene(val environment: AppEnvironment) : DemoScene {
	override val provider = BillboardRenderer()



	private val random = Random(1)

	private val billboards = mutableListOf<BillboardObject>()

	init {
		environment.renderManager.onUpdate.then { rebuild() }
	}

	override fun rebuild() {
		val summaryAmount = sceneAmount.value

		if (summaryAmount < billboards.size) {

			if (billboards.isEmpty()) return

			val amount = ((billboards.size - summaryAmount) * 0.5).roundToInt().clamp(1, 100_000)

			(0 until amount).forEach {
				val billboard = billboards.removeLast()
				provider.removeBillboard(billboard)
			}

		} else if (summaryAmount > billboards.size){
			val amount = ((summaryAmount - billboards.size) * 0.5).roundToInt().clamp(1, 100_000)

			(0 until amount).forEach {
				createBillboard()
			}
		}
	}

	private fun createBillboard() {
		val summaryRange = sceneRange.value
		val range = sqrt(random.nextDouble()) * summaryRange
		val angle = random.nextDouble() * 2.0 * PI
		val sizeFactor = random.nextDouble() * 20.0 + 20.0
		val position = Vector3d(range * cos(angle), range * sin(angle), 0.0)
		val herbSourceList = if (debugMode.value) HerbAssets.herbDebugList else HerbAssets.herbMainList
		val source = herbSourceList.random(random)

		BillboardObject(position, source.copy(size = source.size * sizeFactor)).apply {
			provider.addBillboard(this)
			billboards.add(this)
		}
	}


}
