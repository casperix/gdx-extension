package casperix.scene

import casperix.AppEnvironment
import casperix.DemoScene
import casperix.gdx.geometry.toMatrix4
import casperix.gdx.graphics.GeometryBuilder
import casperix.math.Quaternionf
import casperix.math.matrix.Matrix4f
import casperix.math.vector.Vector3f
import casperix.renderer.core.overclocked.OverclockedModelCache
import casperix.renderer.util.MaterialBuilder
import casperix.renderer.util.TextureBuilder
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.ModelCache
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.SphereShapeBuilder
import com.badlogic.gdx.math.Vector3
import kotlin.math.*
import kotlin.random.Random

class CubesScene(val environment: AppEnvironment) : DemoScene {
	override val provider = OverclockedModelCache()

	override fun rebuild() {

		provider.begin()

		provider.add(createCubes())


		provider.end()

	}

	fun createCubes(): ModelInstance {
		var amount = sceneAmount.value
		val range = sceneRange.value.toFloat()
		return ModelInstance(
			GeometryBuilder.build(material = MaterialBuilder().setTexture(TextureBuilder.build(Gdx.files.internal("stones.png"), true, uWrap = Texture.TextureWrap.Repeat, vWrap = Texture.TextureWrap.Repeat)).build()) { builder ->
				val random = Random(1)
				var x = 0
				var y = 0
				val maxX = sqrt(amount.toDouble()).roundToInt()
				while (--amount >= 0) {
					builder.setVertexTransform(Matrix4f.compose(Vector3f( x * 16f, y * 16f, 0f), Vector3f.ONE, Quaternionf.IDENTITY).toMatrix4())
					if (random.nextBoolean()) {
						SphereShapeBuilder.build(builder, 12f, 12f, 12f, 6, 8, 0f, 180f, 0f, 180f)
					} else {
						BoxShapeBuilder.build(builder, 0f, 0f, 2f, 12f, 12f, 4f)
					}



					if (++x > maxX) {
						x = 0
						y++
					}
				}

			}, Vector3(0f, 0f, 0f)
		)
	}
}
