package casperix.scene

import casperix.AppEnvironment
import casperix.DemoScene
import casperix.gdx.geometry.toVector3
import casperix.renderer.loader.SceneAssetProvider
import casperix.math.vector.Vector3d
import casperix.renderer.core.overclocked.OverclockedModelCache
import casperix.renderer.loader.SceneAssetModification
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import com.badlogic.gdx.graphics.g3d.ModelCache
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.math.Vector3
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.random.Random

class X3Scene(val environment: AppEnvironment) : DemoScene {
	override val provider = OverclockedModelCache()
	val random = Random(1)

	override fun rebuild() {
		val treeModel = SceneAssetProvider.getInternalModifiedModel("tree_circle.gltf", SceneAssetModification())

		provider.begin()

		val summaryAmount = sceneAmount.value
		val summaryRange = sceneRange.value

		(1..summaryAmount).forEach {
			val range = sqrt(random.nextDouble()) * summaryRange
			val angle = random.nextDouble() * 2.0 * PI
			val position = Vector3d(range * cos(angle), range * sin(angle), 0.0).toVector3()

			val scale = 12f * (0.5 + random.nextFloat()).toFloat()
			val treeInstance = ModelInstance(treeModel)
			treeInstance.transform.translate(position)
			treeInstance.transform.rotate(Vector3.Z, 360f * random.nextFloat())
			treeInstance.transform.scale(scale, scale, scale)
			provider.add(treeInstance)
		}

		provider.end()

	}
}
