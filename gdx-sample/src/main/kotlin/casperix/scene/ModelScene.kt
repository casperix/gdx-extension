package casperix.scene

import casperix.AppEnvironment
import casperix.DemoScene
import casperix.SceneType
import casperix.math.Quaternionf
import casperix.math.geometry.fPI
import casperix.math.matrix.Matrix4f
import casperix.math.vector.Vector3d
import casperix.math.vector.Vector3f
import casperix.renderer.core.overclocked.OverclockedModelCache
import casperix.renderer.core.overclocked.OverclockedModelInstance
import casperix.renderer.loader.SceneAssetModification
import casperix.renderer.loader.SceneAssetProvider
import casperix.renderer.util.addAttribute
import casperix.settings.SceneSettings
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt
import kotlin.random.Random

class ModelScene(val environment: AppEnvironment) : DemoScene {
	override val provider = OverclockedModelCache()
	private val random = Random(1)
	private val modelName = "test.gltf"
	private val mergeMaterials = true
	private val materials = SceneAssetProvider.getAsset(Gdx.files.internal("materials.gltf"))
	private val models = listOf(
//		SceneAssetProvider.getInternalModifiedModel("/home/casper/projects/short-rail/assets/target/vehicle/diesel1.gltf", SceneAssetModification(mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
		SceneAssetProvider.getInternalModifiedModel(modelName, SceneAssetModification(convertMaterials = ::paintToRed, mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
//		SceneAssetProvider.getInternalModifiedModel(modelName, SceneAssetModification(convertMaterials = ::paintToGreen, mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
//		SceneAssetProvider.getInternalModifiedModel(modelName, SceneAssetModification(convertMaterials = ::paintToBlue, mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
//		SceneAssetProvider.getInternalModifiedModel(modelName, SceneAssetModification(convertMaterials = ::paintToYellow, mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
		SceneAssetProvider.getInternalModifiedModel(modelName, SceneAssetModification(convertMaterials = ::paintToWhite, mergeMeshes = mergeMaterials, mergeMaterials = mergeMaterials)),
	)

	private val instances = mutableListOf<RenderableProvider>()

	private fun paintToWhite(material: Material): Material {
		return if (material.id == "PaintLS") {
			material.addAttribute(PBRColorAttribute.createBaseColorFactor(Color.RED))
//			materials.scene.model.materials.firstOrNull { it.id == "WhiteLS" } ?: material
		} else {
			material
		}
	}

	private fun paintToRed(material: Material): Material {
		return if (material.id == "PaintLS") {
			material.addAttribute(PBRColorAttribute.createBaseColorFactor(Color.BLUE))
//			materials.scene.model.materials.firstOrNull { it.id == "RedLS" } ?: material
		} else {
			material
		}
	}

	private fun paintToGreen(material: Material): Material {
		return if (material.id == "PaintLS") {
			material.addAttribute(PBRColorAttribute.createBaseColorFactor(Color.GREEN))
//			materials.scene.model.materials.firstOrNull { it.id == "GreenLS" } ?: material
		} else {
			material
		}
	}

	private fun paintToBlue(material: Material): Material {
		return if (material.id == "PaintLS") {
			material.addAttribute(PBRColorAttribute.createBaseColorFactor(Color.BLUE))
//			materials.scene.model.materials.firstOrNull { it.id == "BlueLS" } ?: material
		} else {
			material
		}
	}

	private fun paintToYellow(material: Material): Material {
		return if (material.id == "PaintLS") {
			materials.scene.model.materials.firstOrNull { it.id == "YellowLS" } ?: material
		} else {
			material
		}
	}

	init {
		SceneSettings.useCache.then {
			rebuild()
		}
		SceneSettings.sceneType.then {
			rebuild()
		}
		SceneSettings.sceneScaleInstance.then {
			rebuild()
		}
	}

	override fun rebuild() {
		instances.forEach {
			environment.renderManager.remove(it)
		}
		instances.clear()

		val isSceneActive = SceneSettings.sceneType.value == SceneType.MODEL
		if (!isSceneActive) return

		val useCache = SceneSettings.useCache.value
		provider.begin()

		(1..SceneSettings.sceneAmount.value).forEach {
			val range = sqrt(random.nextDouble()) * SceneSettings.sceneRange.value
			val angle = random.nextDouble() * 2.0 * PI
			val position = Vector3d(range * cos(angle), range * sin(angle), 0.0)

			val model = models.random(random)
			val instance = createInstance(model, position.toVector3f(), true)

			if (useCache) {
				provider.add(instance)
			} else {
				instances.add(instance)
				environment.renderManager.add(instance)
			}
		}

		provider.end()
	}

	private fun createInstance(model: Model, position: Vector3f, canTransform: Boolean): RenderableProvider {
		val scale = SceneSettings.sceneScaleInstance.value
		val r = random.nextFloat()
		val axis = Vector3f.Z//if (r < 0.33) Vector3f.X else if (r < 0.67) Vector3f.Y else Vector3f.Z
		val angle = random.nextInt(3).toFloat() * fPI / 2f

		return OverclockedModelInstance(
			ModelObject(
				if (!canTransform) Matrix4f.IDENTITY else
					Matrix4f.compose(
						position,
						Vector3f(scale),
						Quaternionf.fromAxisAnge(axis, angle)
					),
				model
			)
		)
	}
}
