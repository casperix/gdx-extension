package casperix.scene

import casperix.AppEnvironment
import casperix.DemoScene
import casperix.SceneSelector
import casperix.SceneType
import casperix.file.FileReference
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d
import casperix.renderer.imposter.ImposterFactory
import casperix.renderer.imposter.ImposterModel
import casperix.renderer.imposter.ImposterRenderer
import casperix.renderer.imposter.ImposterSettings
import casperix.renderer.loader.SceneAssetModification
import casperix.renderer.loader.SceneAssetProvider
import casperix.settings.SceneSettings
import casperix.settings.SceneSettings.debugMode
import casperix.settings.SceneSettings.imposterResolution
import casperix.settings.SceneSettings.imposterStepFactor
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import casperix.ui.engine.font.GdxFont
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.g3d.ModelInstance
import java.lang.Math.*
import kotlin.random.Random

class ImposterScene(val environment: AppEnvironment) : DemoScene {
	val renderManager = environment.renderManager
	val debugFont = environment.uiEngine.supplier.fontFromFile(FileReference.classpath("fonts/Verdana.ttf"), 8, "0123456789,.:- havid") as GdxFont

	val SAVE_IMPOSTER_TO_DISK = true

	//	val VARIANTS = listOf("test.gltf", "willow.gltf", "pine.gltf", "maple.gltf")
	val VARIANTS = listOf("test.gltf", "willow.gltf", "pine.gltf", "maple.gltf")
//	val VARIANTS = listOf("pine")

	val testModel = SceneAssetProvider.getInternalModifiedModel("test.gltf", SceneAssetModification(mergeMeshes = false))

	var imposterDataList = emptyList<ImposterModel>()
	override val provider = ImposterRenderer()
	val random = Random(1)

	init {
		debugMode.then { updateImposterModels() }
		imposterResolution.then { updateImposterModels() }
		imposterStepFactor.then { updateImposterModels() }
		SceneSettings.sceneType.then { updateImposterModelsIfEmpty() }
	}

	private fun updateImposterModelsIfEmpty() {
		if (imposterDataList.isEmpty()) {
			updateImposterModels()
		}
	}

	private fun updateImposterModels() {
		val isSceneActive = SceneSettings.sceneType.value == SceneType.IMPOSTER
		if (!isSceneActive) return

		imposterDataList.forEach {
			it.dispose()
		}
		imposterDataList = createImposterModels()
		rebuild()
	}

	private fun createImposterModels(): List<ImposterModel> {
		val tress = VARIANTS.map {
			SceneAssetProvider.getInternalModifiedModel(it, SceneAssetModification(mergeMeshes =  false))
		}

		val resolution = imposterResolution.value
		val stepFactor = imposterStepFactor.value
		val debugMode = debugMode.value


		val imposterDataList = tress.map { model ->
			val providers = listOf(ModelInstance(model))
			val factory = ImposterFactory(
				renderManager, providers, ImposterSettings(
					Vector2i(resolution, resolution),
					Vector3d(0.0, 0.0, 1.0),
					2.0,
					ImposterSettings.RangeInfo(-PI, PI, stepFactor * 4 + 1),
					ImposterSettings.RangeInfo(0.0, PI * 0.5, stepFactor + 1),
					true,
					true,
					debugMode,
					debugFont.bitmapFont,
				)
			)
			if (SAVE_IMPOSTER_TO_DISK) {
				factory.buildPixmap().forEachIndexed { index, pixmap ->
					val nameId = tress.indexOf(model)
					val name = VARIANTS.getOrNull(nameId) ?: "unknown"
					PixmapIO.writePNG(Gdx.files.local("temp/${name}/frame_$index.png"), pixmap)
				}
			}
			factory.build()
		}
		return imposterDataList
	}

	override fun rebuild() {
		provider.clear()

		if (imposterDataList.isEmpty()) return

		if (debugMode.value) {
			val debugModel = imposterDataList.first()
			provider.addParticle(debugModel, Vector3d(0.0, 0.0, 10.0), Vector2d(30.0))
//
//			testInstance1.transform.setToTranslation(40f, 0f, 10f)
//			testInstance1.transform.scale(10f, 10f, 10f)
//			renderManager.add(testInstance1)
//
//			testInstance2.transform.setToTranslation(-40f, 0f, 10f)
//			testInstance2.transform.scale(10f, 10f, 10f)
//			renderManager.add(testInstance2)


		} else {
			val summaryAmount = sceneAmount.value
			val summaryRange = sceneRange.value
			val releaseModelList = imposterDataList.subList(1, imposterDataList.size)

			(1..summaryAmount).forEach {
				val range = sqrt(random.nextDouble()) * summaryRange
				val angle = random.nextDouble() * 2.0 * PI
				val position = Vector3d(range * cos(angle), range * sin(angle), 0.0)

				val imposterData = releaseModelList.random(random)
				provider.addParticle(imposterData, position, Vector2d(40.0, 40.0))
			}
		}

//		renderManager.add(provider, RenderableGroup(true, true, -1))
	}
}