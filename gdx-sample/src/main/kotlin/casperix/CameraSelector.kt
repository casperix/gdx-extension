package casperix

import casperix.gdx.geometry.set
import casperix.math.axis_aligned.Box2d
import casperix.math.geometry.DEGREE_TO_RADIAN
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.misc.Disposable
import casperix.renderer.RenderManager
import casperix.renderer.camera.CameraHelperRender
import casperix.renderer.camera.createOrbitalCameraController
import casperix.scene.camera.orbital.OrbitalCameraInputSettings
import casperix.scene.camera.orbital.OrbitalCameraPointerInputSettings
import casperix.scene.camera.orbital.OrbitalCameraTransformSettings
import casperix.settings.CameraSettings.cameraType
import casperix.settings.CameraSettings.debugMode
import casperix.settings.CameraSettings.fixVerticalAngle
import casperix.settings.SceneSettings.sceneRange
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.PerspectiveCamera
import kotlin.math.PI

enum class CameraType {
	PERSPECTIVE,
	ORTHOGRAPHIC,
}

class CameraSelector(val renderManager: RenderManager) {

	val inputSettings = OrbitalCameraInputSettings(pointer = OrbitalCameraPointerInputSettings(rotateHorizontalAngleFactor = PI, rotateVerticalAngleFactor = PI, zoomStep = 10.0))
	val transformSettings = OrbitalCameraTransformSettings(PI * 0.01, PI * 0.55, pivotBox = Box2d(Vector2d(-sceneRange.value.toDouble()), Vector2d(sceneRange.value.toDouble())))
	val cameraController = createOrbitalCameraController(renderManager.onUpdate, renderManager.watcher.onCursor, ::getControlledCamera, inputSettings, transformSettings)
	var helperRender: Disposable? = null

	val perspectiveCamera = PerspectiveCamera()
	val orthographicCamera = OrthographicCamera()
	val helperCamera = OrthographicCamera()

	init {
		fixVerticalAngle.then { updateCamera() }
		cameraType.then { updateCamera() }
		debugMode.then { updateCamera() }
		updateCamera()
	}

	private fun getControlledCamera(): Camera {
		return when (cameraType.value) {
			CameraType.ORTHOGRAPHIC -> orthographicCamera
			CameraType.PERSPECTIVE -> perspectiveCamera
		}
	}

	private fun updateCamera() {
		helperRender?.dispose()
		helperRender = null

		if (fixVerticalAngle.value) {
			val angle = 30.0 * DEGREE_TO_RADIAN
			cameraController.transformer.settings = transformSettings.copy(minVerticalAngle = angle, maxVerticalAngle = angle)
		} else {
			cameraController.transformer.settings = transformSettings
		}

		if (!debugMode.value) {
			renderManager.camera = getControlledCamera()
		} else {
			renderManager.camera = helperCamera
			helperRender = CameraHelperRender(renderManager, getControlledCamera())
			helperCamera.apply {
				viewportWidth = Gdx.graphics.width.toFloat()
				viewportHeight = Gdx.graphics.height.toFloat()
				position.set(Vector3d(0.0, 0.0, 1000.0))
				direction.set(Vector3d(0.0, 0.0, -1.0))
				up.set(Vector3d(0.0, 1.0, 0.0))
				near = 1f
				far = 2000f
				update(true)
			}
		}
	}
}