package casperix

import casperix.gdx.app.GdxApplicationConfig
import casperix.gdx.app.GdxDesktopApplicationLauncher
import casperix.gdx.app.GdxDesktopWindowConfig

fun main() {
	GdxDesktopApplicationLauncher(
		GdxApplicationConfig(
			"Gdx Extension Demo",
			desktop = GdxDesktopWindowConfig()
		)
	) {
		App(it)
	}
}

