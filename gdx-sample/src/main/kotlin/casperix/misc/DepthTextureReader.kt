package casperix.misc

import casperix.map2d.IntMap2D
import casperix.math.color.Color
import casperix.math.vector.Vector2i
import com.badlogic.gdx.graphics.GLTexture
import org.lwjgl.opengl.GL30
import kotlin.math.roundToInt

object DepthTextureReader {

	fun getColorMap(texture: GLTexture): IntMap2D {
		val buffer = FloatArray(texture.width * texture.height)

		texture.bind()
		GL30.glGetTexImage(GL30.GL_TEXTURE_2D, 0, GL30.GL_RED, GL30.GL_FLOAT, buffer)

		return floatToColorMap(texture.width, texture.height, buffer)
	}

	private fun floatToColorMap(width: Int, height: Int, buffer: FloatArray): IntMap2D {
		return IntMap2D(Vector2i(width, height), IntArray(width * height) { index ->
			val raw = buffer[index]
			if (raw.isNaN()) {
				Color.RED.value
			} else {
				val value = (raw * 255.0).roundToInt()
				Color(value, value, value, 255).value
			}
		})
	}

}