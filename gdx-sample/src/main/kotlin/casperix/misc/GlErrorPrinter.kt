package casperix.misc

import org.lwjgl.opengl.GL30

object GlErrorPrinter {
	fun print() {
		do {
			val enum = GL30.glGetError()
			if (enum != GL30.GL_NO_ERROR) {
				println(enum)
			}
		} while (enum != GL30.GL_NO_ERROR)

	}
}