package casperix

import casperix.gdx.graphics.GeometryBuilder
import casperix.renderer.core.RenderableGroup
import casperix.renderer.core.overclocked.OverclockedModelInstance
import casperix.renderer.util.MaterialBuilder
import casperix.renderer.util.TextureBuilder
import casperix.scene.*
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import casperix.settings.SceneSettings.sceneType
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.CylinderShapeBuilder
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import kotlin.math.PI
import kotlin.time.ExperimentalTime
import kotlin.time.measureTime

enum class SceneType {
	IMPOSTER,
	X3,
	BILLBOARD,
	CUBES,
	MODEL,
}

@OptIn(ExperimentalTime::class)
class SceneSelector(environment: AppEnvironment) {
	val renderManager = environment.renderManager

	companion object {

		fun createBack(): OverclockedModelInstance {
			val cylinderSize = sceneRange.value * 2f
			return OverclockedModelInstance(
				GeometryBuilder.build(material = MaterialBuilder().setTexture(TextureBuilder.build(Gdx.files.internal("clay.png"), true, uWrap = Texture.TextureWrap.Repeat, vWrap = Texture.TextureWrap.Repeat)).build()) { builder ->

					val matrix = Matrix4()
					matrix.setToRotationRad(Vector3.X, -PI.toFloat() / 2f)
					builder.setVertexTransform(matrix)
					builder.setUVRange(0f, 0f, 10f, 10f)
					CylinderShapeBuilder.build(builder, cylinderSize, 8f, cylinderSize, 40)


				}, Vector3(0f, 0f, -4f+0f)
			)
		}
	}

	val modelScene = ModelScene(environment)
	val cubesScene = CubesScene(environment)
	val imposterScene = ImposterScene(environment)
	val x3Scene = X3Scene(environment)
	val billboardScene = BillboardScene(environment)
	var activeScene: DemoScene? = null

	private var lastBack: RenderableProvider? = null

	init {

		sceneType.then { selectScene() }
		sceneRange.then { updateBack();rebuildScene() }
		sceneAmount.then { rebuildScene() }

		selectScene()
	}

	private fun rebuildScene() {
		val time = measureTime {
			activeScene?.rebuild()
		}
	}

	private fun updateBack() {
		lastBack?.let { renderManager.remove(it,  RenderableGroup(priority = 1)) }
		lastBack = null
		val nextBack = createBack()
		renderManager.add(nextBack, RenderableGroup(priority = 1))
		lastBack = nextBack
	}

	fun selectScene() {
		renderManager.camera.far = sceneRange.value * 3f

		when (sceneType.value) {
			SceneType.MODEL-> setupScene(modelScene)
			SceneType.CUBES -> setupScene(cubesScene)
			SceneType.X3 -> setupScene(x3Scene)
			SceneType.IMPOSTER -> setupScene(imposterScene)
			SceneType.BILLBOARD -> setupScene(billboardScene)
		}
		rebuildScene()
	}

	private fun setupScene(nextScene: DemoScene) {
		if (nextScene == activeScene) return

		activeScene?.apply {
			renderManager.remove(provider)
		}
		renderManager.add(nextScene.provider)
		activeScene = nextScene
	}


}