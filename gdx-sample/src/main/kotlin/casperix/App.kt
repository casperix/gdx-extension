package casperix

import casperix.app.window.WindowWatcher
import casperix.gdx.geometry.toPrecision
import casperix.gdx.geometry.toVector3
import casperix.gdx.geometry.toVector3f
import casperix.math.color.Color4d
import casperix.math.color.toColor
import casperix.math.geometry.RADIAN_TO_DEGREE
import casperix.math.vector.Vector3d
import casperix.renderer.RenderManager
import casperix.renderer.atlas.MultiLayerAtlas
import casperix.renderer.light.LightHelperRender
import casperix.renderer.loader.SceneAssetProvider
import casperix.renderer.shadow.PlaneShadowAdapter
import casperix.renderer.shadow.ShadowRender
import casperix.renderer.shadow.ShadowRenderSettings
import casperix.scene.camera.orbital.OrbitalCameraController
import casperix.ui.UILightController
import casperix.ui.UIMiscController
import casperix.ui.UIShadowController
import casperix.ui.UIStyle
import casperix.ui.component.panel.Panel
import casperix.ui.component.tab.TabMenu
import casperix.ui.component.tab.ToggleTab
import casperix.ui.component.text.Text
import casperix.ui.component.timer.TimerLogic
import casperix.ui.component.toggle.Toggle
import casperix.ui.core.SizeMode
import casperix.ui.engine.GdxUIManager
import casperix.ui.engine.UIManager
import casperix.ui.engine.toColor
import casperix.ui.type.LayoutAlign
import casperix.ui.type.LayoutSide
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import net.mgsx.gltf.scene3d.lights.DirectionalLightEx
import kotlin.math.roundToInt

data class AppEnvironment(val renderManager: RenderManager, val uiEngine: UIManager, val light: DirectionalLight, val shadowRender: ShadowRender)

class App(val watcher: WindowWatcher) {

	val renderManager = RenderManager.createSimpleRender(watcher, Color4d(0.2, 0.4, 0.8, 0.0)).apply {
		SceneAssetProvider.debugMergeInfo = true
		SceneAssetProvider.manualAtlasUpdate = false
		MultiLayerAtlas.debugSaveAtlas = true
	}

	val uiEngine = GdxUIManager(watcher, true, false)
	val light = DirectionalLightEx().apply { setDirection(Vector3d(0.6, 0.6, -0.5).toVector3()); intensity = 2f }
	val shadowRender = ShadowRender(renderManager, light, ShadowRenderSettings())
	val environment = AppEnvironment(renderManager, uiEngine, light, shadowRender)

	val cameraSelector = CameraSelector(renderManager)
	val sceneSelector = SceneSelector(environment)

	init {
		Gdx.graphics.setWindowedMode(1100, 600)
		Gdx.gl.glLineWidth(3f)
		renderManager.environment.set(ColorAttribute.createAmbientLight(Color4d(0.1).toColor().toColor()))

		LightAnimator(renderManager, light)
		LightHelperRender(renderManager)

		createUI()
		RenderToTextureDemo(renderManager)
		PlaneShadowAdapter(renderManager, shadowRender, light)

		renderManager.onPreRender.then {
			SceneAssetProvider.updateAtlas()
		}
	}

	private fun createUI() {
		val menu = TabMenu<ToggleTab>(LayoutSide.TOP)
		menu.node.placement.sizeMode = SizeMode.constWidth(UIStyle.PANEL_WIDTH)
		menu.node.placement.align = LayoutAlign.LEFT
		Panel(menu.node)

		menu.tabs += ToggleTab(Toggle("shadow", UIStyle.BUTTON_SIZE), UIShadowController(uiEngine, renderManager, shadowRender).node)
		menu.tabs += ToggleTab(Toggle("light", UIStyle.BUTTON_SIZE), UILightController(renderManager).node)
		menu.tabs += ToggleTab(Toggle("misc", UIStyle.BUTTON_SIZE), UIMiscController(renderManager, sceneSelector).node)
		uiEngine.root += menu

		menu.tabs[2].toggle.switch.set(true)

		val performanceInfo = Text("")
		performanceInfo.node.placement.align = LayoutAlign.RIGHT_TOP
		uiEngine.root += performanceInfo

		var tick = 0.0
		performanceInfo.node.events.nextFrame.then {
			tick = it
		}
		TimerLogic.timeInterval(performanceInfo.node, 0.1) {
			val info = mutableListOf<String>()

			val renderInfo = renderManager.statistic.get()
			info += "fps: " + Gdx.graphics.framesPerSecond
			info += "fps (by tick): " + (1.0 / tick).roundToInt()
			info += "batches: " + renderInfo.amounts.batches
			info += "vertices: " + renderInfo.amounts.vertices
			info += "elements: " + renderInfo.amounts.elements

			info += getCameraInfo(cameraSelector.cameraController)

			info += "light:"
			info += "up: " + shadowRender.camera.up.toPrecision(1)
			info += "direction: " + shadowRender.camera.direction.toPrecision(1)

			performanceInfo.text = info.joinToString("\n")
		}
	}

	private fun getCameraInfo(controller: Any?): List<String> {
		val base = renderManager.camera.run {
			listOf(
				"position: " + position.toVector3f().toPrecision(1),
				"direction: " + direction.toVector3f().toPrecision(1),
				"up: " + up.toVector3f().toPrecision(1),
			)
		}

		return if (controller is OrbitalCameraController) {
			val state = controller.transformer
			listOf("orbital-camera") + base + listOf(
				"pivot: ${state.getPivot().toPrecision(1)}",
				"v-angle: ${(state.getOffset().verticalAngle * RADIAN_TO_DEGREE).roundToInt()}",
				"h-angle: ${(state.getOffset().horizontalAngle * RADIAN_TO_DEGREE).roundToInt()}",
			)
		} else {
			listOf("unknown-camera")
		}
	}


}