package casperix.settings

import casperix.signals.concrete.StorageSignal

object LightSettings {
	val shadowDebugObserver = StorageSignal(false)
	val animationObserver = StorageSignal(true)
	val hdrObserver = StorageSignal(true)
	val gammaObserver = StorageSignal(true)
}