package casperix.settings

import casperix.signals.concrete.StorageSignal

object ShadowSettings {
	var hasShadowObserver = StorageSignal(true)
	var hasPCFObserver = StorageSignal(true)
	var showDepthObserver = StorageSignal(false)
	var invertBiasObserver = StorageSignal(true)
	var doublePrecisionObserver = StorageSignal(false)
}