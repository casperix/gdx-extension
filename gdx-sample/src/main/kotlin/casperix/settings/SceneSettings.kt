package casperix.settings

import casperix.SceneType
import casperix.signals.concrete.StorageSignal

object SceneSettings {
	val sceneType = StorageSignal(SceneType.MODEL)
	val sceneAmount = StorageSignal(100)
	val sceneRange = StorageSignal(100)
	val sceneScaleInstance = StorageSignal(10f)
	val useCache = StorageSignal(true)
	val debugMode = StorageSignal(false)
	val imposterResolution = StorageSignal(64)
	val imposterStepFactor = StorageSignal(4)
}