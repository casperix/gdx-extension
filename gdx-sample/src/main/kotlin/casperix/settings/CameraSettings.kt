package casperix.settings

import casperix.CameraType
import casperix.signals.concrete.StorageSignal

object CameraSettings {
	var cameraType = StorageSignal(CameraType.PERSPECTIVE)
	var debugMode = StorageSignal(false)
	val fixVerticalAngle = StorageSignal(false)

}