package casperix.ui

import casperix.CameraType
import casperix.SceneSelector
import casperix.SceneType
import casperix.renderer.RenderManager
import casperix.math.vector.Vector2d
import casperix.settings.CameraSettings
import casperix.settings.CameraSettings.cameraType
import casperix.settings.CameraSettings.fixVerticalAngle
import casperix.settings.SceneSettings
import casperix.settings.SceneSettings.imposterResolution
import casperix.settings.SceneSettings.imposterStepFactor
import casperix.settings.SceneSettings.sceneAmount
import casperix.settings.SceneSettings.sceneRange
import casperix.settings.SceneSettings.sceneScaleInstance
import casperix.settings.SceneSettings.sceneType
import casperix.ui.component.NumberEditor
import casperix.ui.component.button.Button
import casperix.ui.component.text.Text
import casperix.ui.component.toggle.Toggle
import casperix.ui.component.toggle.ToggleGroup
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout
import casperix.ui.util.BooleanGroupSelector
import com.badlogic.gdx.Gdx

class UIMiscController(val renderManager: RenderManager, val sceneBuilder: SceneSelector) : UIComponent(UINode()) {
	val buttonSize = Vector2d(150.0, 30.0)

	init {
		node.layout = Layout.VERTICAL

		node += Text("camera")
		ToggleGroup.createAndPlace(node, BooleanGroupSelector.Behaviour.ALWAYS_ONE, listOf(
			Toggle("orthographic", buttonSize, false) {
				if (it) cameraType.value = CameraType.ORTHOGRAPHIC
			},
			Toggle("perspective", buttonSize, false) {
				if (it) cameraType.value = CameraType.PERSPECTIVE
			},
		))
		node += Toggle("debug", buttonSize, false) {
			CameraSettings.debugMode.value = it
		}

		node += Text("scene type")
		ToggleGroup.createAndPlace(node, BooleanGroupSelector.Behaviour.ALWAYS_ONE, listOf(
			Toggle("model", buttonSize, false) {
				if (it) sceneType.value = SceneType.MODEL
			},
			Toggle("cubes", buttonSize, false) {
				if (it) sceneType.value = SceneType.CUBES
			},
			Toggle("billboard", buttonSize, false) {
				if (it) sceneType.value = SceneType.BILLBOARD
			},
			Toggle("imposter", buttonSize, false) {
				if (it) sceneType.value = SceneType.IMPOSTER
			},
			Toggle("X3", buttonSize, false) {
				if (it) sceneType.value = SceneType.X3
			},
		))

		node += Text("video")
		node += Toggle("vsync", buttonSize, true) {
			Gdx.graphics.setVSync(it)
		}

		node += Text("scene")
		node += NumberEditor.createIntMultiplier("range", sceneRange.value, 1.5f, 50, 5000) {
			sceneRange.value = it
		}
		node += NumberEditor.createFloatMultiplier("instance-scale", 3, sceneScaleInstance.value, 2f, 0.01f, 100f) {
			sceneScaleInstance.value = it
		}

		node += NumberEditor.createIntMultiplier("amount", sceneAmount.value, 2f, 1, 1000000) {
			sceneAmount.value = it
		}

		node += NumberEditor.createIntMultiplier("imp-resolution", imposterResolution.value, 2f, 16, 512) {
			imposterResolution.value = it
		}
		node += NumberEditor.createIntMultiplier("imp-step-factor", imposterStepFactor.value, 2f, 2, 16) {
			imposterStepFactor.value = it
		}
		node += Toggle("debug", buttonSize, SceneSettings.debugMode.value) {
			SceneSettings.debugMode.value = it
		}
		node += Toggle("cache", buttonSize, SceneSettings.useCache.value) {
			SceneSettings.useCache.value = it
		}

		node += Toggle("fix-v-angle", buttonSize, fixVerticalAngle.value) {
			fixVerticalAngle.value = it
		}

		node += Button("rebuild", buttonSize) {
			sceneBuilder.selectScene()
		}

	}

}