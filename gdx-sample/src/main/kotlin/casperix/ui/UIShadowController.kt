package casperix.ui

import casperix.map2d.IntMap2D
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3f
import casperix.misc.DepthTextureReader
import casperix.renderer.RenderManager
import casperix.renderer.shadow.DepthPrecisionFormat
import casperix.renderer.shadow.ShadowAttribute
import casperix.renderer.shadow.ShadowRender
import casperix.renderer.shadow.ShadowRenderSettings
import casperix.settings.ShadowSettings.doublePrecisionObserver
import casperix.settings.ShadowSettings.hasPCFObserver
import casperix.settings.ShadowSettings.hasShadowObserver
import casperix.settings.ShadowSettings.invertBiasObserver
import casperix.settings.ShadowSettings.showDepthObserver
import casperix.ui.component.NumberEditor
import casperix.ui.component.image.Image
import casperix.ui.component.panel.Panel
import casperix.ui.component.text.Text
import casperix.ui.component.toggle.Toggle
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.engine.UIManager
import casperix.ui.graphic.BitmapGraphic
import casperix.ui.graphic.ScalingStretch
import casperix.ui.layout.Layout
import casperix.ui.layout.Orientation
import casperix.ui.layout.TableLayout

class UIShadowController(val uiEngine: UIManager, val renderManager: RenderManager, val shadowRender: ShadowRender) : UIComponent(UINode()) {
	val info = Text("")

	private val biasEditor = NumberEditor.createFloatMultiplier("bias", 5, 0.0001f, 2f, 0.00002f, 0.65536f)
	private val viewportWidthEditor = NumberEditor.createFloatMultiplier("width", 1, 500f, 2f, 16f, 4096f)
	private val viewportHeightEditor = NumberEditor.createFloatMultiplier("height", 1, 500f, 2f, 16f, 4096f)
	private val viewportRangeEditor = NumberEditor.createFloatMultiplier("range", 1, 100f, 2f, 2f, 1000f)
	private val textureWidthEditor = NumberEditor.createIntMultiplier("width", 1024, 2f, 16, 4096)
	private val textureHeightEditor = NumberEditor.createIntMultiplier("height", 1024, 2f, 16, 4096)

	private var lastDepthMap:IntMap2D? = null

	init {
		node.layout = Layout.VERTICAL

		shadowDrawer()

		val buttons = UINode(layout = TableLayout(Orientation.HORIZONTAL, 3))
		node += buttons

		hasShadowObserver.then { update() }
		hasPCFObserver.then { update() }
		invertBiasObserver.then { update() }

		buttons += Toggle("ON", UIStyle.BUTTON_SIZE, hasShadowObserver)
		buttons += Toggle("PCF", UIStyle.BUTTON_SIZE, hasPCFObserver)
		buttons += Toggle("inv. bias", UIStyle.BUTTON_SIZE, invertBiasObserver)
		buttons += Toggle("F32", UIStyle.BUTTON_SIZE, doublePrecisionObserver)

		registerNumberEditor(biasEditor)

		node += Text("viewport")
		registerNumberEditor(viewportWidthEditor)
		registerNumberEditor(viewportHeightEditor)
		registerNumberEditor(viewportRangeEditor)
		node += Text("texture")
		registerNumberEditor(textureWidthEditor)
		registerNumberEditor(textureHeightEditor)

		node += info
		update()
	}

	private fun registerNumberEditor(editor: NumberEditor<*>) {
		node += editor
		editor.observer.then { update() }
	}

	private fun shadowDrawer() {
		node += Toggle("gen shadow", Vector2d(100.0, 30.0), showDepthObserver)

		val imageNode = UINode()
		node += imageNode
		renderManager.onUpdate.then {
			imageNode.children.clear()

			if (showDepthObserver.value) {
				lastDepthMap?.let {
					uiEngine.supplier.bitmapRemove(it)
				}
				val colorMap = DepthTextureReader.getColorMap(shadowRender.getDepthMap().texture)
				val bitmap = uiEngine.supplier.bitmapFromPixels(colorMap, true, true)
				imageNode += Image(BitmapGraphic(bitmap, flipY = true, scaling = ScalingStretch()), Vector2d(256.0))
				lastDepthMap = colorMap
			} else {
				imageNode += Panel(UINode(Vector2d(256.0)))
			}
		}
	}

	private fun update() {
		shadowRender.setActive(hasShadowObserver.value)

		val depthFormat = if (doublePrecisionObserver.value) DepthPrecisionFormat.F32 else DepthPrecisionFormat.F16

		shadowRender.settings = ShadowRenderSettings(
			Vector2f(viewportWidthEditor.value, viewportHeightEditor.value),
			viewportRangeEditor.value,
			viewUp = Vector3f.Z,
			depthFormat,
			Vector2i(textureWidthEditor.value, textureHeightEditor.value)
		)

		val biasSign = if (invertBiasObserver.value) -1f else 1f
		val bias = biasEditor.value * biasSign

		val environment = renderManager.environment
		environment.set(ShadowAttribute(ShadowAttribute.BIAS, true, bias))
		environment.set(ShadowAttribute(ShadowAttribute.PCF, hasPCFObserver.value))
	}
}