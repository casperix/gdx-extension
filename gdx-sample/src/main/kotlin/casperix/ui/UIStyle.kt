package casperix.ui

import casperix.math.vector.Vector2d

object UIStyle {
	val PANEL_WIDTH = 300.0
	val BUTTON_SIZE = Vector2d(80.0, 40.0)
}