package casperix.ui

import casperix.renderer.RenderManager
import casperix.renderer.shadow.ShadowAttribute
import casperix.renderer.simple.ColorCorrectionAttribute
import casperix.settings.LightSettings.animationObserver
import casperix.settings.LightSettings.gammaObserver
import casperix.settings.LightSettings.hdrObserver
import casperix.settings.LightSettings.shadowDebugObserver
import casperix.ui.component.text.Text
import casperix.ui.component.toggle.Toggle
import casperix.ui.core.UIComponent
import casperix.ui.core.UINode
import casperix.ui.layout.Layout

class UILightController(val renderManager: RenderManager) : UIComponent(UINode()) {

	init {
		node.layout = Layout.VERTICAL

		node += Text("shadow")
		node += Toggle("debug", UIStyle.BUTTON_SIZE, shadowDebugObserver)

		node += Text("light")
		node += Toggle("hdr", UIStyle.BUTTON_SIZE, hdrObserver)
		node += Toggle("gamma", UIStyle.BUTTON_SIZE, gammaObserver)

		shadowDebugObserver.then { update() }
		hdrObserver.then { update()}
		gammaObserver.then { update()}

		node += Toggle("animation", UIStyle.BUTTON_SIZE, animationObserver)


		update()
	}


	private fun update() {
		renderManager.environment.set(ShadowAttribute(ShadowAttribute.DEBUG, shadowDebugObserver.value))
		renderManager.environment.set(ColorCorrectionAttribute(ColorCorrectionAttribute.HDR, hdrObserver.value))
		renderManager.environment.set(ColorCorrectionAttribute(ColorCorrectionAttribute.GAMMA, gammaObserver.value))
	}
}