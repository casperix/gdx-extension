package casperix

import casperix.gdx.graphics.ModelBuilder
import casperix.math.color.Color4d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d
import casperix.misc.ByteBuffer
import casperix.renderer.RenderManager
import casperix.renderer.bake.*
import casperix.renderer.billboard.BillboardObject
import casperix.renderer.billboard.BillboardRenderer
import casperix.renderer.core.overclocked.OverclockedModelInstance
import casperix.renderer.loader.SceneAssetModification
import casperix.renderer.loader.SceneAssetProvider
import casperix.renderer.shadow.ShadowAttribute
import casperix.renderer.shadow.ShadowRender
import casperix.scene.HerbAssets
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.GLTexture
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.Texture
import org.lwjgl.opengl.GL30

class RenderToTextureDemo(val renderManager: RenderManager) {
	init {

		val settings = RenderSettings(
			CameraSettings(
				PerspectiveCameraType(30.0), 1.0, 100.0
			),
			Color4d(0.0, 0.0, 0.0, 0.0),
			Vector2i(512, 512),
			Texture.TextureFilter.Nearest,
			Texture.TextureFilter.Nearest,
			true,
			null,
			false,
			false,
		)
		val frameSettings = FrameSettings(CameraTransform(Vector3d(5.0, -5.0, 5.0), Vector3d(-1.0, 1.0, -1.0), Vector3d.Z))

		val model = SceneAssetProvider.getInternalModifiedModel("small_residential_1.gltf", SceneAssetModification(mergeMaterials = true))
		val back = ModelBuilder.cylinder(size = Vector3d(10.0, 0.1, 10.0))

		val renderer = BillboardRenderer()
		val billboardData = HerbAssets.herbMainList.first().copy(size = Vector2d(2.0, 3.0))
		renderer.addBillboard(BillboardObject(Vector3d(0.7, 0.5, 0.0), billboardData))

		val instances = listOf(OverclockedModelInstance(model), OverclockedModelInstance(back), renderer)

		SceneAssetProvider.updateAtlas()
		val renderToTarget = RenderToTarget(renderManager, settings, renderManager.mainShaderProvider, renderManager.shadowShaderProvider)

		val shadowRender = renderManager.getComponent<ShadowRender>() ?: throw Error("Add ${ShadowRender::class} to ${RenderManager::class}")
		val original = shadowRender.settings
		renderManager.environment.set(ShadowAttribute(ShadowAttribute.BIAS, true, 0.01f))
		shadowRender.settings = shadowRender.settings.copy(viewSize = Vector2f(10f), viewRange = 10f)

		val outputTexture = renderToTarget.renderToTexture(instances, instances, frameSettings)
//		val pixmap =  getColorMap(outputTexture)
		val pixmap = renderToTarget.pixmapMap.values.first()
		PixmapIO.writePNG(FileHandle("temp/demo.png"), pixmap)

		shadowRender.settings = original
	}

	private fun getColorMap(texture: GLTexture): Pixmap {
		val buffer = ByteBuffer(texture.width * texture.height * 4)

		texture.bind()
		GL30.glGetTexImage(GL30.GL_TEXTURE_2D, 0, GL30.GL_RGBA, GL30.GL_UNSIGNED_BYTE, buffer)

		val pixmap = Pixmap(texture.width, texture.height, Pixmap.Format.RGBA8888)
		pixmap.pixels.put(buffer)
		return pixmap
	}
}
