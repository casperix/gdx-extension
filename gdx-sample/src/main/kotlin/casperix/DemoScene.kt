package casperix

import com.badlogic.gdx.graphics.g3d.RenderableProvider

interface DemoScene {
	val provider: RenderableProvider
	fun rebuild()

}