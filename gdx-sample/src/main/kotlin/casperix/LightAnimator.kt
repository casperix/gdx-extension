package casperix

import casperix.gdx.geometry.toVector3
import casperix.math.Quaterniond
import casperix.math.vector.Vector3d
import casperix.renderer.RenderManager
import casperix.settings.LightSettings
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.math.Vector3

class LightAnimator(val renderManager: RenderManager, val light: DirectionalLight) {
	val BASE_SPEED = 0.05
	var angle = 0.5
	var speed = 0.3
	val axis = Vector3d(0.0, 1.0, 0.2).normalize()

	init {
		renderManager.onUpdate.then {
			update(it)
		}
	}

	private fun update(tick: Double) {
		if (!LightSettings.animationObserver.value) return
		angle += tick * speed
		val q = Quaterniond.fromAxisAnge(axis, angle)
		val direction = q.transform(Vector3d.X)

		if (direction.z > 0) speed = BASE_SPEED * 30.0 else speed = BASE_SPEED

		light.setDirection(direction.toVector3())
	}
}