package casperix.renderer.core.impl

import casperix.misc.Disposable
import casperix.renderer.core.RenderableMap
import casperix.renderer.core.RenderableGroup
import com.badlogic.gdx.graphics.g3d.RenderableProvider

data class SortedEntry(val group: RenderableGroup, val providerList:List<RenderableProvider>)

class SortedRenderableMap() : Disposable, RenderableMap {


	private val entries = mutableMapOf<RenderableGroup, RenderableGroupEntry>()

	override fun clear() {
		entries.clear()
	}

	fun groups(isDepthPass: Boolean): List<SortedEntry> {
		val filteredGroups = entries.filter { if (isDepthPass) it.key.castShadow else it.key.renderColor }
		val sortedGroups = filteredGroups.toSortedMap { A, B -> compareGroups(A, B) }
		return sortedGroups.map { (group, entry) ->
			SortedEntry(group, entry.calculateRenderableProviders())
		}
	}

	private fun compareGroups(A: RenderableGroup, B: RenderableGroup): Int {
		val deltaPriority = B.priority - A.priority
		if (deltaPriority != 0) return deltaPriority
		if (B.renderColor != A.renderColor) return if (B.renderColor) 1 else -1
		if (B.castShadow != A.castShadow) return if (B.castShadow) 1 else -1
		return 0
	}

	fun getGroupSize(group: RenderableGroup): Int {
		return getGroupEntry(group).size
	}

	private fun getGroupEntry(group: RenderableGroup): RenderableGroupEntry {
		return entries.getOrPut(group) { RenderableGroupEntry() }
	}

	override fun add(instance: RenderableProvider, group: RenderableGroup) {
		val entry = getGroupEntry(group)
		entry.add(instance)
	}

	override fun remove(instance: RenderableProvider, group: RenderableGroup) {
		val entry = getGroupEntry(group)
		entry.remove(instance)
		if (entry.size == 0) entries.remove(group)
	}

	override fun dispose() {
		clear()
	}

}