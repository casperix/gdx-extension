package casperix.renderer.core.impl

import com.badlogic.gdx.graphics.g3d.RenderableProvider

internal class RenderableGroupEntry() {
	private val original = mutableSetOf<RenderableProvider>()
	private var sorted = emptyList<RenderableProvider>()
	private var dirty: Boolean = false

	val size: Int get() = original.size

	fun calculateRenderableProviders(): List<RenderableProvider> {
		if (dirty) {
			sorted = original.toList()//sortedWith(renderableProviderComparator)
			dirty = false
		}
		return sorted
	}

	fun add(instance: RenderableProvider) {
		original += instance
		dirty = true
	}

	fun remove(instance: RenderableProvider) {
		original -= instance
		dirty = true
	}
}