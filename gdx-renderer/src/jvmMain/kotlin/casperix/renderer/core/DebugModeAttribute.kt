package casperix.renderer.core

import com.badlogic.gdx.graphics.g3d.Attribute

class DebugModeAttribute(val enabled:Boolean) : Attribute(ID) {
	companion object {
		val ID = register("debugMode")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return DebugModeAttribute(enabled)
	}
}