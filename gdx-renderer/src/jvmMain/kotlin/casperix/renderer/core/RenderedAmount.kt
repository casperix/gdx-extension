package casperix.renderer.core

data class RenderedAmount(val vertices:Int, val elements:Int, val batches:Int) {
	operator fun plus(other:RenderedAmount):RenderedAmount {
		return RenderedAmount( vertices + other.vertices, elements + other.elements, batches + other.batches)
	}

	companion object {
		val ZERO = RenderedAmount(0, 0, 0)
	}
}