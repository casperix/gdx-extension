package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.utils.Pool

/**
 * 	Pool emulator for compatibility with default systems
 */
class RenderableExtendedPoolEmulator : Pool<Renderable>(0, 0) {
	override fun newObject(): OverclockedRenderable {
		return OverclockedRenderable()
	}

	override fun free(`object`: Renderable?) {

	}

	override fun obtain(): OverclockedRenderable {
		return newObject()
	}
}