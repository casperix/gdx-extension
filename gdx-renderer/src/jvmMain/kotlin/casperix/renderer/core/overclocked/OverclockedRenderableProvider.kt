package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool

interface OverclockedRenderableProvider : RenderableProvider {
	val version: Long

	fun createRenderableList(): List<OverclockedRenderable> {
		return createRenderableListFromDefault(this)
	}

	override fun getRenderables(renderables: Array<Renderable>, pool: Pool<Renderable>) {
		val overclocked = createRenderableList()
		renderables.addAll(*overclocked.toTypedArray())
	}

	companion object {
		fun createRenderableList(provider: RenderableProvider): List<OverclockedRenderable> {
			return if (provider is OverclockedRenderableProvider) {
				provider.createRenderableList()
			} else {
				createRenderableListFromDefault(provider)
			}
		}

		private fun createRenderableListFromDefault(provider: RenderableProvider): List<OverclockedRenderable> {
			val tempList = Array<Renderable>()
			val pool = RenderableExtendedPoolEmulator()
			provider.getRenderables(tempList, pool)
			tempList.removeAll { it.meshPart.mesh.numVertices == 0 }
			return tempList.map { OverclockedRenderable.wrap(it) }
		}
	}
}