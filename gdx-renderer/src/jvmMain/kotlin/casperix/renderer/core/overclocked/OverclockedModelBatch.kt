package casperix.renderer.core.overclocked

import casperix.renderer.billboard.BillboardShader
import casperix.renderer.core.RenderedAmount
import casperix.renderer.simple.SimpleShader
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.DefaultTextureBinder
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider
import com.badlogic.gdx.utils.GdxRuntimeException

class OverclockedModelBatch(val shaderProvider: ShaderProvider) {

	private var cacheList = mutableListOf<OverclockedRenderableCache>()
	private var camera: Camera? = null
	private val context = RenderContext(DefaultTextureBinder(DefaultTextureBinder.LRU, 1))
	private var lastHash = 		0
	private val renderableMap = mutableMapOf<String, MutableList<Renderable>>()
	var statistic = RenderedAmount(0, 0, 0); private set

	fun dispose() {
		shaderProvider.dispose()
	}

	private fun begin(cam: Camera) {
		if (camera != null) throw GdxRuntimeException("Call end() first.")
		camera = cam
		context.begin()
	}

	private fun end() {
		flush()
		context.end()
		camera = null
	}

	fun render(camera: Camera, environment: Environment, providerList: List<RenderableProvider>) {
		begin(camera)

		if (cacheList.size > providerList.size) {
			cacheList = cacheList.subList(0, providerList.size)
		}
		//	drop cache, if environment changed
		val nextHash = environment.hashCode()
		if (lastHash != nextHash) {
			lastHash = nextHash
			cacheList = mutableListOf()
		}
		providerList.forEachIndexed { index, renderableProvider ->
			var cacheItem = cacheList.getOrNull(index)
			val overclockedProvider = renderableProvider as? OverclockedRenderableProvider

			if (cacheItem == null || cacheItem.provider != renderableProvider || (overclockedProvider != null && overclockedProvider.version != cacheItem.version) || overclockedProvider == null) {
				cacheItem = OverclockedRenderableCache(renderableProvider, overclockedProvider?.version ?: 0L, environment, shaderProvider)

				if (cacheList.size == index) cacheList.add(cacheItem)
				else cacheList[index] = cacheItem
			}

			cacheItem.renderableList.forEach { renderable ->
				renderableMap.compute(renderable.batchKey) { _, items ->
					if (items == null) {
						mutableListOf(renderable)
					} else {
						items.add(renderable)
						items
					}
				}
			}
		}

		end()
	}

	private fun flush() {
		var currentShader: Shader? = null
		renderableMap.values.forEach { renderableList ->
			renderableList.forEach { renderable ->
				if (currentShader !== renderable.shader) {
					currentShader?.end()
					currentShader = renderable.shader
					currentShader!!.begin(camera, context)
				}
				currentShader!!.render(renderable)
			}
		}
		currentShader?.end()
		renderableMap.clear()
		statistic = SimpleShader.dropStatistic() + BillboardShader.dropStatistic()
	}
}