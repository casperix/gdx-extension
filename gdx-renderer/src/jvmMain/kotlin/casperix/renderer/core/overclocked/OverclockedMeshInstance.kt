package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.math.Matrix4

class OverclockedMeshInstance(mesh: Mesh, name:String = "", transform: Matrix4 = Matrix4(), material: Material = Material(), val shader:Shader? = null, val primitiveType: Int = GL20.GL_TRIANGLES) : OverclockedRenderableProvider {
	companion object {
		var instanceCounter = 0L
	}


	override var version = 0L + (++instanceCounter) * 0xFFFFFFFF

	var mesh: Mesh = mesh
		set(value) {
			if (field == value) return
			field = value
			version++
		}

	var transform: Matrix4 = transform
		set(value) {
			if (field == value) return
			field = value
			version++
		}

	var material: Material = material
		set(value) {
			if (field == value) return
			field = value
			version++
		}

	override fun createRenderableList(): List<OverclockedRenderable> {
		val item = OverclockedRenderable()
		item.material = material
		item.shader = shader
		item.worldTransform.set(transform)
		item.meshPart.set("", mesh, 0, if (mesh.numIndices != 0) mesh.numIndices else mesh.numVertices, primitiveType)

		return listOf(item)
	}
}