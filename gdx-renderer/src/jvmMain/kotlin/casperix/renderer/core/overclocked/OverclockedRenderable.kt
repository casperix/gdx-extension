package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.Renderable

class OverclockedRenderable() : Renderable() {

	constructor(other:Renderable) : this() {
		set(other)
		updateState()
	}

	fun updateState() {
		batchKey = meshPart.mesh.toString() + "#" + material.hashCode() + "#" + shader.toString()
	}

	var batchKey = ""

	companion object {
		fun wrap(renderable: Renderable):OverclockedRenderable {
			return (renderable as? OverclockedRenderable) ?: OverclockedRenderable(renderable)
		}
	}
}