package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.ModelCache
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.Pool

class OverclockedModelCache(val name:String = "") : ModelCache(), OverclockedRenderableProvider {
	val field = ModelCache::class.java.getDeclaredField("renderables")
	override var version = 0L

	init {
		field.isAccessible = true
	}

	override fun end() {
		super.end()
		version++
	}

	override fun getRenderables(renderables: Array<Renderable>, pool: Pool<Renderable>) {
		val self_renderables = field.get(this) as Array<Renderable>
		self_renderables.forEach {
			if (it.meshPart.mesh.numVertices != 0) {
				renderables.add(OverclockedRenderable.wrap(it))
			}
		}
	}
}