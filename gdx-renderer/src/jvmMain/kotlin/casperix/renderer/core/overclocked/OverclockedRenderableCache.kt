package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider

class OverclockedRenderableCache(val provider: RenderableProvider, val version: Long, environment: Environment, shaderProvider: ShaderProvider) {
	val renderableList = OverclockedRenderableProvider.createRenderableList(provider)

	init {
		bakeState(environment, shaderProvider)
	}

	private fun bakeState(environment: Environment, shaderProvider: ShaderProvider) {
		renderableList.forEach { renderable ->
			renderable.environment = renderable.environment ?: environment
			renderable.shader = renderable.shader ?: shaderProvider.getShader(renderable)
			renderable.updateState()
		}
	}
}