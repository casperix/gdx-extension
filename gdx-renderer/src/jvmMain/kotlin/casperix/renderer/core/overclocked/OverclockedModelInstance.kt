package casperix.renderer.core.overclocked

import casperix.gdx.geometry.toQuaternion
import casperix.math.Quaternionf
import casperix.scene.ModelObject
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.graphics.g3d.model.NodePart
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3

class OverclockedModelInstance(item: ModelObject) : OverclockedRenderableProvider {
	companion object {
		var instanceCounter = 0L
	}

	constructor(model: Model) : this(ModelObject(Matrix4(), model))

	constructor(model: Model, position: Vector3) : this(ModelObject(Matrix4(position, Quaternionf.IDENTITY.toQuaternion(), Vector3(1f, 1f, 1f)), model))

	private var instance = ModelInstance(item.data, item.transform)

	override var version = 0L + (++instanceCounter) * 0xFFFFFFFF

	var model: Model = item.data
		set(value) {
			if (field == value) return
			field = value
			instance = ModelInstance(value, transform)
			version++
		}

	var transform: Matrix4 = item.transform
		set(value) {
			if (field == value) return
			field = value
			instance.transform = value
			version++
		}

	override fun createRenderableList():List<OverclockedRenderable> {
		val target = mutableListOf <OverclockedRenderable>()
		for (node in instance.nodes) {
			getRenderableList(node, target)
		}
		return target
	}

	private fun getRenderableList(node: Node, renderables: MutableList<OverclockedRenderable>) {
		if (node.parts.size > 0) {
			for (nodePart in node.parts) {
				if (nodePart.enabled) {
					val renderable = OverclockedRenderable()
					setupRenderableItem(renderable, node, nodePart)
					renderables.add(renderable)
				}
			}
		}
		for (child in node.children) {
			getRenderableList(child, renderables)
		}
	}

	private fun setupRenderableItem(out: OverclockedRenderable, node: Node, nodePart: NodePart) {
		nodePart.setRenderable(out)
		if (nodePart.bones == null) {
			out.worldTransform.set(transform).mul(node.globalTransform)
			out.updateState()
		} else {
			out.worldTransform.set(transform)
		}
		out.userData = instance.userData
	}
}