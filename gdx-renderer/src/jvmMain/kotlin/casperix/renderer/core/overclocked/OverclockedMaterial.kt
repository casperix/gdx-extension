package casperix.renderer.core.overclocked

import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.Material

class OverclockedMaterial(attributes:List<Attribute>) : Material(*attributes.toTypedArray()) {
	private val hashCode = super.hashCode()

	override fun hashCode(): Int {
		return hashCode
	}
}