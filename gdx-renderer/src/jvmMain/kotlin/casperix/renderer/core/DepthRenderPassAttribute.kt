package casperix.renderer.core

import com.badlogic.gdx.graphics.g3d.Attribute

class DepthRenderPassAttribute() : Attribute(ID) {
	companion object {
		val ID = register("depthRenderPass")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return DepthRenderPassAttribute()
	}

}