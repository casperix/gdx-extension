package casperix.renderer.core

import com.badlogic.gdx.graphics.g3d.RenderableProvider

interface RenderableMap {
	fun add(instance: RenderableProvider, group: RenderableGroup = RenderableGroup("default", true, true, 0))
	fun remove(instance: RenderableProvider, group: RenderableGroup = RenderableGroup("default", true, true, 0))
	fun clear()
}