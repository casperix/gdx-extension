package casperix.renderer.core

class RenderedStatistic {
	fun nextFrame() {
		lastFrame = currentFrame
		currentFrame = RenderedAmountAndTime.ZERO
	}

	fun append(info:RenderedAmountAndTime) {
		currentFrame += info
	}

	fun get():RenderedAmountAndTime {
		return lastFrame
	}

	private var lastFrame = RenderedAmountAndTime.ZERO
	private var currentFrame = RenderedAmountAndTime.ZERO
}