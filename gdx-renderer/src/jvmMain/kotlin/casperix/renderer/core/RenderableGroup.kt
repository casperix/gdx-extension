package casperix.renderer.core

data class RenderableGroup(val name:String = "", val renderColor: Boolean = true, val castShadow: Boolean = true, val priority: Int = 0, val sortByMaterial: Boolean = true)