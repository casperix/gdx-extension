package casperix.renderer.core

import kotlin.time.Duration
import kotlin.time.ExperimentalTime
import kotlin.time.TimedValue

@OptIn(ExperimentalTime::class)
data class RenderedAmountAndTime(val amounts: RenderedAmount, val frameTime: Duration) {
	operator fun plus(other: RenderedAmountAndTime): RenderedAmountAndTime {
		return RenderedAmountAndTime( amounts + other.amounts, frameTime + other.frameTime)
	}

	constructor(value:TimedValue<RenderedAmount>) : this(value.value, value.duration)

	companion object {
		val ZERO = RenderedAmountAndTime(RenderedAmount(0, 0, 0), Duration.ZERO)
	}
}