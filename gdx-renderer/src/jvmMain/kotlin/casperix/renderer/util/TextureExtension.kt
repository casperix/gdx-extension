package casperix.renderer.util

import casperix.math.vector.Vector2i
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

fun Texture.asAttribute(id:Long):TextureAttribute {
	return TextureAttribute(id, this)
}

fun Texture.getSize():Vector2i {
	return Vector2i(width, height)
}