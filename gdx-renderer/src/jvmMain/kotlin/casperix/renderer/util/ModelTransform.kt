package casperix.renderer.util

import casperix.math.axis_aligned.Box2f
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.math.Quaternion
import com.badlogic.gdx.math.Vector3

object ModelTransform {
	fun scaleUV(mesh: Mesh, area: Box2f): Mesh? {
		val uvAttribute = mesh.getVertexAttribute(VertexAttributes.Usage.TextureCoordinates) ?: return null
		val buffer = FloatArray(mesh.numVertices * mesh.vertexSize / 4)
		mesh.getVertices(buffer)

		(0 until mesh.numVertices).forEach { vertexId ->
			val uvOffset = vertexId * mesh.vertexSize / 4 + uvAttribute.offset / 4

			val sourceU = buffer.get(uvOffset + 0)
			val sourceV = buffer.get(uvOffset + 1)

			val targetU = area.min.x + area.dimension.x * sourceU
			val targetV = area.min.y + area.dimension.y * sourceV

			buffer.set(uvOffset + 0, targetU)
			buffer.set(uvOffset + 1, targetV)
		}

		val nextMesh = mesh.copy(false)
		nextMesh.setVertices(buffer)
		return nextMesh
	}


	fun union(base: Model, append: Model, translation: Vector3, rotation: Quaternion, scale: Vector3) {
		append.nodes.forEach {
			if (!it.parts.isEmpty || it.children.firstOrNull() != null) {
				val next = it.copy()
				next.translation.add(translation)
				next.rotation.mul(rotation)
				next.scale.set(next.scale.x * scale.x, next.scale.y * scale.y, next.scale.z * scale.z)
				next.calculateWorldTransform()
				next.calculateLocalTransform()
				base.nodes.add(next)
			}
		}
	}
}