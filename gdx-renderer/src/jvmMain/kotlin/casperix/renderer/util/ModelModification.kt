package casperix.renderer.util

import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.model.MeshPart
import com.badlogic.gdx.graphics.g3d.model.Node

object ModelModification {

	fun replaceMeshByMesh(model: Model, replaceMap: Map<Mesh, Mesh>) {
		val meshParts = model.getAllMeshParts()

		replaceMap.forEach { last, next ->
			replaceMesh(model, meshParts, last, next)
		}
	}

	private fun replaceMesh(model: Model, meshParts: List<MeshPart>, last: Mesh, next: Mesh) {
		model.meshes.removeValue(last, false)
		if (!model.meshes.contains(next)) {
			model.meshes.add(next)
		}

		meshParts.forEach {
			if (it.mesh == last) it.mesh = next
		}
	}


	fun replaceMaterialByMaterial(model: Model, replaceMap: Map<Material, Material>) {
		val nodes = model.getAllNodes()

		replaceMap.forEach { (last, next) ->
			replaceMaterial(model, nodes, last, next)
		}
	}

	private fun replaceMaterial(model: Model, nodeList: List<Node>, last: Material, next: Material) {
		model.materials.removeValue(last, false)
		if (!model.materials.contains(next)) {
			model.materials.add(next)
		}

		nodeList.forEach { node ->
			node.parts.forEach { nodePart ->
				if (nodePart.material == last) nodePart.material = next
			}
		}
	}

	fun transformMesh(model: Model, transformMesh: (Mesh) -> Mesh) {
		val replaceMap = model.getAllMeshes().map { original ->
			Pair(original, transformMesh(original))
		}.toMap()

		replaceMeshByMesh(model, replaceMap)
	}

	fun transformMaterial(model: Model, transformMaterial: (Material) -> Material) {
		val replaceMap = model.getAllMaterials().map { original ->
			Pair(original, transformMaterial(original))
		}.toMap()

		replaceMaterialByMaterial(model, replaceMap)
	}
}