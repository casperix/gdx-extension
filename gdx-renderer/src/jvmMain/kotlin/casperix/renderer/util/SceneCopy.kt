package casperix.renderer.util

import com.badlogic.gdx.graphics.g3d.Model
import net.mgsx.gltf.scene3d.scene.SceneAsset
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import net.mgsx.gltf.scene3d.scene.SceneModel

fun SceneAsset.copy(): SceneAsset {
	val asset = SceneAsset()
	asset.animations = Array(animations)
	asset.data = data
	asset.meshes = Array(meshes)
	asset.textures = Array(textures)

	asset.scenes = Array(scenes.map { it.copy() }.toTypedArray())
	asset.scene = asset.scenes.first()

	return asset
}

fun SceneModel.copy(): SceneModel {
	val scene = SceneModel()
	scene.model = model.copy()
	scene.name = name
	scene.cameras = ObjectMap(cameras)
	scene.lights = ObjectMap(lights)
	return scene
}

fun Model.copy(): Model {
	val model = Model()
	model.meshes.addAll(meshes)
	model.meshParts.addAll(meshParts)
	model.animations.addAll(animations)
	model.materials.addAll(materials)
	model.nodes.addAll(*nodes.map { it.copy() }.toTypedArray())
	ModelModification.transformMaterial(model) {
		it.copy()
	}
	return model
}