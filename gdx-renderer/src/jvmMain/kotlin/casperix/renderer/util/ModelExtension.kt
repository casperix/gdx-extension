package casperix.renderer.util

import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.model.MeshPart
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.graphics.g3d.model.NodePart

fun Model.getAllNodes(): List<Node> {
	return collectRecursive(nodes.toList())
}

fun Model.getAllNodeParts(): List<NodePart> {
	return getAllNodes().flatMap { it.parts }
}

private fun collectRecursive(items: List<Node>): List<Node> {
	return items + items.flatMap { child ->
		collectRecursive(child.children.toList())
	}
}

fun Model.getAllMeshParts(): List<MeshPart> {
	return getAllNodes().flatMap {
		it.parts.map {
			it.meshPart
		}
	}
}

fun Model.getAllMeshes(): Set<Mesh> {
	return getAllNodes().flatMap {
		it.parts.map {
			it.meshPart.mesh
		}
	}.toSet()
}

fun Model.getAllMaterials(): Set<Material> {
	return getAllNodes().flatMap {
		it.parts.map {
			it.material
		}
	}.toSet()
}
