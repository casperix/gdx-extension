package casperix.renderer.util

import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.utils.Array


fun Material(attributes: List<Attribute>):Material {
	return Material(*attributes.toTypedArray())
}

fun Material.addAttribute(attribute: Attribute, nextId:String = id): Material {
	val attributes = mutableListOf<Attribute>()
	attributes += this
	attributes += attribute
	return Material(nextId, Array(attributes.toTypedArray()))
}

fun Material.removeAttribute(attribute: Attribute, nextId:String = id): Material {
	val attributes = mutableListOf<Attribute>()
	attributes += this
	attributes -= attribute
	return Material(nextId, Array(attributes.toTypedArray()))
}

fun Material.addAttributes(other: Attributes, nextId:String = id): Material {
	val attributes = mutableListOf<Attribute>()
	attributes += this
	attributes += other
	return Material(nextId, Array(attributes.toTypedArray()))
}

operator fun Material.plus(attribute: Attribute): Material {
	return addAttribute(attribute)
}

operator fun Material.plus(attributes: Attributes): Material {
	return addAttributes(attributes)
}