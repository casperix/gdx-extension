package casperix.renderer.util

import casperix.math.vector.Vector2i
import casperix.renderer.atlas.MultiLayerAtlas
import casperix.renderer.atlas.MaterialMergeInfo
import casperix.renderer.atlas.PartMergeInfo
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute
import net.mgsx.gltf.scene3d.scene.SceneAsset

object ModelMaterialMerger {
	private val diffuseTextures = mutableMapOf< Color, Texture>()
	private val pbrTextures = mutableMapOf<Vector2i, Texture>()
	private val normalTextures = mutableMapOf<Vector2i, Texture>()

	fun merge(asset: SceneAsset, atlasManager: MultiLayerAtlas) {
		asset.scenes.forEach { scene ->
			merge(scene.model, atlasManager)
		}
	}

	private fun Attributes.getTexture(attributeId: Long): Texture? {
		val attribute = get(attributeId) as? TextureAttribute
		return attribute?.textureDescription?.texture
	}

	fun generateMaterialInfo(material: Material): MaterialMergeInfo? {
		var diffuseTexture = material.getTexture(TextureAttribute.Diffuse)
		if (diffuseTexture == null) {
			val color = (material.get(PBRColorAttribute.BaseColorFactor) as? ColorAttribute)?.color ?: Color.WHITE
			diffuseTexture = diffuseTextures.getOrPut(color) {
				Texture(Pixmap(8, 8, Pixmap.Format.RGB888).apply {
					setColor(color)
					fill()
				})
			}
		}
		val baseSize = diffuseTexture.getSize()

		var normalTexture = material.getTexture(TextureAttribute.Normal)
		var pbrTexture = material.getTexture(PBRTextureAttribute.MetallicRoughnessTexture)

		if (normalTexture == null) {
			normalTexture = normalTextures.getOrPut(diffuseTexture.getSize()) {
				Texture(Pixmap(baseSize.x, baseSize.y, Pixmap.Format.RGB888).apply {
					setColor(0.5f, 0.5f, 1f, 1f)
					fill()
				})
			}
		}
		if (pbrTexture == null) {
			pbrTexture = pbrTextures.getOrPut(diffuseTexture.getSize()) {
				Texture(Pixmap(baseSize.x, baseSize.y, Pixmap.Format.RGB888).apply {
					setColor(0.0f, 0.0f, 0.0f, 1f)
					fill()
				})
			}
		}
		if (baseSize == normalTexture.getSize() && baseSize == pbrTexture.getSize()) {
			return MaterialMergeInfo(diffuseTexture, normalTexture, pbrTexture)
		}

		return null
	}

	fun merge(model: Model, atlasManager: MultiLayerAtlas) {
		val materialList = model.getAllMaterials()
		val materialInfoList = materialList.mapNotNull {
			generateMaterialInfo(it)
		}

		if (materialInfoList.isEmpty()) {
			return
		}

		val regions = atlasManager.pack(materialInfoList)

		val uvTransformMap = regions.mapIndexed { index, region ->
			val info = materialInfoList[index]
			Pair(info, region)
		}.toMap()

		val materialMap = mutableMapOf<Material, PartMergeInfo>()
		val nodePartList = model.getAllNodeParts()
		nodePartList.forEach { nodePart ->
			val materialInfo = generateMaterialInfo(nodePart.material)
			if (materialInfo != null) {
				materialMap.compute(nodePart.material) { _, lastInfo -> PartMergeInfo(materialInfo, lastInfo?.parts.orEmpty() + setOf(nodePart.meshPart)) }
			}
		}

		materialMap.forEach { material, info ->
			val lastMeshes = info.parts.map {
				it.mesh
			}.toSet().toList()

			val area = uvTransformMap[info.material]
			if (area != null) {
				ModelModification.transformMesh(model) { originalMesh ->
					val index = lastMeshes.indexOf(originalMesh)
					if (index >= 0) {
						val lastMesh = lastMeshes[index]
						ModelTransform.scaleUV(lastMesh, area) ?: originalMesh
					} else {
						originalMesh
					}
				}
			}
		}

		val replaceList = materialMap.keys
		val template = atlasManager.getMaterial() ?: throw Error("Atlas can't be empty")
		val replaceMap = replaceList.map { Pair(it, template) }.toMap()

		ModelModification.replaceMaterialByMaterial(model, replaceMap)
		model.materials.add(template)
	}

}