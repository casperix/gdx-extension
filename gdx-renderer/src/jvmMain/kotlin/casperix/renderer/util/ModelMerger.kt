package casperix.renderer.util

import casperix.misc.collapse
import casperix.misc.min
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.model.MeshPart
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.graphics.g3d.model.NodePart
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.scene3d.scene.SceneAsset

object ModelMerger {

	data class MergeKey(val material: Material, val primitiveType: Int, val vertexAttributes: VertexAttributes)

	data class PartInfo(val node: Node, val part: NodePart)

	fun colorize(model: Model, color: Color): Model {
		model.materials.forEach {
			if (it.id.contains("Paint")) {
				it.set(ColorAttribute.createDiffuse(color))
			}
		}
		return model
	}



	fun vertexAttributesUnification(model: Model) {
		val meshVertexAttributesMap = mutableMapOf<List<VertexAttribute>, List<Mesh>>()

		model.meshes.forEach { mesh ->
			val original = mesh.vertexAttributes
			val key = original.sortedBy { it.usage }

			meshVertexAttributesMap.compute(key) { _, meshes -> meshes.orEmpty() + mesh }
		}

		val replaceMap = mutableMapOf<Mesh, Mesh>()

		meshVertexAttributesMap.forEach { templateAttributes, meshes ->
			meshes.forEach { sourceMesh ->
				val targetMesh = replaceVertexAttribute(templateAttributes, sourceMesh)
				replaceMap.set(sourceMesh, targetMesh)
			}
		}

		ModelModification.replaceMeshByMesh(model, replaceMap)
	}

	private fun replaceVertexAttribute(attributes: List<VertexAttribute>, sourceMesh: Mesh): Mesh {
		val sourceAttributes = sourceMesh.vertexAttributes
		val attributesCopy = attributes.map { it.copy() }
		val targetAttributes = VertexAttributes(*attributesCopy.toTypedArray())
		if (sourceAttributes == targetAttributes) return sourceMesh

		val sourceIndicesBuffer = ShortArray(sourceMesh.numIndices)
		sourceMesh.getIndices(sourceIndicesBuffer)


		val sourceBuffer = FloatArray(sourceMesh.vertexSize / 4 * sourceMesh.numVertices)
		val targetBuffer = FloatArray(sourceMesh.vertexSize / 4 * sourceMesh.numVertices)
		sourceMesh.getVertices(sourceBuffer)

		val targetAttributeBySourceIndex = sourceAttributes.mapIndexed { index, sourceAttribute ->
			targetAttributes.findByUsage(sourceAttribute.usage)
		}
		(0 until sourceMesh.numVertices).forEach { vertexId ->
			sourceAttributes.forEachIndexed { sourceIndex, sourceAttribute ->
				val targetAttribute = targetAttributeBySourceIndex[sourceIndex]

				(0 until sourceAttribute.numComponents).forEach { component ->
					val sourceValue = sourceBuffer[vertexId * sourceMesh.vertexSize / 4 + sourceAttribute.offset / 4 + component]
					targetBuffer[vertexId * sourceMesh.vertexSize / 4 + targetAttribute.offset / 4 + component] = sourceValue
				}
			}
		}

		val targetMesh = Mesh(true, sourceMesh.numVertices, sourceMesh.numIndices, targetAttributes)
		targetMesh.setVertices(targetBuffer)
		targetMesh.setIndices(sourceIndicesBuffer)
		return targetMesh
	}

	fun mergeModel(model: Model) {
		val partsMap = collectParts(model)

		var counter = 0
		partsMap.forEach { (key, parts) ->
			val vertices = parts.collapse(0) { next, sum -> sum + next.part.meshPart.mesh.numVertices }
			if (vertices == 0) return@forEach

			val builder = MeshBuilder()
			builder.begin(key.vertexAttributes, key.primitiveType)

			parts.forEach { info ->
				val meshPart = info.part.meshPart

				val transform = info.node.globalTransform
				val transformedMesh = meshPart.mesh.copy(true)
				transformedMesh.transform(transform, 0, transformedMesh.numVertices)

				val isNegativeScale = transform.det() < 0f
				if (isNegativeScale) {
					invertFaceOrientation(transformedMesh)
				}

				model.meshes.removeValue(meshPart.mesh, true)
				model.meshParts.removeValue(meshPart, true)

				transformNormals(transformedMesh, transform)
				builder.addMesh(transformedMesh)
			}

			parts.forEach { info ->
				model.nodes.removeValue(info.node, true)
			}

			val generatedMesh = builder.end()
			generateNode("generated${++counter}", model, generatedMesh, key)
		}
	}

	private fun generateNode(name: String, model: Model, mesh: Mesh, key: MergeKey) {
		val node = Node()
		node.id = name

		val meshPart = MeshPart()
		meshPart.mesh = mesh
		meshPart.offset = 0
		meshPart.primitiveType = key.primitiveType
		meshPart.size = mesh.numIndices

		val rootPart = NodePart(meshPart, key.material)
		node.parts.add(rootPart)

		model.meshes.add(mesh)
		model.nodes.add(node)
		model.meshParts.add(meshPart)
	}

	private fun transformNormals(copiedMesh: Mesh, transform: Matrix4) {
		val transformN = transform.cpy()
		transformN.setTranslation(Vector3.Zero)

		transformNormal(copiedMesh, transformN, 0, copiedMesh.numVertices, VertexAttributes.Usage.BiNormal)
		transformNormal(copiedMesh, transformN, 0, copiedMesh.numVertices, VertexAttributes.Usage.Tangent)
		transformNormal(copiedMesh, transformN, 0, copiedMesh.numVertices, VertexAttributes.Usage.Normal)
	}

	private fun invertFaceOrientation(copiedMesh: Mesh) {
		if (copiedMesh.numIndices != 0) {
			(0 until copiedMesh.numIndices / 3).forEach { triangleIndex ->
				val indexOffset1 = triangleIndex * 3 + 1
				val indexOffset2 = triangleIndex * 3 + 2
				val indexValue1 = copiedMesh.indicesBuffer[indexOffset1]
				val indexValue2 = copiedMesh.indicesBuffer[indexOffset2]

				copiedMesh.indicesBuffer.put(indexOffset1, indexValue2)
				copiedMesh.indicesBuffer.put(indexOffset2, indexValue1)
			}
		} else if (copiedMesh.numVertices != 0) {
			(0 until copiedMesh.numVertices / 3).forEach { triangleIndex ->
				throw Error("Vertices swap is not supported now")
			}
		}
	}

	private fun collectParts(model: Model): Map<MergeKey, MutableList<PartInfo>> {
		val partsMap = mutableMapOf<MergeKey, MutableList<PartInfo>>()

		val nodeList = model.getAllNodes()
		nodeList.forEach { node ->
			node.parts.forEach { part ->
				val vertexAttributes = part.meshPart.mesh.vertexAttributes
				val parts = partsMap.getOrPut(MergeKey(part.material, part.meshPart.primitiveType, vertexAttributes)) { mutableListOf() }
				parts.add(PartInfo(node, part))
			}
		}

		pushNodesToMap(model.nodes, partsMap)
		return partsMap
	}

	private fun pushNodesToMap(nodes: Iterable<Node>, partsMap: MutableMap<MergeKey, MutableList<PartInfo>>) {
		nodes.forEach { node ->
			pushNodesToMap(node.children, partsMap)

		}
	}

	private fun transformNormal(mesh: Mesh, matrix: Matrix4?, start: Int, count: Int, attribute: Int) {
		val posAttr: VertexAttribute = mesh.getVertexAttribute(attribute) ?: return
		val posOffset = posAttr.offset / 4
		val stride: Int = mesh.getVertexSize() / 4
		val numComponents = posAttr.numComponents
		val vertices = FloatArray(count * stride)
		mesh.getVertices(start * stride, count * stride, vertices)

		//	Dont support 4-dim components
		val usedComponents = min(3, numComponents)
		Mesh.transform(matrix, vertices, stride, posOffset, usedComponents, 0, count)

		mesh.updateVertices(start * stride, vertices)
	}

	fun removeEmptyItems(asset: SceneAsset) {
		asset.scene.model.nodes.toList().forEach {
			if (!it.hasChildren() && it.parts.isEmpty) {
				asset.scene.model.nodes.removeValue(it, true)
			}
		}
		asset.scene.cameras.clear()
		asset.scene.lights.clear()
	}

	fun materialToRoot(asset: SceneAsset): SceneAsset {
		val model = asset.scene.model
		if (model.materials.isEmpty) {
			model.materials.addAll(*model.getAllMaterials().toList().toTypedArray())
		}

		return asset
	}
}

