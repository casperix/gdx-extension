package casperix.renderer

import casperix.app.window.WindowWatcher
import casperix.math.color.Color
import casperix.renderer.simple.SimpleShaderProvider
import casperix.math.color.Color4d
import casperix.misc.DisposableHolder
import casperix.renderer.core.*
import casperix.renderer.core.overclocked.OverclockedModelBatch
import casperix.renderer.core.impl.SortedEntry
import casperix.renderer.core.impl.SortedRenderableMap
import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal
import casperix.signals.then
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.PerspectiveCamera
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.scene3d.scene.SceneManager
import net.mgsx.gltf.scene3d.shaders.PBRCommon
import net.mgsx.gltf.scene3d.shaders.PBRShaderProvider
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue


@OptIn(ExperimentalTime::class)
class RenderManager(val watcher: WindowWatcher, var needClearColor: Boolean, var clearColor: Color, val mainShaderProvider: ShaderProvider, val shadowShaderProvider: ShaderProvider) : DisposableHolder(), RenderableMap {

	companion object {
		/**
		 * 	Renderer use PBR technic
		 */
		fun createPBRender(watcher: WindowWatcher, clearColor: Color4d = Color4d(0.0, 0.4, 0.8, 1.0)): RenderManager {
			return RenderManager(
				watcher, true, clearColor,
				PBRShaderProvider.createDefault(PBRShaderProvider.createDefaultConfig()),
				PBRShaderProvider.createDefaultDepth(PBRShaderProvider.createDefaultDepthConfig()),
			)
		}

		/**
		 * 	Renderer use simplified experimental technic
		 */
		fun createSimpleRender(watcher: WindowWatcher, clearColor: Color4d = Color4d(0.0, 0.4, 0.8, 1.0)): RenderManager {
			val provider = SimpleShaderProvider()
			return RenderManager(
				watcher, true, clearColor,
				provider,
				provider,
			)
		}
	}

	val sortedCollection = SortedRenderableMap()

	var camera: Camera = PerspectiveCamera().apply {
		position.set(Vector3(-1f, -1f, 1f))
		direction.set(Vector3(2f, 2f, -1f))
		up.set(Vector3(0f, 0f, 1f))
	}

	val statistic = RenderedStatistic()
	val sceneManager = SceneManager()
	val environment = sceneManager.environment

	val modelMainBatchMap = mutableMapOf<RenderableGroup, OverclockedModelBatch>()// ModelBatchExt(mainShaderProvider, mainSorter)
	val modelShadowBatchMap = mutableMapOf<RenderableGroup, OverclockedModelBatch>()// ModelBatchExt(shadowShaderProvider, shadowSorter)

	@Deprecated(message = "Use onPreRender for read state or onPostRender for write state")
	val onUpdate = watcher.onUpdate
	val onPreRender = Signal<Double>()
	val onPostRender = Signal<Double>()

	init {
		updateCameras()
		var tick = 0.0
		watcher.onPreRender.then (components) {
			onPreRender.set(tick)
		}
		watcher.onUpdate.then(components) {
			tick = it
			update()
		}
		watcher.onPostRender.then (components) {
			onPostRender.set(tick)
		}
		watcher.onRender.then(components) { statistic.append(render()) }
		watcher.onResize.then(components) { resize(it.x, it.y) }
		watcher.onExit.then(components) { dispose() }
	}

	override fun dispose() {
		modelMainBatchMap.values.forEach {
			it.dispose()
		}
		modelMainBatchMap.clear()
		modelShadowBatchMap.values.forEach {
			it.dispose()
		}
		modelShadowBatchMap.clear()

		sceneManager.dispose()
	}

	private fun update() {
		statistic.nextFrame()
		val tick = Gdx.graphics.deltaTime
		sceneManager.setCamera(camera)
		sceneManager.update(tick)
	}

	fun clear(forceClearColor: Boolean) {
		val antiAliasingBits = if (Gdx.graphics.bufferFormat.coverageSampling) GL20.GL_COVERAGE_BUFFER_BIT_NV else 0
		val clearColorBits = if (needClearColor || forceClearColor) GL20.GL_COLOR_BUFFER_BIT else 0
		val floatColor = clearColor.toColor4f()
		Gdx.gl.glClearColor(floatColor.red, floatColor.green, floatColor.blue, floatColor.alpha)
		Gdx.gl.glClear(clearColorBits or GL20.GL_DEPTH_BUFFER_BIT or antiAliasingBits)
	}


	private fun render(): RenderedAmountAndTime {
		val (statistic, time) = measureTimedValue {
			clear(false)
			PBRCommon.enableSeamlessCubemaps()
			renderScene(modelMainBatchMap, { OverclockedModelBatch(mainShaderProvider) }, camera, sortedCollection.groups(false))
		}

		return RenderedAmountAndTime(statistic, time)
	}

	override fun clear() {
		sortedCollection.clear()
	}

	override fun add(instance: RenderableProvider, group: RenderableGroup) {
		sortedCollection.add(instance, group)
	}

	override fun remove(instance: RenderableProvider, group: RenderableGroup) {
		sortedCollection.remove(instance, group)
	}

	fun renderDepth(camera: Camera?): RenderedAmount {
		val activeCamera = camera ?: this.camera
		environment.set(DepthRenderPassAttribute())
		val statistic = renderScene(modelShadowBatchMap, { OverclockedModelBatch(shadowShaderProvider) }, activeCamera, sortedCollection.groups(true))
		environment.remove(DepthRenderPassAttribute.ID)
		return statistic
	}

	private fun renderScene(batchMap: MutableMap<RenderableGroup, OverclockedModelBatch>, batchProvider: () -> OverclockedModelBatch, camera: Camera, groups: List<SortedEntry>): RenderedAmount {
		var statistic = RenderedAmount(0, 0, 0)

		groups.forEach { entry ->
			val batch = batchMap.getOrPut(entry.group) { batchProvider() }
			batch.render(camera, environment, entry.providerList)
			statistic += batch.statistic
		}

		return statistic
	}

	private fun updateCameras() {
		camera.update()
	}

	private fun resize(width: Int, height: Int) {
		sceneManager.updateViewport(width.toFloat(), height.toFloat())
		updateCameras()
	}
}