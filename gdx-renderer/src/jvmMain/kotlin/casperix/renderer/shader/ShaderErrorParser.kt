package casperix.renderer.shader

import casperix.math.vector.Vector2i
import casperix.misc.console.ConsoleColors
import casperix.misc.toString

object ShaderErrorParser {
	class AnalyzedLine(val text: String, val active: Boolean)

	fun parseErrorLog(vertexPath: String, fragmentPath: String, useColorCode: Boolean, vertexShaderSource: String, fragmentShaderSource: String, rawError: String): List<String> {
		val messages = mutableListOf<String>()

		val lines = rawError.split("\n")
		lines.forEachIndexed { index, line ->
			val nextLine = lines.getOrElse(index + 1) { "" }

			if (line.startsWith("Vertex")) {
				messages += "Vertex Shader Error ($vertexPath)"
				messages += ShaderErrorParser.parse(useColorCode, vertexShaderSource, nextLine)
			} else if (line.startsWith("Fragment")) {
				messages += "Fragment Shader Error ($fragmentPath)"
				messages += ShaderErrorParser.parse(useColorCode, fragmentShaderSource, nextLine)
			}
		}
		return messages
	}

	fun parse(useColorCode:Boolean, shaderSource: String, rawError: String):List<String> {
		val errorPosition = parseErrorPosition(rawError) ?: Vector2i(-1)

		val lines = analyzeShader(shaderSource.split("\n"))

		val output = lines.flatMapIndexed { lineIndex, line ->
			val lineIndexFormatted = lineIndex.toString(3, ' ')

			if (lineIndex != errorPosition.y - 1) {
				if (!useColorCode) {
					listOf(lineIndexFormatted + ".  ${line.text}" )
				} else {
					if (line.active) {
						listOf(ConsoleColors.makeWhite(lineIndexFormatted) + "  ${line.text}")
					} else {
						listOf(ConsoleColors.makeWhite(lineIndexFormatted + "  ${line.text}"))
					}
				}
			} else {
				if (!useColorCode) {
					listOf("", lineIndexFormatted + ".  ${line.text}", "<<<$rawError>>>", "")
				} else {
					listOf(ConsoleColors.makeRed(lineIndexFormatted) + ConsoleColors.makeRed("  ${line.text} <<<"), "", ConsoleColors.makeRed(rawError), "")
				}
			}
		}
		return output
	}

	private fun analyzeShader(lines: List<String>): List<AnalyzedLine> {
		val declaredDefines = mutableSetOf<String>()

		var activeBlock = true
		val usedDefines = mutableListOf<String>()

		return lines.map { line ->
			val macro = line.substringAfter("#", "")
			val isMacro = macro != ""
			if (isMacro) {
				val idents = macro.split(Regex("\\s*"))
				if (!idents.isEmpty()) {
					val primary = idents[0]
					val secondary = idents.getOrNull(1) ?: ""
					when (primary) {
						"define" -> declaredDefines += secondary
						"ifdef" -> {
							activeBlock = declaredDefines.contains(secondary)
							usedDefines += secondary
						}

						"else" -> {
							val last = usedDefines.lastOrNull()
							activeBlock = !declaredDefines.contains(last)
						}

						"endif" -> {
							usedDefines.removeLast()
							activeBlock = usedDefines.isEmpty() || declaredDefines.contains(usedDefines.last())
						}
					}
				}
			}
			AnalyzedLine(line, activeBlock || isMacro)
		}
	}

	private fun parseErrorPosition(error: String): Vector2i? {
		val errorFirstLine = error.split(Regex("\n"), 2).firstOrNull() ?: ""
		val item = Regex("\\d+:\\d+").find(errorFirstLine)
		if (item != null) {
			val numbers = item.value.split(":").map { Integer.parseInt(it) }
			if (numbers.size != 2) return null
			return Vector2i(numbers[0], numbers[1])
		}
		return null
	}
}