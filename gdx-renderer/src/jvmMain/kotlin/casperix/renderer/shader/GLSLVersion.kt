package casperix.renderer.shader

/**
 * 	See https://en.wikipedia.org/wiki/OpenGL_Shading_Language
 */
enum class GLSLVersion(val es:Boolean, val code: String) {
	GL_1_10(false,"110"),
	GL_1_20(false,"120"),
	GL_1_30(false,"130"),
	GL_1_40(false,"140"),
	GL_1_50(false,"150"),
	GL_3_30(false,"330"),
	GL_4_00(false,"400"),
	GL_4_10(false,"410"),
	GL_4_20(false,"420"),
	GL_4_30(false,"430"),
	GL_4_40(false,"440"),
	GL_4_50(false,"450"),
	GL_4_60(false,"460"),

	GL_ES_1_00(true,"100"),
	GL_ES_3_00(true,"300 es"),
	GL_ES_3_10(true,"310 es"),
	GL_ES_3_20(true,"320 es"),
}

