package casperix.renderer.shader.setter

import casperix.renderer.shader.AbstractSetter
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix3

class Matrix3Setter<Source>(program: ShaderProgram, name: String, val provider: (source: Source) -> Matrix3) : AbstractSetter<Source>(program, name) {

	override fun set(renderContext: RenderContext, source: Source) {
//		if (locationId == -1) return
		val value = provider(source)
		program.setUniformMatrix(locationId, value)
	}
}