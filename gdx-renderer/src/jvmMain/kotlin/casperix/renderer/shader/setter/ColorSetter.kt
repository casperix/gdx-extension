package casperix.renderer.shader.setter

import casperix.renderer.shader.AbstractSetter
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class ColorSetter<Source>(program: ShaderProgram, name: String, val provider: (source: Source) -> Color?) : AbstractSetter<Source>(program, name) {

	override fun set(renderContext: RenderContext, source: Source) {
		if (locationId == -1) return
		val value = provider(source) ?: return
		program.setUniformf(locationId, value)
	}
}