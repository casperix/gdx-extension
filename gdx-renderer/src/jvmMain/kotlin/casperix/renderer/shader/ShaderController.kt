package casperix.renderer.shader

import casperix.renderer.core.overclocked.OverclockedRenderable
import casperix.renderer.shader.setter.*
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix3
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3

typealias CommonShaderSetter = ShaderSetter<Attributes>
typealias UniqueShaderSetter = ShaderSetter<Renderable>


/**
 * @param materialLocations common render settings (camera, material, environment)
 * @param transformLocations object render settings (unique for current object)
 */
class ShaderController(
	val program: ShaderProgram
) {
	val commonSetters = ShaderSetterList<Attributes>(program)
	val renderableSetters = ShaderSetterList<OverclockedRenderable>(program)
}

class ShaderSetterList<InputType>(val program: ShaderProgram) {
	val setters = mutableListOf<ShaderSetter<InputType>>()

	fun float(name: String, provider: (InputType) -> Float?) {
		setters += FloatSetter(program, name, provider)
	}

	fun matrix3(name: String, provider: (InputType) -> Matrix3) {
		setters += Matrix3Setter(program, name, provider)
	}

	fun matrix4(name: String, provider: (InputType) -> Matrix4) {
		setters += Matrix4Setter(program, name, provider)
	}

	fun vector3(name: String, provider: (InputType) -> Vector3?) {
		setters += Vector3Setter(program, name, provider)
	}

	fun vector2(name: String, provider: (InputType) -> Vector2?) {
		setters += Vector2Setter(program, name, provider)
	}

	fun color(name: String, provider: (InputType) -> Color?) {
		setters += ColorSetter(program, name, provider)
	}

	fun texture(name: String, provider: (InputType) -> TextureDescriptor<Texture>?) {
		setters += TextureSetter(program, name, provider)
	}

	fun validate() {
		setters.removeIf { !it.isValid }
	}
}

//data class CommonShaderContext(val environment: Environment, val camera: Camera, val material: Material)

interface ShaderSetter<Source> {
	fun set(renderContext: RenderContext, source: Source)
	val isValid: Boolean
}

abstract class AbstractSetter<Context>(val program: ShaderProgram, name: String) : ShaderSetter<Context> {
	val locationId = program.fetchUniformLocation(name, false)
	override val isValid = locationId != -1
}


inline fun <reified T : Attribute> Attributes.getAttribute(type: Long): T? {
	return get(T::class.java, type)
}
