package casperix.renderer.shader

import com.badlogic.gdx.Application
import com.badlogic.gdx.Gdx

object GLSLVersionCalculator {
	fun getPreferredVersion(): GLSLVersion {
		return if (Gdx.app.type == Application.ApplicationType.Desktop) {
			GLSLVersion.GL_3_30
		} else {
			GLSLVersion.GL_ES_3_10
		}
	}
}