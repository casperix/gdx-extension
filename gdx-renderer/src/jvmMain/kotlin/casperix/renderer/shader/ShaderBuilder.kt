package casperix.renderer.shader

import casperix.file.FileReference
import casperix.gdx.file.toGdxFile
import casperix.renderer.shader.ShaderErrorParser.parseErrorLog
import com.badlogic.gdx.graphics.glutils.ShaderProgram

object ShaderBuilder {
	private val cacheErrors = mutableSetOf<List<String>>()

	fun build(basePath: FileReference, defines: Set<String> = emptySet(), customVersion: GLSLVersion? = null): ShaderProgram {
		val vertexReference = basePath.copy(path = "${basePath.path}.vert")
		val fragmentReference = basePath.copy(path = "${basePath.path}.frag")
		return build(vertexReference, fragmentReference, defines, customVersion)
	}

	/**
	 * @param customVersion -- setup concrete GLSL version. Or use null, for use platform compatible version
	 */
	fun build(vertexReference: FileReference, fragmentReference:FileReference, defines: Set<String> = emptySet(),  customVersion: GLSLVersion? = null): ShaderProgram {
		val vertexHandle = vertexReference.toGdxFile()
		val fragmentHandle = fragmentReference.toGdxFile()

		val version = customVersion ?: GLSLVersionCalculator.getPreferredVersion()

		val setupBlock = "#version ${version.code}\n" +
				defines.joinToString("\n", postfix = "\n") { "#define $it" } +
				if (version.es) "precision highp float;\n" else ""


		val vertexShader = setupBlock + vertexHandle.readString()
		val fragmentShader = setupBlock + fragmentHandle.readString()

		val program = ShaderProgram(
			vertexShader,
			fragmentShader,
		)

		if (!program.isCompiled) {
			val output = parseErrorLog(vertexHandle.name(), fragmentHandle.name(), !version.es, program.vertexShaderSource, program.fragmentShaderSource, program.log)
			printIfFirst(output)
		}
		return program
	}

	private fun printIfFirst(output: List<String>) {
		if (!cacheErrors.add(output)) return
		output.forEach {
			println(it)
		}
	}


}

