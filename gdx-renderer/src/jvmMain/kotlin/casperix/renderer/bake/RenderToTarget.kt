package casperix.renderer.bake

import casperix.gdx.geometry.toVector3
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2f
import casperix.renderer.RenderManager
import casperix.renderer.core.RenderedAmount
import casperix.renderer.core.overclocked.OverclockedModelBatch
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.ShadowRender
import casperix.renderer.simple.ColorCorrectionAttribute
import casperix.renderer.simple.SimpleShader
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider
import com.badlogic.gdx.graphics.glutils.FrameBuffer
import com.badlogic.gdx.graphics.glutils.FrameBufferTextureArray
import com.badlogic.gdx.math.Matrix4

class RenderToTarget(val renderManager: RenderManager, val renderSettings: RenderSettings, val mainProvider: ShaderProvider, val shadowProvider: ShaderProvider) {
	private val mainModelBatch = OverclockedModelBatch(mainProvider)
	private val shadowModelBatch = OverclockedModelBatch(shadowProvider)
	private val spriteBatch = PolygonSpriteBatch(32).apply {
		projectionMatrix = Matrix4().setToOrtho2D(0f, 0f, renderSettings.viewportSize.x.toFloat(), renderSettings.viewportSize.y.toFloat())
	}

	val pixmapMap = mutableMapOf<FrameSettings, Pixmap>()

	private var index = 0

	fun renderToTexture(mainList: List<RenderableProvider>, shadowList: List<RenderableProvider>,  frameSettings: FrameSettings): Texture {
		val buffer = FrameBuffer(Pixmap.Format.RGBA8888, renderSettings.viewportSize.x, renderSettings.viewportSize.y, renderSettings.hasDepth, renderSettings.hasStencil)
		val shadowRender = renderManager.getComponent<ShadowRender>() ?: throw Error("Add ${ShadowRender::class} to ${RenderManager::class}")

		shadowRender.renderToDepth { lightAsCamera ->
			renderToCurrent(shadowList, frameSettings, true, lightAsCamera)
		}

		buffer.bind()
		renderToCurrent(mainList, frameSettings, false, null)
		buffer.end()

		if (renderSettings.mipmap) {
			buffer.colorBufferTexture.bind()
			Gdx.gl.glGenerateMipmap(GL20.GL_TEXTURE_2D)
		}
		return buffer.colorBufferTexture
	}

	fun renderToTextureArray(mainList: List<RenderableProvider>, shadowList: List<RenderableProvider>, frameSettingsList: List<FrameSettings>): TextureArray {
		if (frameSettingsList.isEmpty()) return TextureArray(false)

		val buffer = FrameBufferTextureArray(
			renderSettings.viewportSize.x, renderSettings.viewportSize.y, frameSettingsList.size, Pixmap.Format.RGBA8888, renderSettings.hasDepth, renderSettings.hasStencil, renderSettings.minFilter, renderSettings.magFilter, Texture.TextureWrap.ClampToEdge, Texture.TextureWrap.ClampToEdge
		)

		val shadowRender = renderManager.getComponent<ShadowRender>() ?: throw Error("Add ${ShadowRender::class} to ${RenderManager::class}")
		val shadowTextureSize = shadowRender.settings.textureSize

		frameSettingsList.forEachIndexed { index, frameSettings ->
			//	render depth to shadow texture
			shadowRender.renderToDepth { lightAsCamera ->
				renderToCurrent(shadowList, frameSettings, true, lightAsCamera)
			}
			//	render color to frame buffer texture
			buffer.bind()
			buffer.bindSide(index)
			renderToCurrent(mainList, frameSettings, false, null)
			buffer.end()
		}

		buffer.colorBufferTexture.bind()
		Gdx.gl.glGenerateMipmap(GL30.GL_TEXTURE_2D_ARRAY)
		return buffer.colorBufferTexture
	}

	private fun renderToCurrent(instances: List<RenderableProvider>, frameSettings: FrameSettings, shadowStep: Boolean, customCamera: Camera?): RenderedAmount {
		val camera = customCamera ?: createCamera(renderSettings.viewportSize.toVector2d(), renderSettings.camera, frameSettings.transform)
		val environment = renderManager.environment
		val ignoredGammaAttribute = if (!renderSettings.ignoreGamma) null else environment.getAttribute<ColorCorrectionAttribute>(ColorCorrectionAttribute.GAMMA)

		val originalClearColor = renderManager.clearColor
		renderManager.clearColor = renderSettings.backgroundColor

		if (shadowStep) {
			shadowModelBatch.render(camera, environment, instances)
		} else {
			val originalCW = SimpleShader.defaultCW
			SimpleShader.defaultCW = true
			Gdx.gl.glFrontFace(GL20.GL_CW)

			Gdx.gl.glViewport(0, 0, renderSettings.viewportSize.x, renderSettings.viewportSize.y)
			if (renderSettings.clearColor) renderManager.clear(true)

			if (ignoredGammaAttribute != null) environment.remove(ColorCorrectionAttribute.GAMMA)
			mainModelBatch.render(camera, environment, instances)
			if (ignoredGammaAttribute != null) environment.set(ignoredGammaAttribute)

			if (renderSettings.debugMode && renderSettings.debugFont != null) {
				val textOffset = Vector2f(2f, 2f)

				spriteBatch.begin()
				renderSettings.debugFont.draw(spriteBatch, "id: $index\nname: ${frameSettings.name}", textOffset.x, textOffset.y)
				spriteBatch.end()
			}

			if (renderSettings.generatePixMaps) {
				pixmapMap[frameSettings] = createFromFrameBuffer(0, 0, renderSettings.viewportSize.x, renderSettings.viewportSize.y, renderSettings.hasAlpha)
			}

			Gdx.gl.glFrontFace(GL20.GL_CCW)
			SimpleShader.defaultCW = originalCW
		}

		renderManager.clearColor = originalClearColor

		return if (shadowStep) {
			shadowModelBatch.statistic
		} else {
			mainModelBatch.statistic
		}
	}

	private fun createFromFrameBuffer(x: Int, y: Int, w: Int, h: Int, hasAlpha:Boolean): Pixmap {
		Gdx.gl.glPixelStorei(GL20.GL_PACK_ALIGNMENT, 1)
		val pixmapFormat = if (hasAlpha) Pixmap.Format.RGBA8888 else Pixmap.Format.RGB888
		val glFormat = if (hasAlpha) GL20.GL_RGBA else GL20.GL_RGB

		val pixmap = Pixmap(w, h, pixmapFormat)
		val pixels = pixmap.pixels
		Gdx.gl.glReadPixels(x, y, w, h, glFormat, GL20.GL_UNSIGNED_BYTE, pixels)
		return pixmap
	}

	private fun createCamera(viewport: Vector2d, settings: CameraSettings, transform: CameraTransform): Camera {
		val camera = when (settings.type) {
			is PerspectiveCameraType -> {
				val camera = PerspectiveCamera()
				camera.fieldOfView = settings.type.fieldOfView.toFloat()
				camera
			}

			is OrthographicCameraType -> {
				val camera = OrthographicCamera()
				camera.zoom = settings.type.zoom.toFloat()
				camera
			}
		}

		camera.near = settings.near.toFloat()
		camera.far = settings.far.toFloat()
		camera.viewportWidth = viewport.x.toFloat()
		camera.viewportHeight = viewport.y.toFloat()

		camera.position.set(transform.position.toVector3())
		camera.direction.set(transform.direction.toVector3())
		camera.up.set(transform.up.toVector3())

		camera.update(true)

		val m = camera.view.`val`
		m[Matrix4.M10] *= -1f
		m[Matrix4.M11] *= -1f
		m[Matrix4.M12] *= -1f
		m[Matrix4.M13] *= -1f
		camera.combined.set(camera.projection).mul(camera.view)

		return camera
	}
}

