package casperix.renderer.bake

import casperix.math.SphericalCoordinated
import casperix.math.geometry.RADIAN_TO_DEGREE
import casperix.math.vector.Vector3d
import casperix.renderer.RenderManager
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.utils.ShaderProvider
import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.roundToInt
import kotlin.math.sin

/**
 * 	Make few prerender, from custom orbital position
 */
class OrbitalRenderToTextureArray(
	val renderManager: RenderManager,
	val renderSettings: RenderSettings,
	mainShaderProvider: ShaderProvider,
	shadowShaderProvider: ShaderProvider,
	val providers: List<RenderableProvider>,
	val center:Vector3d,
	val positions: List<SphericalCoordinated>,
) {
	val renderer = RenderToTarget(renderManager, renderSettings, mainShaderProvider, shadowShaderProvider)
	val frameSettingsList = positions.map { generateFrameSettings(it) }
	val outputTextureArray = renderer.renderToTextureArray( providers, providers, frameSettingsList)
	val outputPixmapList = renderer.pixmapMap.values

	private fun generateFrameSettings(spherical: SphericalCoordinated): FrameSettings {
		val cartesian = spherical.fromSpherical()

		val horizontalAngle = spherical.horizontalAngle
		val verticalAngle = spherical.verticalAngle
		val cameraDirection = -cartesian.normalize()
		val right = Vector3d(cos(horizontalAngle + PI / 2.0), sin(horizontalAngle + PI / 2.0), 0.0)
		val up = right.cross(cameraDirection).normalize()

		val transform = CameraTransform(
			center + cartesian,
			cameraDirection,
			up
		)


		val hAngleDegree = horizontalAngle * RADIAN_TO_DEGREE
		val vAngleDegree = verticalAngle * RADIAN_TO_DEGREE

		val name = "ha: ${hAngleDegree.roundToInt()}; va: ${vAngleDegree.roundToInt()}"

		return FrameSettings(transform, name)
	}

}