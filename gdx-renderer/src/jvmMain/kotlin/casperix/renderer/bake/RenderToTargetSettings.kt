package casperix.renderer.bake

import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.g2d.BitmapFont

sealed class CameraType

class OrthographicCameraType(val zoom: Double) : CameraType()

class PerspectiveCameraType(val fieldOfView: Double) : CameraType()


class CameraSettings(val type: CameraType, val near: Double, val far: Double)

class CameraTransform(val position: Vector3d, val direction: Vector3d, val up: Vector3d)

data class FrameSettings(
	val transform: CameraTransform,
	val name: String = "",
)

data class RenderSettings(
	val camera: CameraSettings,
	val backgroundColor: Color,
	val viewportSize: Vector2i,
	val minFilter: TextureFilter,
	val magFilter: TextureFilter,
	val generatePixMaps: Boolean,
	val debugFont: BitmapFont?,
	val debugMode: Boolean,
	val mipmap:Boolean,
	val clearColor:Boolean = true,
	val hasDepth: Boolean = true,
	val hasStencil: Boolean = false,
	val ignoreGamma: Boolean = false,
	val hasAlpha:Boolean = true,
)