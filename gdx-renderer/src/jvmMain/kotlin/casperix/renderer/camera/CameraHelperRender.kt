package casperix.renderer.camera

import casperix.gdx.geometry.plus
import casperix.gdx.geometry.times
import casperix.gdx.graphics.GeometryBuilder
import casperix.gdx.graphics.addLine
import casperix.renderer.RenderManager
import casperix.math.geometry.Line
import casperix.misc.DisposableHolder
import casperix.signals.then
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.ModelInstance
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder.VertexInfo

class CameraHelperRender(val renderManager: RenderManager, val camera: Camera, val scale:Float = 100f) : DisposableHolder() {
	private var lastInstance: ModelInstance? = null

	init {

		val vertexAttributes = GeometryBuilder.createAttributes(VertexAttributes.Usage.Position or VertexAttributes.Usage.ColorUnpacked)


		renderManager.onUpdate.then(components) {
			if (isDisposed) return@then

			lastInstance?.let {
				renderManager.remove(it)
			}

			val cameraDebugModel = GeometryBuilder.build(vertexAttributes = vertexAttributes, geometryType = GeometryBuilder.GeometryType.LINES) { meshPartBuilder ->
				meshPartBuilder.apply {
//					addLine(
//						Line(
//							VertexInfo().apply {
//								setPos(Vector3.Zero)
//								setCol(Color.BLACK)
//							},
//							VertexInfo().apply {
//								setPos(camera.position)
//								setCol(Color.YELLOW)
//							},
//						)
//					)
					addLine(
						Line(
							VertexInfo().apply {
								setPos(camera.position)
								setCol(Color.RED)
							},
							VertexInfo().apply {
								setPos(camera.position + camera.direction * scale)
								setCol(Color.RED)
							},
						)
					)
					addLine(
						Line(
							VertexInfo().apply {
								setPos(camera.position)
								setCol(Color.BLUE)
							},
							VertexInfo().apply {
								setPos(camera.position + camera.up * scale)
								setCol(Color.BLUE)
							},
						)
					)
				}
			}

			val nextInstance = ModelInstance(cameraDebugModel)
			renderManager.add(nextInstance)
			lastInstance = nextInstance
		}
	}

	override fun dispose() {
		lastInstance?.apply {
			renderManager.remove(this)
		}
		super.dispose()
	}
}