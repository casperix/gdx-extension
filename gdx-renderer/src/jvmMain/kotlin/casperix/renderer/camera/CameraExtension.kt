package casperix.renderer.camera

import casperix.app.window.Cursor
import casperix.gdx.geometry.set
import casperix.gdx.input.GdxInputManager
import casperix.gdx.renderer.camera.update
import casperix.input.DefaultInputDispatcher
import casperix.renderer.RenderManager
import casperix.misc.Disposable
import casperix.misc.max
import casperix.scene.camera.orbital.OrbitalCameraController
import casperix.scene.camera.orbital.OrbitalCameraInputSettings
import casperix.scene.camera.orbital.OrbitalCameraTransformSettings
import casperix.signals.concrete.Future
import casperix.signals.concrete.Promise
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.PerspectiveCamera
import java.util.function.Supplier
import kotlin.math.absoluteValue

fun createOrbitalCameraController(
	renderManager: RenderManager,
	inputSettings: OrbitalCameraInputSettings,
	transformSettings: OrbitalCameraTransformSettings
): OrbitalCameraController {
	return createOrbitalCameraController(renderManager.onPostRender, renderManager.watcher.onCursor, { renderManager.camera }, inputSettings, transformSettings)
}

fun createOrbitalCameraController(
	onPostRender: Future<Double>,
	onCursor: Promise<Cursor>,
	activeCamera: Supplier<Camera>,
	inputSettings: OrbitalCameraInputSettings,
	transformSettings: OrbitalCameraTransformSettings
): OrbitalCameraController {
	val cameraInputs = DefaultInputDispatcher()
	GdxInputManager.register(cameraInputs, -1)
	val controller = OrbitalCameraController(
		onPostRender,
		onCursor,
		cameraInputs,
		null,
		inputSettings,
		transformSettings,
	) { state ->
		activeCamera.get().apply {
			viewportWidth = Gdx.graphics.width.toFloat()
			viewportHeight = Gdx.graphics.height.toFloat()

			val maxDist = max(transformSettings.maxRange.absoluteValue, transformSettings.minRange.absoluteValue).toFloat()

			up.set(state.up)
			direction.set(state.direction)
			position.set(state.position)
			if (this is OrthographicCamera) {
				zoom = state.offset.range.toFloat() * 0.001f
				near =-maxDist * 2f
				far = maxDist * 2f
				update(true, true)
			} else if (this is PerspectiveCamera) {
				near = maxDist * 0.002f
				far = maxDist * 2f
				update(true)
			}
		}
	}
	controller.components.add(object : Disposable {
		override fun dispose() {
			GdxInputManager.unregister(cameraInputs)
		}
	})
	return controller
}

