package casperix.renderer.light

import casperix.gdx.geometry.toVector3
import casperix.gdx.geometry.toVector3f
import casperix.gdx.graphics.GeometryBuilder
import casperix.gdx.graphics.addLine
import casperix.gdx.graphics.addTriangle
import casperix.renderer.RenderManager
import casperix.math.geometry.Line3f
import casperix.math.geometry.Triangle3f
import casperix.math.geometry.direction
import casperix.math.vector.Vector3f
import casperix.renderer.core.overclocked.OverclockedModelInstance
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.RenderableProvider
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder

class LightHelperRender(val renderManager: RenderManager) {
	private val RAY_LENGTH = 100f
	private var lastInstance: RenderableProvider? = null
	private val material = Material(BlendingAttribute(true, 1f))

	init {

		renderManager.onUpdate.then {
			lastInstance?.let {
				renderManager.remove(it)
			}

			val directionalLightsAttribute = renderManager.environment.get(DirectionalLightsAttribute.Type) as? DirectionalLightsAttribute ?: return@then
			val firstLight = directionalLightsAttribute.lights.firstOrNull() ?: return@then

			val pivot = Vector3f(0f, 0f, 2f)
			val target = pivot + firstLight.direction.toVector3f().normalize() * (-RAY_LENGTH)
			val projection = pivot + target.getXY().expand(0f)

			val lightModel = GeometryBuilder.build(
				GeometryBuilder.Task("", material, GeometryBuilder.GeometryType.LINES, GeometryBuilder.defaultVertexAttributes) { builder ->
					val color = Color.YELLOW
//					builder.addLine(Line3f(pivot, target).convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3()).setCol(Color.WHITE) })
					builder.addLine(Line3f(target, projection).convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3()).setCol(Color.GRAY) })
					builder.addLine(Line3f(projection, pivot).convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3()).setCol(Color.GRAY) })

					addArrow(Line3f(target, pivot)).forEach {
						builder.addLine(it.convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3()).setCol(Color.WHITE) })
					}
				},
				GeometryBuilder.Task("", material, GeometryBuilder.GeometryType.TRIANGLES, GeometryBuilder.defaultVertexAttributes) {
					it.addTriangle(Triangle3f(pivot, target, projection).convert { MeshPartBuilder.VertexInfo().setPos(it.toVector3()).setCol(1f, 1f, 1f, 0.2f) })
				},
			)
			val nextInstance = OverclockedModelInstance(lightModel)
			renderManager.add(nextInstance)
			lastInstance = nextInstance
		}
	}
}

private fun addArrow(axis: Line3f, arrowSize:Float = 5f):List<Line3f> {
	val arrowDirection = axis.direction().normalize()

	val arrowRandomUp = Vector3f.Z
	val arrowRight = arrowDirection.cross(arrowRandomUp)
	val arrowUp = arrowDirection.cross(arrowRight)

	val arrowPivot = axis.v1 - arrowDirection * arrowSize

	return listOf(
		axis,
		Line3f(axis.v1, arrowPivot + arrowRight * arrowSize * 0.2f),
		Line3f(axis.v1, arrowPivot - arrowRight * arrowSize * 0.2f),
		Line3f(axis.v1, arrowPivot + arrowUp * arrowSize * 0.2f),
		Line3f(axis.v1, arrowPivot - arrowUp * arrowSize * 0.2f),
	)

}
