package casperix.renderer.light

import casperix.misc.DisposableHolder
import casperix.gdx.geometry.toVector3
import casperix.math.vector.Vector3d
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight

class LightComponent(val environment: Environment, direction: Vector3d = Vector3d.ONE) : DisposableHolder() {
	val light = DirectionalLight()
	init {
		light.setDirection(direction.toVector3())
		environment.add(light)
	}

	override fun dispose() {
		environment.remove(light)
		super.dispose()
	}
}