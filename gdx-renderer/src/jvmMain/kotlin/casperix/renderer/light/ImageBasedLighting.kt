package casperix.renderer.light

import casperix.misc.DisposableHolder
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Cubemap
import com.badlogic.gdx.graphics.CubemapData
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import net.mgsx.gltf.scene3d.attributes.PBRCubemapAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute


class ImageBasedLighting(val brdfLUT: Texture, environment: Environment, irradianceData: CubemapData, radianceData: CubemapData) : DisposableHolder() {
	val irradianceMap: Cubemap
	val radianceMap: Cubemap

	init {
		irradianceMap = Cubemap(irradianceData)
		radianceMap = Cubemap(radianceData)
		radianceMap.setFilter(Texture.TextureFilter.MipMap, Texture.TextureFilter.Linear)

		environment.set(ColorAttribute.createAmbient(Color(1f, 1f, 1f, 1f)))
		environment.set(PBRTextureAttribute(PBRTextureAttribute.BRDFLUTTexture, brdfLUT))
		environment.set(PBRCubemapAttribute.createSpecularEnv(radianceMap))
		environment.set(PBRCubemapAttribute.createDiffuseEnv(irradianceMap))
	}

	override fun dispose() {
		super.dispose()
		irradianceMap.dispose()
		radianceMap.dispose()
		brdfLUT.dispose()
	}
}