package casperix.renderer.shadow

import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3f

data class ShadowRenderSettings(
	val viewSize: Vector2f = Vector2f(100f),
	val viewRange: Float = 100f,
	val viewUp: Vector3f = Vector3f.Z,
	val textureFormat: DepthPrecisionFormat = DepthPrecisionFormat.F16,
	val textureSize: Vector2i = Vector2i(512),
)