package casperix.renderer.shadow

import casperix.gdx.geometry.set
import casperix.gdx.geometry.toVector3f
import casperix.misc.DisposableHolder
import casperix.renderer.RenderManager
import casperix.renderer.core.DepthRenderPassAttribute
import casperix.renderer.core.RenderedAmount
import casperix.renderer.core.RenderedAmountAndTime
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3
import kotlin.time.ExperimentalTime
import kotlin.time.measureTimedValue

@OptIn(ExperimentalTime::class)
class ShadowRender(
	val renderManager: RenderManager,
	val light: DirectionalLight,
	settings: ShadowRenderSettings,
) : DisposableHolder() {
	val settingsObserver = StorageSignal(settings)
	var settings: ShadowRenderSettings
		get() {
			return settingsObserver.value
		}
		set(value) {
			settingsObserver.value = value
		}

	val camera = OrthographicCamera()

	private var isActive = true
	private val environment = renderManager.environment
	private var frameBuffer: ShadowFrameBuffer? = null
	private val textureDesc = TextureDescriptor<Texture>()
	private val center = Vector3()


	init {
		renderManager.components += this
		renderManager.environment.add(light)
		renderManager.onPreRender.then(components) { renderManager.statistic.append(renderToDepthDefault()) }

		settingsObserver.then(components) { updateAll() }

		environment.set(ShadowTextureAttribute(textureDesc))
	}

	override fun dispose() {
		renderManager.components -= this
		frameBuffer?.dispose()
		frameBuffer = null
		super.dispose()
	}

	private fun updateAll() {
		settingsObserver.value.apply {
			val lastFrameBuffer = frameBuffer
			if (lastFrameBuffer == null || lastFrameBuffer.width != textureSize.x || lastFrameBuffer.height != textureSize.y || lastFrameBuffer.format != textureFormat) {
				rebuildFrameBuffer()
			}

			camera.viewportWidth = viewSize.x
			camera.viewportHeight = viewSize.y
			camera.near = 1f
			camera.far = viewRange + 1f
			camera.update(true)
		}
	}

	private fun rebuildFrameBuffer() {
		settingsObserver.value.apply {
			frameBuffer?.dispose()
			frameBuffer = ShadowFrameBuffer(textureFormat, textureSize.x, textureSize.y)
		}

		textureDesc.magFilter = Texture.TextureFilter.Nearest
		textureDesc.minFilter = Texture.TextureFilter.Nearest
		textureDesc.vWrap = Texture.TextureWrap.ClampToEdge
		textureDesc.uWrap = Texture.TextureWrap.ClampToEdge
		textureDesc.texture = frameBuffer!!.colorBufferTexture

	}

	private fun renderToDepthDefault():RenderedAmountAndTime {
		if (!isActive) return RenderedAmountAndTime.ZERO
		return renderToDepth {
			renderManager.renderDepth(it)
		}
	}

	fun renderToDepth(render:(Camera)->RenderedAmount):RenderedAmountAndTime {
		val frameBuffer = frameBuffer ?: return RenderedAmountAndTime.ZERO

		val (statistic, time ) = measureTimedValue {
			environment.set(ShadowLightAttribute(camera.combined))
			environment.set(DepthRenderPassAttribute())

			begin(frameBuffer)
			val statistic = render(camera)
			end(frameBuffer)

			environment.remove(DepthRenderPassAttribute.ID)
			statistic
		}

		return RenderedAmountAndTime(statistic, time)
	}

	fun setActive(value: Boolean) {
		if (isActive == value) return
		isActive = value

		if (value) {
			environment.set(ShadowTextureAttribute(textureDesc))
		} else {
			environment.remove(ShadowTextureAttribute.ID)
		}
	}

	fun setCenter(center: Vector3?): ShadowRender {
		this.center.set(center)
		return this
	}

	fun setCenter(x: Float, y: Float, z: Float): ShadowRender {
		center[x, y] = z
		return this
	}

	protected fun updateCameraTransform() {
		val position = center.toVector3f() - light.direction.toVector3f() * (settings.viewRange * 0.5f + 1f)

		camera.position.set(position)
		camera.direction.set(light.direction).nor()
		camera.up.set(settings.viewUp)
		camera.normalizeUp()
		camera.update()
	}

	fun begin(frameBuffer: ShadowFrameBuffer) {
		updateCameraTransform()

		frameBuffer.begin()
		Gdx.gl.glViewport(0, 0, frameBuffer.width, frameBuffer.height)
		Gdx.gl.glClearColor(1f, 1f, 1f, 1f)
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT or GL20.GL_DEPTH_BUFFER_BIT)
		Gdx.gl.glEnable(GL20.GL_SCISSOR_TEST)
		Gdx.gl.glScissor(1, 1, frameBuffer.width - 2, frameBuffer.height - 2)
	}

	fun end(frameBuffer: ShadowFrameBuffer) {
		Gdx.gl.glDisable(GL20.GL_SCISSOR_TEST)
		frameBuffer.end()
	}

	fun getProjViewTrans(): Matrix4 {
		return camera.combined
	}

	fun getDepthMap(): TextureDescriptor<*> {
		return textureDesc
	}

}
