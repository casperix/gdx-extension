package casperix.renderer.shadow

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.utils.TextureDescriptor

data class ShadowTextureAttribute(val depthTexture: TextureDescriptor<Texture>) : Attribute(ID) {
	companion object {
		val ID = register("shadowTexture")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ShadowTextureAttribute(depthTexture)
	}

}