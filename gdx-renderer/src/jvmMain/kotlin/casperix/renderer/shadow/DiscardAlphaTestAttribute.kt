package casperix.renderer.shadow

import com.badlogic.gdx.graphics.g3d.Attribute

class DiscardAlphaTestAttribute(val enabled:Boolean, val threshold:Float = 0.5f) : Attribute(ID) {
	companion object {
		val ID = register("discardAlphaTest")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return DiscardAlphaTestAttribute(enabled, threshold)
	}

}