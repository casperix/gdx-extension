package casperix.renderer.shadow

import casperix.gdx.geometry.Intersection
import casperix.gdx.geometry.toVector3
import casperix.gdx.geometry.toVector3d
import casperix.math.geometry.Line3d
import casperix.math.geometry.Plane3d
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector3d
import casperix.misc.clamp
import casperix.misc.toPrecision
import casperix.renderer.RenderManager
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import kotlin.math.absoluteValue
import kotlin.math.atan
import kotlin.math.atan2

class PlaneShadowAdapter(val renderManager: RenderManager, val shadowRender: ShadowRender, val light: DirectionalLight) {
	init {
		renderManager.watcher.onPostRender.then {
			update()
		}
	}

	fun update() {
		val camera = renderManager.camera
		val cameraRay = camera.run {
			Line3d(
				position.toVector3d(),
				direction.toVector3d() * far.toDouble()
			)
		}
		val plane = Plane3d(Vector3d.Z, 0.0)
		val intersection = Intersection.lineWithPlane(cameraRay, plane)
		if (intersection != null) {
			val toCamera = (camera.position.toVector3d() - intersection)

			val heightFactor = (1.4 + 0.7 * toCamera.length() / (toCamera.z.absoluteValue + 0.001)).toFloat()

			val lightDirection = light.direction.toVector3d()
			val heightAspect = atan2(lightDirection.z.absoluteValue, lightDirection.getXY().length()).clamp(0.5, 1.0).toFloat()

//			val bias = 0.001// 4.0 / shadowRender.settings.textureSize.x
//			println("heightAspect: " + heightAspect.toPrecision(2) + "; bias: ${bias.toPrecision(6)}")
//			renderManager.environment.set(ShadowAttribute(ShadowAttribute.BIAS, true, bias.toFloat()))

			val range = toCamera.length().toFloat() * heightFactor
			shadowRender.setCenter(intersection.toVector3())
			shadowRender.settings = shadowRender.settings.copy(viewSize = Vector2f(range, range  * heightAspect), viewRange = range)
		}
	}
}