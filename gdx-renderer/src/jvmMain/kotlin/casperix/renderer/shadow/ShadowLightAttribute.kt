package casperix.renderer.shadow

import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.math.Matrix4

data class ShadowLightAttribute(val lightProjectionView: Matrix4) : Attribute(ID) {
	companion object {
		val ID = register("shadowLight")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ShadowLightAttribute(lightProjectionView)
	}

}