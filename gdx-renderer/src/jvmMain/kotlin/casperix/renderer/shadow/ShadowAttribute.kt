package casperix.renderer.shadow

import com.badlogic.gdx.graphics.g3d.Attribute

data class ShadowAttribute(val type:Long, val enabled:Boolean, val value:Float = 1f) : Attribute(type) {
	companion object {
		val DEBUG = register("ShadowAttributeDebug")
		val BIAS = register("ShadowAttributeBias")
		val PCF = register("ShadowAttributePCF")
		val RECEIVER = register("ShadowAttributeReceiver")
		val CASTER = register("ShadowAttributeCaster")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ShadowAttribute(type, enabled, value)
	}

}