package casperix.renderer.shadow

import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.glutils.FrameBuffer

class ShadowFrameBuffer(val format: DepthPrecisionFormat, width: Int, height: Int) : FrameBuffer(build(format, width, height)) {


	companion object {
		private fun build(format: DepthPrecisionFormat, width: Int, height: Int): FrameBufferBuilder {
			val depthFormat = when (format) {
				DepthPrecisionFormat.F16 -> GL30.GL_DEPTH_COMPONENT16
				DepthPrecisionFormat.F32 -> GL30.GL_DEPTH_COMPONENT32F
			}
			val colorFormat = when (format) {
				DepthPrecisionFormat.F16 -> GL30.GL_R16F
				DepthPrecisionFormat.F32 -> GL30.GL_R32F
			}

			val bufferBuilder = FrameBufferBuilder(width, height)
			bufferBuilder.addDepthRenderBuffer(depthFormat)
			bufferBuilder.addColorTextureAttachment(colorFormat, GL30.GL_RED, GL20.GL_FLOAT)
			return bufferBuilder
		}
	}

}