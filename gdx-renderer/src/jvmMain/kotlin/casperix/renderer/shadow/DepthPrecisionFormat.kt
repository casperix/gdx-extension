package casperix.renderer.shadow

enum class DepthPrecisionFormat {
	F16,
	F32,
}