package casperix.renderer

import casperix.math.SphericalCoordinated
import casperix.renderer.light.ImageBasedLighting
import casperix.renderer.misc.SkyBox
import casperix.renderer.shadow.ShadowRender
import casperix.renderer.shadow.ShadowRenderSettings
import casperix.renderer.camera.createOrbitalCameraController
import casperix.scene.camera.orbital.OrbitalCameraController
import casperix.scene.camera.orbital.OrbitalCameraInputSettings
import casperix.scene.camera.orbital.OrbitalCameraTransformSettings
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Cubemap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight
import com.badlogic.gdx.graphics.glutils.FacedCubemapData
import com.badlogic.gdx.graphics.glutils.KTXTextureData
import kotlin.math.PI


object SceneEnvironment {
	fun default(renderManager: RenderManager) {
		orbitalCamera(renderManager)
		imageBasedLighting(renderManager)
		skybox(renderManager)
	}

	fun shadow(renderManager: RenderManager): ShadowRender {
		val light = DirectionalLight()
		return ShadowRender(renderManager, light, ShadowRenderSettings())
	}

	fun orbitalCamera(renderManager: RenderManager, inputSettings: OrbitalCameraInputSettings = OrbitalCameraInputSettings(), transformSettings: OrbitalCameraTransformSettings = OrbitalCameraTransformSettings()): OrbitalCameraController {
		val controller = createOrbitalCameraController( renderManager, inputSettings, transformSettings)
		controller.transformer.setOffset(SphericalCoordinated(10.0, PI / 4.0, 0.0))
		return controller
	}

	fun imageBasedLighting(renderManager: RenderManager) {
		ImageBasedLighting(
			Texture(Gdx.files.internal("environment/brdfLUT.png")),
			renderManager.environment,
			FacedCubemapData(
				Gdx.files.internal("environment/irradiance_posx.jpg"),
				Gdx.files.internal("environment/irradiance_negx.jpg"),
				Gdx.files.internal("environment/irradiance_posy.jpg"),
				Gdx.files.internal("environment/irradiance_negy.jpg"),
				Gdx.files.internal("environment/irradiance_posz.jpg"),
				Gdx.files.internal("environment/irradiance_negz.jpg"),
			),
			KTXTextureData(Gdx.files.internal("environment/radiance.ktx"), false)
		)
	}

	fun skybox(renderManager: RenderManager) {
		SkyBox(
			renderManager, Cubemap(
				FacedCubemapData(
					Gdx.files.internal("environment/skybox_posx.jpg"),
					Gdx.files.internal("environment/skybox_negx.jpg"),
					Gdx.files.internal("environment/skybox_posy.jpg"),
					Gdx.files.internal("environment/skybox_negy.jpg"),
					Gdx.files.internal("environment/skybox_posz.jpg"),
					Gdx.files.internal("environment/skybox_negz.jpg"),
				)
			)
		)
	}
}