package casperix.renderer.billboard

import casperix.math.axis_aligned.Box2d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.math.Matrix4

data class BillboardData(val material: Material, val uvArea:Box2d, val size:Vector2d, val screenOffset: Vector3d)