package casperix.renderer.billboard

import casperix.renderer.core.DebugModeAttribute
import casperix.renderer.shadow.DiscardAlphaTestAttribute
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

class BillboardMaterialInfo(
	val debugMode: Boolean,
	val blending: BlendingAttribute?,
	val discardAlphaTest: Float?,
	val diffuseColor:Color?,
	val diffuseTexture: Texture?,
	val normalTexture: Texture?,
	val shadowCastTexture: Texture?,
) {
	val key = BillboardMaterialKey(debugMode, blending?.blended, blending?.opacity, discardAlphaTest, diffuseTexture?.textureObjectHandle, normalTexture?.textureObjectHandle, shadowCastTexture?.textureObjectHandle)

	companion object {
		fun from(material: Material):BillboardMaterialInfo {
			val debugModeAttribute = material.get(DebugModeAttribute.ID) as? DebugModeAttribute
			val alphaTestAttribute = material.get(DiscardAlphaTestAttribute.ID) as? DiscardAlphaTestAttribute
			val alphaTest = if (alphaTestAttribute != null && alphaTestAttribute.enabled) alphaTestAttribute.threshold else null
			val diffuseColor = material.get(ColorAttribute.Diffuse) as? ColorAttribute
			val diffuseTexture = material.get(TextureAttribute.Diffuse) as? TextureAttribute
			val normalTexture = material.get(TextureAttribute.Normal) as? TextureAttribute
			val ambientTexture = material.get(BillboardShadowCastTextureAttribute.ID) as? TextureAttribute
			val blending = material.get(BlendingAttribute.Type) as? BlendingAttribute

			return BillboardMaterialInfo(
				debugModeAttribute?.enabled ?: false,
				blending,
				alphaTest,
				diffuseColor?.color,
				diffuseTexture?.textureDescription?.texture,
				normalTexture?.textureDescription?.texture,
				ambientTexture?.textureDescription?.texture,
			)
		}
	}
}


