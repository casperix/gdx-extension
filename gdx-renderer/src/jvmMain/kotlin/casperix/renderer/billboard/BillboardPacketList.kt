package casperix.renderer.billboard

import casperix.misc.Disposable
import casperix.misc.disposeAll

class BillboardPacketList(val type: BillboardTypeInfo) : Disposable {
	val packets = mutableListOf<BillboardPacket>()

	override fun dispose() {
		packets.disposeAll()
	}
}