package casperix.renderer.billboard

import casperix.math.vector.Vector3d
import casperix.scene.SceneObject

class BillboardObject(val position: Vector3d, val data: BillboardData) : SceneObject