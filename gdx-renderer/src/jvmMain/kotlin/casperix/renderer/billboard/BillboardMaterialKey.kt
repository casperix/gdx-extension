package casperix.renderer.billboard

data class BillboardMaterialKey(
	val debugMode: Boolean,
	val blending: Boolean?,
	val opacity: Float?,
	val discardAlphaTest: Float?,
	val diffuseTextureHandle: Int?,
	val normalTextureHandle: Int?,
	val shadowCastTextureHandle: Int?,
)