package casperix.renderer.billboard

import casperix.renderer.util.Material
import casperix.renderer.util.asAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

class BillboardTypeInfo(val type: BillboardMaterialInfo) {
	val material = Material(
		listOfNotNull(
			type.diffuseTexture?.asAttribute(TextureAttribute.Diffuse),
			type.normalTexture?.asAttribute(TextureAttribute.Normal),
			type.shadowCastTexture?.asAttribute(BillboardShadowCastTextureAttribute.ID),
			type.blending,
		)
	)
	val shader = BillboardShader(type)
}