package casperix.renderer.billboard

import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.Attribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute

class BillboardShadowCastTextureAttribute(val texture: Texture) : TextureAttribute(ID, texture) {
	companion object {
		val ID = register("BillboardShadowCast")

		init {
			Mask = Mask or ID
		}

		fun create(texture: Texture): BillboardShadowCastTextureAttribute {
			return BillboardShadowCastTextureAttribute(texture)
		}
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return BillboardShadowCastTextureAttribute(texture)
	}

}