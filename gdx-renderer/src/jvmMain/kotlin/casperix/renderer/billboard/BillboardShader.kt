package casperix.renderer.billboard

import casperix.file.FileReference
import casperix.gdx.geometry.toMatrix4
import casperix.gdx.geometry.toVector3d
import casperix.math.matrix.Matrix4d
import casperix.renderer.core.DepthRenderPassAttribute
import casperix.renderer.core.RenderedAmount
import casperix.renderer.core.overclocked.OverclockedRenderable
import casperix.renderer.shader.ShaderBuilder
import casperix.renderer.shader.ShaderController
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.ShadowLightAttribute
import casperix.renderer.simple.SimpleShaderProvider
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import com.badlogic.gdx.math.Matrix4

class BillboardShader(val type: BillboardMaterialInfo) : Shader {
	private val shaderDefines = run {
		val defines = mutableSetOf<String>()
		if (type.debugMode) defines += "DEBUG_MODE"
		if (type.discardAlphaTest != null) defines += "DISCARD_ALPHA_TEST"
		if (type.diffuseTexture != null) defines += "DIFFUSE_TEXTURE"
		if (type.normalTexture != null) defines += "NORMAL_TEXTURE"
		if (type.shadowCastTexture != null) defines += "SHADOW_CAST_TEXTURE"
		defines.toSet()
	}

	private val defaultController = createController(ShaderBuilder.build(FileReference.classpath("shaders/billboard"), shaderDefines), false)
	private val shadowController = createController(ShaderBuilder.build(FileReference.classpath("shaders/billboardDepth"), shaderDefines), true)

	private var currentCamera: Camera? = null
	private var currentContext: RenderContext? = null
	private var currentRenderable: Renderable? = null
	private var currentProgram: ShaderProgram? = null
	private var currentMesh: Mesh? = null

	private fun createController(program: ShaderProgram, shadowCreationPhase: Boolean): ShaderController {
		val controller = ShaderController(program)
		controller.commonSetters.apply {
			if (type.discardAlphaTest != null) {
				float("u_discard_alpha_test_threshold") {
					type.discardAlphaTest
				}
			}

			texture("u_diffuse_texture") {
				val attribute: TextureAttribute? = it.getAttribute(TextureAttribute.Diffuse)
				attribute?.textureDescription
			}

			vector3("u_camera_up") {
				currentCamera?.up
			}

			vector3("u_camera_right") {
				currentCamera?.run {
					direction.cpy().crs(up)
				}
			}

			vector3("u_camera_direction") {
				currentCamera?.direction
			}

			if (shadowCreationPhase) {
				matrix4("u_light_projection_view_matrix") {
					it.getAttribute<ShadowLightAttribute>(ShadowLightAttribute.ID)?.lightProjectionView?.cpy() ?: Matrix4()
				}
				texture("u_billboard_shadow_cast_texture") {
					val attribute: TextureAttribute? = it.getAttribute(BillboardShadowCastTextureAttribute.ID) ?: it.getAttribute(TextureAttribute.Diffuse)
					attribute?.textureDescription
				}
			} else {
				matrix4("u_projection_view_matrix") {
					currentCamera?.run {
						combined
					} ?: Matrix4()
				}
				texture("u_normal_texture") {
					val attribute: TextureAttribute? = it.getAttribute(TextureAttribute.Normal)
					attribute?.textureDescription
				}

				vector3("u_light_direction_in_camera_space") {
					val camera = currentCamera
					val lightAttributes: DirectionalLightsAttribute? = it.getAttribute(DirectionalLightsAttribute.Type)
					if (lightAttributes == null || camera == null) {
						null
					} else {
						val cameraZ = camera.direction.toVector3d()
						val cameraY = camera.up.toVector3d()
						val cameraX = cameraZ.cross(cameraY)
						val cameraTransform = Matrix4d.createByAxis(cameraX, cameraY, cameraZ).toMatrix4()

						val light = lightAttributes.lights.first()
						val toLightDirection = light.direction.cpy();
						toLightDirection.mul(cameraTransform)
						toLightDirection.scl(-1f, -1f, 1f)
						toLightDirection
					}
				}
			}
		}

		controller.renderableSetters.apply {
			matrix4("u_model_matrix") {
				it.worldTransform
			}
		}

		return controller
	}

	override fun dispose() {

	}

	override fun init() {

	}

	override fun begin(camera: Camera?, context: RenderContext?) {
		if (camera == null || context == null) return
		currentContext = context
		currentCamera = camera
	}

	override fun render(renderable: Renderable) {
		val renderContext = currentContext ?: return

		val attributes = SimpleShaderProvider.getAttributes(renderable)
		val depthRenderAttribute: DepthRenderPassAttribute? = attributes.getAttribute(DepthRenderPassAttribute.ID)
		val shadowIteration = depthRenderAttribute != null

		val controller = if (shadowIteration) shadowController else defaultController

		controller.program.bind()
		renderContext.setCullFace(GL20.GL_BACK)
		renderContext.setDepthTest(GL20.GL_LEQUAL)
		renderContext.setDepthMask(true)

		if (!shadowIteration) {
			val blendingAttribute = attributes.getAttribute<BlendingAttribute>(BlendingAttribute.Type)
			if (blendingAttribute == null) {
				renderContext.setBlending(false, GL20.GL_ONE, GL20.GL_ONE)
			} else {
				renderContext.setBlending(blendingAttribute.blended, blendingAttribute.sourceFunction, blendingAttribute.destFunction)
				if (blendingAttribute.blended) {
					Gdx.gl.glBlendFuncSeparate(blendingAttribute.sourceFunction, blendingAttribute.destFunction, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)
				}
			}
		} else {
			renderContext.setBlending(false, GL20.GL_ONE, GL20.GL_ONE)
		}

		val overclocked = OverclockedRenderable.wrap(renderable)

		controller.commonSetters.setters.forEach { it.set(renderContext, attributes) }
		controller.renderableSetters.setters.forEach { it.set(renderContext, overclocked) }

		val meshPart = renderable.meshPart
		val mesh = meshPart?.mesh ?: return
//		mesh.bind(shader)
		mesh.render(controller.program, meshPart.primitiveType, 0, mesh.numIndices, true)
		currentMesh = mesh

		accumulatedBatches++
		accumulatedElements++
		accumulatedVertices += mesh.numIndices
	}

	private fun unbind() {
		val program = currentProgram
		val mesh = currentMesh

		if (mesh != null && program != null) {
			mesh.unbind(program)
		}

		currentCamera = null
		currentMesh = null
		currentProgram = null
		currentRenderable = null
		currentContext = null
	}


	override fun end() {
		unbind()
	}

	override fun compareTo(other: Shader?): Int {
		if (other == null) return -1
		if (other === this) return 0
		return 1
	}

	override fun canRender(instance: Renderable?): Boolean {
		return instance?.shader == this
	}

	companion object {
		private var accumulatedBatches = 0
		private var accumulatedElements = 0
		private var accumulatedVertices = 0

		fun dropStatistic(): RenderedAmount {
			val result = RenderedAmount(accumulatedVertices, accumulatedElements, accumulatedBatches)
			accumulatedVertices = 0
			accumulatedElements = 0
			accumulatedBatches = 0
			return result
		}
	}
}