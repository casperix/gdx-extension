package casperix.renderer.billboard

import casperix.renderer.core.overclocked.OverclockedModelInstance
import casperix.renderer.core.overclocked.OverclockedRenderable
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.model.MeshPart
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.graphics.g3d.model.NodePart

object BillboardRenderableBuilder {

	fun build(packetListMap: MutableMap<BillboardMaterialKey, BillboardPacketList>): List<OverclockedRenderable> {
		val shaderMap = mutableMapOf<Material, Shader>()
		val model = Model()
		packetListMap.forEach { (_, stack) ->
			stack.packets.forEach { container ->
				val mesh = container.builder.mesh
				val info = stack.type
				val size = if (mesh.numIndices != 0) mesh.numIndices else mesh.numVertices
				val meshPart = MeshPart("", container.builder.mesh, 0, size, GL20.GL_TRIANGLES)
				val nodePart = NodePart(meshPart, info.material)
				shaderMap[info.material] = info.shader
				val node = Node()
				node.parts.add(nodePart)
				model.nodes.add(node)
			}
		}

		val nextInstance = OverclockedModelInstance(model)
		val nextRenderableList = nextInstance.createRenderableList()
		nextRenderableList.forEach {
			it.shader = shaderMap.get(it.material)
		}
		return nextRenderableList
	}
}