package casperix.renderer.billboard

import casperix.misc.Disposable
import casperix.misc.disposeAll
import casperix.renderer.core.overclocked.OverclockedRenderable
import casperix.renderer.core.overclocked.OverclockedRenderableProvider
import com.badlogic.gdx.graphics.Color

/**
 * 	Billboard renderer.
 * 	Add this instance to [RenderManager]
 * 	Just add billboards using the [addBillboard]
 */
class BillboardRenderer(val name: String = "") : OverclockedRenderableProvider, Disposable {
	override var version = 0L

	private val packetListMap = mutableMapOf<BillboardMaterialKey, BillboardPacketList>()
	private val billboardMap = mutableMapOf<BillboardObject,  BillboardPacket>()
	private var renderableList: List<OverclockedRenderable>? = null

	override fun dispose() {
		packetListMap.values.disposeAll()
	}

	fun removeAll() {
		packetListMap.forEach { (_, entry)->
			entry.packets.forEach {
				it.removeAll()
			}
		}
	}

	fun removeBillboard(billboard: BillboardObject):Boolean {
		val packet = billboardMap.remove(billboard) ?: return false
		if (!packet.billboardSet.remove(billboard)) return false
		packet.dirty = true
		version++
		return false
	}

	fun addBillboard(billboard: BillboardObject) {
		val info = BillboardMaterialInfo.from(billboard.data.material)

		val packetList = packetListMap.getOrPut(info.key) { BillboardPacketList(BillboardTypeInfo(info)) }
		var packet = packetList.packets.firstNotNullOfOrNull { if (!it.builder.isFull()) it else null }
		if (packet == null) {
			packet = BillboardPacket()
			packetList.packets += packet
			renderableList = null
		}

		val color = info.diffuseColor ?: Color.WHITE

		billboardMap.put(billboard, packet)
		packet.billboardSet += billboard
		packet.builder.addParticle(billboard.position, billboard.data.uvArea, billboard.data.size, billboard.data.screenOffset, color)
		version++
	}

	private fun updateAllBuffer() {
		packetListMap.forEach { (_, stack) ->
			stack.packets.forEach { buffer ->
				buffer.update()
			}
			stack.packets.removeIf {
				if (it.builder.isEmpty()) {
					renderableList = null
					it.dispose()
					true
				} else {
					false
				}
			}
		}
	}

	private fun updateModel() {
		if (renderableList != null) return
		renderableList = BillboardRenderableBuilder.build(packetListMap)
	}

	override fun createRenderableList(): List<OverclockedRenderable> {
		updateAllBuffer()
		updateModel()
		return renderableList.orEmpty()
	}
}

