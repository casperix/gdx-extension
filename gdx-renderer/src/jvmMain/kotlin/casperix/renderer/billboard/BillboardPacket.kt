package casperix.renderer.billboard

import casperix.misc.Disposable
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute

class BillboardPacket : Disposable {
	var dirty = false
	val builder = BillboardMeshBuilder()
	val billboardSet = mutableSetOf<BillboardObject>()

	override fun dispose() {
		builder.dispose()
		billboardSet.clear()
	}

	fun update() {
		if (dirty) {
			dirty = false
			rebuild()
		}
		builder.updateBuffer()
	}

	private fun rebuild() {
		builder.clear()
		billboardSet.forEach { billboard ->
			val diffuseColor = billboard.data.material.get(ColorAttribute.Diffuse) as? ColorAttribute
			val color = diffuseColor?.color ?: Color.WHITE
			builder.addParticle(billboard.position, billboard.data.uvArea, billboard.data.size, billboard.data.screenOffset, color)
		}
	}

	fun removeAll() {
		builder.clear()
		billboardSet.clear()
	}
}