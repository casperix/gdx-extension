package casperix.renderer.billboard

import casperix.math.axis_aligned.Box2d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.misc.Disposable
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.VertexAttributes

/**
 * 	Building a mesh to render a particle system.
 * 	Internal class. See [BillboardRenderer]
 */
class BillboardMeshBuilder : Disposable {
	companion object {
		private const val MAX_PARTICLE_AMOUNT = 16384
		private const val INDICES_PER_PARTICLE = 6
		private const val VERTICES_PER_PARTICLE = 4
		private const val FLOAT_SIZE = 4

		private val indices:ShortArray = ShortArray(MAX_PARTICLE_AMOUNT * INDICES_PER_PARTICLE).apply {
			for (p in 0 until MAX_PARTICLE_AMOUNT) {
				val indexOffset = p * 6
				val vertexOffset = p * 4
				this[indexOffset + 0] = (vertexOffset + 0).toShort()
				this[indexOffset + 1] = (vertexOffset + 1).toShort()
				this[indexOffset + 2] = (vertexOffset + 2).toShort()
				this[indexOffset + 3] = (vertexOffset + 2).toShort()
				this[indexOffset + 4] = (vertexOffset + 1).toShort()
				this[indexOffset + 5] = (vertexOffset + 3).toShort()
			}
		}

		private val vertexAttributes = VertexAttributes(
			VertexAttribute(VertexAttributes.Usage.Position, 3, "a_center"),
			VertexAttribute(VertexAttributes.Usage.Generic, 3, "a_offset"),
			VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, "a_tex"),
			VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, "a_color"),
		)

		private val floatsInVertex = vertexAttributes.vertexSize / FLOAT_SIZE
	}

	private val vertices = FloatArray(MAX_PARTICLE_AMOUNT * VERTICES_PER_PARTICLE * floatsInVertex)
	private var usedParticleAmount = 0

	private var dirty = false

	val mesh = Mesh(false, true, vertices.size, indices.size, vertexAttributes)
//	val renderer = MeshRender(mesh, name, shader = shader, material = material)

	override fun dispose() {
		mesh.dispose()
	}

	fun updateBuffer() {
		if (!dirty) return
		dirty = false

		mesh.setVertices(vertices, 0, usedParticleAmount * VERTICES_PER_PARTICLE * floatsInVertex)
		mesh.setIndices(indices, 0, usedParticleAmount * INDICES_PER_PARTICLE)
	}

	fun clear() {
		dirty = true
		usedParticleAmount = 0
	}

	fun isEmpty():Boolean {
		return usedParticleAmount == 0
	}

	fun isFull(): Boolean {
		return usedParticleAmount >= MAX_PARTICLE_AMOUNT
	}

	fun addParticle(position: Vector3d, uvArea: Box2d, size: Vector2d, screenOffset: Vector3d, color: Color) {
		setParticle(usedParticleAmount++, position, uvArea, size, screenOffset, color)
	}

	private fun setParticle(particleIndex: Int, position: Vector3d, uvArea: Box2d, size: Vector2d, screenOffset: Vector3d, color: Color) {
		if (particleIndex >= MAX_PARTICLE_AMOUNT) throw Error("Max expected particleIndex is $MAX_PARTICLE_AMOUNT, but actual is $particleIndex")
		dirty = true

		for (dx in 0..1) {
			val textureU = if (dx == 0) uvArea.min.x else uvArea.max.x
			val vertexOffsetX =  (dx.toFloat() - 0.5f + screenOffset.x.toFloat()) * size.x.toFloat()

			for (dy in 0..1) {
				val textureV = 1.0 - if (dy == 0) uvArea.min.y else uvArea.max.y
				val vertexOffsetY = (dy.toFloat() - 0.5f + screenOffset.y.toFloat()) * size.y.toFloat()

				val bufferOffset = (particleIndex * 4 + dx + dy * 2) * floatsInVertex
				vertices[bufferOffset + 0] = position.x.toFloat()
				vertices[bufferOffset + 1] = position.y.toFloat()
				vertices[bufferOffset + 2] = position.z.toFloat()
				vertices[bufferOffset + 3] =vertexOffsetX
				vertices[bufferOffset + 4] = vertexOffsetY
				vertices[bufferOffset + 5] = screenOffset.z.toFloat()
				vertices[bufferOffset + 6] = textureU.toFloat()
				vertices[bufferOffset + 7] = textureV.toFloat()

				val packedColor = color.toIntBits()
				vertices[bufferOffset + 8] = Float.fromBits(packedColor)
			}
		}
	}
}