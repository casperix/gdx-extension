package casperix.renderer.imposter

import casperix.misc.Disposable
import com.badlogic.gdx.graphics.TextureArray
import com.badlogic.gdx.graphics.g3d.Attribute

data class ImposterModel(val textureArray: TextureArray, val settings: ImposterSettings) : Attribute(imposterAttribute), Disposable {
	companion object {
		val imposterAttribute = register("imposterAttribute")
	}

	override fun dispose() {
		textureArray.dispose()
	}

	override fun compareTo(other: Attribute?): Int {
		if (other == this) return 0
		return -1
	}

	override fun copy(): Attribute {
		return ImposterModel(textureArray, settings)
	}

	override fun equals(other: Any?): Boolean {
		if (this === other) return true
		if (javaClass != other?.javaClass) return false
		if (!super.equals(other)) return false

		other as ImposterModel

		if (textureArray != other.textureArray) return false
		if (settings != other.settings) return false

		return true
	}

	override fun hashCode(): Int {
		var result = super.hashCode()
		result = 31 * result + textureArray.hashCode()
		result = 31 * result + settings.hashCode()
		return result
	}


}