package casperix.renderer.imposter

import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3d
import com.badlogic.gdx.graphics.g2d.BitmapFont

class ImposterSettings(
	val textureSize: Vector2i,
	val rotationCenter:Vector3d,
	val cameraRange: Double,
	val horizontal: RangeInfo,
	val vertical: RangeInfo,
	val hasDepth: Boolean,
	val hasStencil: Boolean,
	val debugMode: Boolean,
	val debugFont: BitmapFont?
) {
	class RangeInfo(val minAngle: Double, val maxAngle: Double, val frames: Int) {
		init {
			if (frames <= 0) throw Error("frames must be positive number")
		}
	}

	val textureAmount get() = vertical.frames * horizontal.frames
}