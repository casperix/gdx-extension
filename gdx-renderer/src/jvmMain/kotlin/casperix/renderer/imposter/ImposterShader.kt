package casperix.renderer.imposter

import casperix.file.FilePathType
import casperix.file.FileReference
import casperix.renderer.shader.ShaderBuilder
import casperix.misc.time.getTimeMs
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.DiscardAlphaTestAttribute
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram
import net.mgsx.gltf.scene3d.lights.DirectionalShadowLight

class ImposterShader : Shader {
	private val receiveShadow = false
	private val defaultShader = ShaderBuilder.build(FileReference.classpath("shaders/imposter"))
	private val withShadowShader = ShaderBuilder.build(FileReference.classpath("shaders/imposter"), setOf("shadowMapFlag"))
	private val shadowCreatorShader = ShaderBuilder.build(FileReference.classpath("shaders/imposterShadow"))


	private var currentCamera: Camera? = null
	private var currentContext: RenderContext? = null
	private var currentRenderable: Renderable? = null
	private var currentProgram: ShaderProgram? = null
	private var currentMesh: Mesh? = null
	private val startTime = getTimeMs()

	override fun dispose() {

	}

	override fun init() {

	}

	override fun begin(camera: Camera?, context: RenderContext?) {
		if (camera == null || context == null) return
		currentContext = context
		currentCamera = camera
	}

	private fun bind(renderable: Renderable) {
		val context = currentContext ?: return
		val camera = currentCamera ?: return
		val environment = renderable.environment ?: return
		val shadowMap: DirectionalShadowLight? = environment.shadowMap as? DirectionalShadowLight
		val attribute = renderable.material.get(ImposterModel.imposterAttribute) as? ImposterModel ?: return

		val shadowIteration = shadowMap != null && shadowMap.camera == camera
		val program = if (shadowIteration) shadowCreatorShader else if (shadowMap == null) defaultShader else withShadowShader
		val vertical = attribute.settings.vertical
		val horizontal = attribute.settings.horizontal

		program.bind()
		context.setCullFace(GL20.GL_FRONT)
//		context.setDepthTest(GL20.GL_ALWAYS)
		context.setDepthTest(GL20.GL_LEQUAL)
//		context.setDepthMask(false)

//		shader.setUniformf("u_camera_up", camera.up)
//		shader.setUniformf("u_camera_direction", camera.direction)
		program.setUniformf("u_camera_position", camera.position)
		program.setUniformi("u_imposter_texture_array", context.textureBinder.bind(attribute.textureArray))

		val time = (getTimeMs() - startTime) * 0.001f
		program.setUniformf("u_time", time)
		program.setUniformf("u_wind_strength", 0.0f)

		if (shadowIteration) {
			context.setBlending(false, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)

			val temp = camera.combined.cpy()
			temp.mul(renderable.worldTransform)
			program.setUniformMatrix("u_projection_view_world_transposed", temp)
		} else {
			context.setBlending(true, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)

			program.setUniformMatrix("u_projection_view_transposed", camera.combined)
		}
		val discardAlphaTestAttribute: DiscardAlphaTestAttribute? = renderable.material.getAttribute(DiscardAlphaTestAttribute.ID)
		program.setUniformf("u_discard_alpha_test_threshold", discardAlphaTestAttribute?.threshold ?: 0.2f)

		program.setUniformf("u_vertical.lastFrame", (vertical.frames - 1).toFloat())
		program.setUniformf("u_vertical.minAngle", vertical.minAngle.toFloat())
		program.setUniformf("u_vertical.maxAngle", vertical.maxAngle.toFloat())

		program.setUniformf("u_horizontal.lastFrame", (horizontal.frames - 1).toFloat())
		program.setUniformf("u_horizontal.minAngle", horizontal.minAngle.toFloat())
		program.setUniformf("u_horizontal.maxAngle", horizontal.maxAngle.toFloat())

		currentRenderable = renderable
		currentProgram = program
	}

	private fun render() {
		val program = currentProgram ?: return
		val mesh = currentRenderable?.meshPart?.mesh ?: return
		mesh.bind(program)
		mesh.render(program, GL20.GL_TRIANGLES, 0, mesh.numIndices, false)
		currentMesh = mesh
	}

	private fun unbind() {
		val program = currentProgram
		val mesh = currentMesh
		val context = currentContext

		if (mesh != null && program != null) {
			mesh.unbind(program)
		}


		currentMesh = null
		currentProgram = null
		currentRenderable = null
		currentContext = null
	}

	override fun render(renderable: Renderable?) {
		if (renderable == null) return
		bind(renderable)
		render()
	}

	override fun end() {
		unbind()
	}

	override fun compareTo(other: Shader?): Int {
		if (other == null) return -1
		if (other === this) return 0
		return 1
	}

	override fun canRender(instance: Renderable?): Boolean {
		return instance?.shader == this
	}
}