package casperix.renderer.imposter

import casperix.math.SphericalCoordinated
import casperix.math.color.Color
import casperix.math.color.ColorCode
import casperix.misc.max
import casperix.renderer.RenderManager
import casperix.renderer.bake.CameraSettings
import casperix.renderer.bake.OrbitalRenderToTextureArray
import casperix.renderer.bake.OrthographicCameraType
import casperix.renderer.bake.RenderSettings
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture.TextureFilter
import com.badlogic.gdx.graphics.g3d.RenderableProvider

class ImposterFactory(val renderManager: RenderManager, val providers: List<RenderableProvider>, val settings: ImposterSettings) {
	fun build(): ImposterModel {
		val factory = prepare(false)
		return ImposterModel(factory.outputTextureArray, settings)
	}

	fun buildPixmap(): Collection<Pixmap> {
		val factory = prepare(true)
		factory.outputTextureArray.dispose()
		return factory.outputPixmapList
	}

	private fun createCameraSettings(): CameraSettings {
		val range = settings.cameraRange
		val zoom = 1.0 / max(settings.textureSize.x, settings.textureSize.y) * range

		return CameraSettings(
			OrthographicCameraType(zoom),
			1.0,
			range * 2.0
		)
	}

	private fun prepare(generatePixMaps: Boolean): OrbitalRenderToTextureArray {
		val positions = mutableListOf<SphericalCoordinated>()

		settings.apply {
			for (vStep in 0 until vertical.frames) {
				for (hStep in 0 until horizontal.frames) {
					val hAngle = if (horizontal.frames == 1) horizontal.minAngle else (horizontal.maxAngle - horizontal.minAngle) * hStep / (horizontal.frames - 1).toFloat() + horizontal.minAngle
					val vAngle = if (vertical.frames == 1) vertical.minAngle else (vertical.maxAngle - vertical.minAngle) * vStep / (vertical.frames - 1).toFloat() + vertical.minAngle
					positions += SphericalCoordinated(settings.cameraRange, vAngle, hAngle)
				}
			}
		}

		val clearColor = if (settings.debugMode) {
			ColorCode.BLUE
		} else {
			ColorCode.BLACK.setAlpha(0)
		}

		return OrbitalRenderToTextureArray(
			renderManager,
			RenderSettings(
				createCameraSettings(),
				clearColor,
				settings.textureSize,
				TextureFilter.MipMapNearestLinear,
				TextureFilter.Linear,
				generatePixMaps,
				settings.debugFont,
				settings.debugMode,
				settings.hasDepth,
				settings.hasStencil,
			),
			renderManager.mainShaderProvider,
			renderManager.shadowShaderProvider,
			providers,
			settings.rotationCenter,
			positions
		)
	}


}

