package casperix.renderer.misc

import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder
import kotlin.reflect.KMutableProperty
import kotlin.reflect.jvm.isAccessible

@Deprecated(message = "Optimize not needed more")
object MeshBuilderHack {
	private val members = MeshBuilder::class.members
	private val item = members.first { it.name == "indicesMap" } as KMutableProperty<*>

	fun dropIndicesMap() {
		item.isAccessible = true
		item.setter.call(null)
		item.isAccessible = false
	}

}