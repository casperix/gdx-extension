package casperix.renderer.misc

import casperix.misc.Disposable
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.graphics.Cubemap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g3d.attributes.CubemapAttribute
import net.mgsx.gltf.scene3d.attributes.PBRCubemapAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute
import net.mgsx.gltf.scene3d.scene.SceneManager
import net.mgsx.gltf.scene3d.utils.EnvironmentUtil

@Deprecated(message = "Use SceneEnvironment")
class GLTFEnvironment(sceneManager: SceneManager, paths: GLTFEnvironmentPaths = GLTFEnvironmentPaths()) : Disposable {
	private val brdfLUT: Texture
	private val specularCubemap: Cubemap
	private val diffuseCubemap: Cubemap
	val environmentCubemap: Cubemap

	init {
		// setup IBL (image based lighting)
		environmentCubemap = EnvironmentUtil.createCubemap(InternalFileHandleResolver(),
				paths.environmentPath, "_0.png", EnvironmentUtil.FACE_NAMES_FULL)

		diffuseCubemap = EnvironmentUtil.createCubemap(InternalFileHandleResolver(),
				paths.diffusePath, "_0.jpg", EnvironmentUtil.FACE_NAMES_FULL)
		specularCubemap = EnvironmentUtil.createCubemap(InternalFileHandleResolver(),
				paths.specularPath, "_", ".jpg", 10, EnvironmentUtil.FACE_NAMES_FULL)
		brdfLUT = Texture(Gdx.files.internal(paths.brdfPath))

//		sceneManager.setAmbientLight(1f)
		sceneManager.environment.set(PBRTextureAttribute(PBRTextureAttribute.BRDFLUTTexture, brdfLUT))
		sceneManager.environment.set(PBRCubemapAttribute.createSpecularEnv(specularCubemap))
		sceneManager.environment.set(PBRCubemapAttribute.createDiffuseEnv(diffuseCubemap))
		sceneManager.environment.set(CubemapAttribute(CubemapAttribute.EnvironmentMap, environmentCubemap))

	}

	override fun dispose() {
		environmentCubemap.dispose()
		diffuseCubemap.dispose()
		specularCubemap.dispose()
		brdfLUT.dispose()
	}
}