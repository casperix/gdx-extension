package casperix.renderer.atlas

import casperix.math.vector.Vector2i
import casperix.util.upload
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture

class AtlasPage(val pageSize: Vector2i, val format:Pixmap.Format) {
	private val pixmap = Pixmap(pageSize.x, pageSize.y, format)
	val texture = Texture(pixmap, true)

	private var minFilter = Texture.TextureFilter.MipMapNearestLinear
	private var magFilter = Texture.TextureFilter.Nearest

	init {
		setFilters(minFilter, magFilter)
	}

	fun setFilters(minFilter: Texture.TextureFilter, magFilter: Texture.TextureFilter) {
		this.minFilter = minFilter
		this.magFilter = magFilter
		texture.setFilter(minFilter, magFilter)
	}

	fun setup(items:List<AtlasPageItem>) {
		items.forEachIndexed {index,it->
			if (it.pixmap.isDisposed) throw Error("Pixmap is invalid (disposed)")
			if (!it.position.greaterOrEq(Vector2i.ZERO)) throw Error("Pixmap left-top corner is outside: ${it.position}")
			if (!(it.position + Vector2i(it.pixmap.width, it.pixmap.height)).lessOrEq(pageSize)) throw Error("Pixmap right-bottom corner is outside: ${it.position}")
			pixmap.drawPixmap(it.pixmap, it.position.x, it.position.y, 0, 0, it.pixmap.width, it.pixmap.height)
		}
		texture.upload()
		Gdx.gl.glGenerateMipmap(GL20.GL_TEXTURE_2D)
	}

}