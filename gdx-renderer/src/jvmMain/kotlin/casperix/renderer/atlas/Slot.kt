package casperix.renderer.atlas

import casperix.math.vector.Vector2i
import casperix.renderer.util.getSize
import casperix.util.packer.PackRegion

class Slot(val material: MaterialMergeInfo) : PackRegion {
	override var position = Vector2i.ZERO
	override val size =  material.diffuseTexture.getSize()
}