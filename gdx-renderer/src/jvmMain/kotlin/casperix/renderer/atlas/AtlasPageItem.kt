package casperix.renderer.atlas

import casperix.math.vector.Vector2i
import com.badlogic.gdx.graphics.Pixmap

class AtlasPageItem(val position: Vector2i, val pixmap: Pixmap)