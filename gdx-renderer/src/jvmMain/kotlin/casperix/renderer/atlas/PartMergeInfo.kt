package casperix.renderer.atlas

import com.badlogic.gdx.graphics.g3d.model.MeshPart

class PartMergeInfo(val material: MaterialMergeInfo, val parts: Set<MeshPart>)