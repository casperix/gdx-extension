package casperix.renderer.atlas

import com.badlogic.gdx.graphics.Texture

data class MaterialMergeInfo(val diffuseTexture: Texture, val normalTexture: Texture, val pbrTexture:Texture)