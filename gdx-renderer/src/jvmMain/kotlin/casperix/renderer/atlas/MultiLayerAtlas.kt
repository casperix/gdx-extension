package casperix.renderer.atlas

import casperix.math.axis_aligned.Box2f
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.util.getPixmap
import casperix.util.packer.NaivePacker
import casperix.util.packer.Packer
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.PixmapIO
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute

class MultiLayerAtlas {
	private val atlasSize = Vector2i(2048)

	private val diffusePage = AtlasPage(atlasSize, Pixmap.Format.RGBA8888)
	private val normalPage = AtlasPage(atlasSize, Pixmap.Format.RGB888)
	private val pbrPage = AtlasPage(atlasSize, Pixmap.Format.RGB888)

	private val usedMaterials = mutableSetOf<MaterialMergeInfo>()
	private var slots = emptyList<Slot>()
	private val commonMaterial = Material(
		"multi-layer-common",
		PBRColorAttribute.createBaseColorFactor(Color.WHITE),
		TextureAttribute.createDiffuse(diffusePage.texture),
		TextureAttribute.createNormal(normalPage.texture),
		TextureAttribute.createSpecular(pbrPage.texture),
	)
	private var dirty = false
	private val packer: Packer = NaivePacker


	companion object {
		private var debugSaveId = 0
		var debugSaveAtlas = false
	}



	fun update() {
		if (!dirty) return
		dirty = false
		val diffuseItems = slots.map { AtlasPageItem(it.position, it.material.diffuseTexture.getPixmap()) }
		val normalItems = slots.map { AtlasPageItem(it.position, it.material.normalTexture.getPixmap()) }
		val pbrItems = slots.map { AtlasPageItem(it.position, it.material.pbrTexture.getPixmap()) }

		if (debugSaveAtlas) {
			println("${MultiLayerAtlas::class.simpleName} update... ${diffuseItems.size} / ${normalItems.size} / ${pbrItems.size}")
		}

		diffusePage.setup(diffuseItems)
		normalPage.setup(normalItems)
		pbrPage.setup(pbrItems)

		if (debugSaveAtlas) {
			debugSaveId++
			PixmapIO.writePNG(Gdx.files.external("temp/atlas_${debugSaveId}_diffuse.png"), diffusePage.texture.getPixmap())
			PixmapIO.writePNG(Gdx.files.external("temp/atlas_${debugSaveId}_normal.png"), normalPage.texture.getPixmap())
			PixmapIO.writePNG(Gdx.files.external("temp/atlas_${debugSaveId}_specular.png"), pbrPage.texture.getPixmap())
		}
	}

	fun pack(materialList: List<MaterialMergeInfo>): List<Box2f> {
		val lastAmount = usedMaterials.size
		usedMaterials += materialList

		if (usedMaterials.size != lastAmount) {
			repackTextures()
		}

		return materialList.map { material ->
			val slot = slots.first { it.material == material }
			val area = Box2f.byDimension(slot.position.toVector2f(), slot.size.toVector2f())

			val u1 = area.min.x / atlasSize.x
			val v1 = area.min.y / atlasSize.y
			val u2 = (area.max.x) / atlasSize.x
			val v2 = (area.max.y) / atlasSize.y

			Box2f(Vector2f(u1, v1), Vector2f(u2, v2))
		}
	}

	private fun repackTextures() {
		dirty = true
		slots = usedMaterials.map { Slot(it) }
		val packed = packer.pack(atlasSize, slots)
		if (!packed) throw Error("Oh. Cant place all textures in atlas")
	}

	fun getMaterial(): Material {
		return commonMaterial
	}
}