package net.mgsx.gltf.loaders.gltf

import com.badlogic.gdx.assets.AssetDescriptor
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader
import com.badlogic.gdx.assets.loaders.FileHandleResolver
import com.badlogic.gdx.assets.loaders.TextureLoader.TextureParameter
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.utils.Array
import com.badlogic.gdx.utils.ObjectMap
import net.mgsx.gltf.data.GLTF
import net.mgsx.gltf.data.texture.GLTFSampler
import net.mgsx.gltf.data.texture.GLTFTexture
import net.mgsx.gltf.loaders.shared.GLTFLoaderBase
import net.mgsx.gltf.loaders.shared.GLTFTypes
import net.mgsx.gltf.loaders.shared.SceneAssetLoaderParameters
import net.mgsx.gltf.loaders.shared.texture.ImageResolver
import net.mgsx.gltf.loaders.shared.texture.TextureResolver
import net.mgsx.gltf.scene3d.scene.SceneAsset

class GLTFAssetLoaderHacked(resolver: FileHandleResolver? = InternalFileHandleResolver()) : AsynchronousAssetLoader<SceneAsset, SceneAssetLoaderParameters?>(resolver) {
	private inner class ManagedTextureResolver(private val glModel: GLTF) : TextureResolver() {
		private val textureDescriptorsSimple = ObjectMap<Int, AssetDescriptor<Texture>>()
		private val textureDescriptorsMipMap = ObjectMap<Int, AssetDescriptor<Texture>>()
		private val pixmaps = ObjectMap<Int, Pixmap>()
		private val textureParameters = ObjectMap<Int, TextureParameter>()
		override fun loadTextures(glTextures: Array<GLTFTexture>, glSamplers: Array<GLTFSampler>, imageResolver: ImageResolver) {}
		fun fetch(manager: AssetManager) {
			for (e in textureDescriptorsSimple) {
				texturesSimple.put(e.key, manager.get(e.value))
			}
			for (e in textureDescriptorsMipMap) {
				texturesMipmap.put(e.key, manager.get(e.value))
			}
		}

		fun loadTextures() {
			for (entry in textureResolver!!.textureParameters) {
				val params = entry.value
				val glTexure = glTextures[entry.key!!]
				val pixmap = pixmaps.get(glTexure.source)
				val texture = Texture(pixmap, params.genMipMaps)
				texture.setFilter(params.minFilter, params.magFilter)
				texture.setWrap(params.wrapU, params.wrapV)
				if (params.genMipMaps) {
					texturesMipmap.put(entry.key, texture)
				} else {
					texturesSimple.put(entry.key, texture)
				}
			}
//			for (entry in pixmaps) {
//				entry.value.dispose()
//			}
		}

		fun getDependencies(deps: Array<AssetDescriptor<*>?>) {
			glTextures = glModel.textures
			glSamplers = glModel.samplers
			if (glTextures != null) {
				for (i in 0 until glTextures.size) {
					val glTexture = glTextures[i]
					val glImage = glModel.images[glTexture.source]
					val imageFile = dataFileResolver!!.getImageFile(glImage)
					val textureParameter = TextureParameter()
					if (glTexture.sampler != null) {
						val sampler = glSamplers[glTexture.sampler]
						if (GLTFTypes.isMipMapFilter(sampler)) {
							textureParameter.genMipMaps = true
						}
						GLTFTypes.mapTextureSampler(textureParameter, sampler)
					} else {
						GLTFTypes.mapTextureSampler(textureParameter)
					}
					if (imageFile == null) {
						var pixmap = pixmaps.get(glTexture.source)
						if (pixmap == null) {
							pixmaps.put(glTexture.source, dataFileResolver!!.load(glImage).also { pixmap = it })
						}
						textureParameters.put(i, textureParameter)
					} else {
						val assetDescriptor = AssetDescriptor(imageFile, Texture::class.java, textureParameter)
						deps.add(assetDescriptor)
						if (textureParameter.genMipMaps) {
							textureDescriptorsMipMap.put(glTexture.source, assetDescriptor)
						} else {
							textureDescriptorsSimple.put(glTexture.source, assetDescriptor)
						}
					}
				}
			}
		}
	}

	private var dataFileResolver: SeparatedDataFileResolver? = null
	private var textureResolver: ManagedTextureResolver? = null
	override fun loadAsync(
		manager: AssetManager, fileName: String, file: FileHandle,
		parameter: SceneAssetLoaderParameters?
	) {
		textureResolver!!.fetch(manager)
	}

	override fun loadSync(
		manager: AssetManager, fileName: String, file: FileHandle,
		parameter: SceneAssetLoaderParameters?
	): SceneAsset {
		val withData = parameter != null && parameter.withData
		textureResolver!!.loadTextures()
		val loader = GLTFLoaderBase(textureResolver)
		val sceneAsset = loader.load(dataFileResolver, withData)

		// Delegates texture disposal to AssetManager.
		val deps = manager.getDependencies(fileName)
		if (deps != null) {
			for (depFileName in deps) {
				val dep = manager.get<Any>(depFileName)
				if (dep is Texture) {
					sceneAsset.textures.removeValue(dep, true)
				}
			}
		}
		textureResolver = null
		dataFileResolver = null
		return sceneAsset
	}

	override fun getDependencies(fileName: String?, file: FileHandle?, parameter: SceneAssetLoaderParameters?): Array<AssetDescriptor<*>?> {
		val deps = Array<AssetDescriptor<*>?>()
		dataFileResolver = SeparatedDataFileResolver()
		dataFileResolver!!.load(file)
		val glModel = dataFileResolver!!.root
		textureResolver = ManagedTextureResolver(glModel)
		textureResolver!!.getDependencies(deps)
		return deps
	}

}