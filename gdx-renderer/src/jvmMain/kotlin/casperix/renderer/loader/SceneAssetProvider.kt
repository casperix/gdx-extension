package casperix.renderer.loader

import casperix.renderer.atlas.MultiLayerAtlas
import casperix.renderer.util.*
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.model.Node
import com.badlogic.gdx.math.Quaternion
import com.badlogic.gdx.math.Vector3
import net.mgsx.gltf.loaders.glb.GLBAssetLoader
import net.mgsx.gltf.loaders.gltf.GLTFAssetLoaderHacked
import net.mgsx.gltf.scene3d.scene.SceneAsset

object SceneAssetProvider {
	var manualAtlasUpdate = false
	var debugMergeInfo = false

	private val atlasManager = MultiLayerAtlas()
	private val assetManager = AssetManager()

	init {
		assetManager.setLoader(SceneAsset::class.java, ".gltf", GLTFAssetLoaderHacked())
		assetManager.setLoader(SceneAsset::class.java, ".glb", GLBAssetLoader())
	}

	fun getInternalAsset(path: String): SceneAsset {
		return getAsset(Gdx.files.internal(path))
	}

	fun updateAtlas() {
		atlasManager.update()
	}

	fun getInternalModifiedModel(path: String, modification: SceneAssetModification = SceneAssetModification()): Model {
		return getModifiedAsset(Gdx.files.internal(path), modification).scene.model
	}

	fun getInternalModifiedAsset(path: String, modification: SceneAssetModification = SceneAssetModification()): SceneAsset {
		val file = Gdx.files.internal(path)
		return getModifiedAsset(file, modification)
	}

	fun getAsset(file: FileHandle): SceneAsset {
		if (!file.exists())
			throw Error("File not exist: ${file.path()}")

		return try {
			assetManager.load(file.path(), SceneAsset::class.java)
			assetManager.finishLoadingAsset<SceneAsset>(file.path())
		} catch (cause: Throwable) {
			throw Error("Error in loading ${file.path()}", cause)
		}
	}

	fun getModifiedModel(file: FileHandle, modification: SceneAssetModification = SceneAssetModification()): Model {
		return getModifiedAsset(file, modification).scene.model
	}

	fun getModifiedAsset(file: FileHandle, modification: SceneAssetModification = SceneAssetModification()): SceneAsset {
		val template = getAsset(file)
		val asset = template.copy()
		val model = asset.scene.model
		val originalNodeAmount = model.getAllNodes().size

		try {
			model.calculateTransforms()

			if (modification.convertMaterials != null) {
				ModelModification.transformMaterial(model, modification.convertMaterials)
			}
			if (modification.transformAsset != null) {
				modification.transformAsset.invoke(asset)
			}
			if (modification.mergeMaterials) {
				ModelMaterialMerger.merge(asset, atlasManager)
			}
			if (modification.mergeMeshes) {
				ModelMerger.vertexAttributesUnification(model)
				ModelMerger.mergeModel(model)
			}
			if (modification.removeEmpty) {
				ModelMerger.removeEmptyItems(asset)
			}
			if (modification.materialToRoot) {
				ModelMerger.materialToRoot(asset)
			}
			if (!manualAtlasUpdate) {
				updateAtlas()
			}

			val finalNodeAmount = model.getAllNodes().size
			if (debugMergeInfo && originalNodeAmount != finalNodeAmount) {
				println(file.path() + " merge from $originalNodeAmount to $finalNodeAmount")
			}

			AssetChecker.validateAsset(asset)
		} catch (cause: Throwable) {
			throw Error("Error in prepare ${file.path()}", cause)
		}

		return asset
	}

	fun createAdapter(model: Model): Node {
		val root = Node()

		model.nodes.forEach { node ->
			if (node.parent == null) {
				root.addChild(node)
			}
		}

		model.nodes.clear()
		model.nodes.add(root)

		return root
	}

	fun scaleAdapter(model: Model, scale: Float): Node {
		val root = createAdapter(model)
		root.scale.set(Vector3(scale, scale, scale))
		root.calculateTransforms(true)
		return root
	}

	fun fbxAdapter(model: Model, scale: Float = 0.001f, rotate: Boolean = true): Model {
		val root = Node()

		model.nodes.forEach { node ->
			if (node.parent == null) {
				root.addChild(node)
			}
		}

		root.scale.set(Vector3(scale, scale, scale))
		if (rotate) {
			root.rotation.set(Quaternion().set(Vector3.X, 90f))
		}
		root.calculateTransforms(true)

		model.nodes.clear()
		model.nodes.add(root)

		return model
	}

}

