package casperix.renderer.loader

import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import net.mgsx.gltf.scene3d.scene.SceneAsset

/**
 * 	Can modify asset immediately after load (before merge optimization)
 */
class SceneAssetModification(
	val convertMaterials: ((Material) -> Material)? = null,
	val mergeMeshes: Boolean = true,
	val removeEmpty: Boolean = true,
	val materialToRoot: Boolean = true,
	val mergeMaterials: Boolean = false,
	val transformAsset: ((SceneAsset) -> Unit)? = null,
)