package casperix.renderer.loader

import casperix.renderer.util.getAllMaterials
import casperix.renderer.util.getAllMeshes
import com.badlogic.gdx.graphics.g3d.Model
import net.mgsx.gltf.scene3d.scene.SceneAsset

object AssetChecker {
	fun validateAsset(asset: SceneAsset) {
		val errors = mutableListOf<String>()
		asset.scenes.forEach { scene ->
			val model = scene.model
			validateModel(model, errors)
		}

		if (errors.isNotEmpty()) {
			throw Error(errors.joinToString("\n"))
		}
	}


	private fun validateModel(model: Model, errors: MutableList<String>) {
		val meshList = model.getAllMeshes()
		val materialList = model.getAllMaterials()

		if (model.meshes.toSet() != meshList) {
			errors += "Invalid mesh list"
		}

		if (model.materials.toSet() != materialList) {
			errors += "Invalid material list"
		}
	}
}