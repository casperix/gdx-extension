package casperix.renderer.simple

import casperix.file.FilePathType
import casperix.file.FileReference
import casperix.renderer.shader.ShaderBuilder
import casperix.renderer.shader.ShaderController
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.ShadowAttribute
import casperix.renderer.shadow.ShadowTextureAttribute
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Environment
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.graphics.g3d.utils.BaseShaderProvider
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute
import kotlin.math.roundToInt

class SimpleShaderProvider() : BaseShaderProvider() {

	private val controllers = mutableMapOf<SimpleShaderKey, ShaderController>()

	fun getController(shadowCreator: Boolean, attributes: Attributes, vertexAttributes: VertexAttributes): ShaderController {
		return getController(createKey(shadowCreator, attributes, vertexAttributes))
	}

	fun getController(key: SimpleShaderKey): ShaderController {
		return controllers.getOrPut(key) {
			createEntry(key)
		}
	}

	override fun createShader(renderable: Renderable): Shader {
		val vertexAttributes = renderable.meshPart.mesh.vertexAttributes
		val attributes = getAttributes(renderable)
		val shadow = getController(createKey(true, attributes, vertexAttributes))
		val colored = getController(createKey(false, attributes, vertexAttributes))
		return SimpleShader(this, shadow, colored)
	}

	companion object {
		private val simpleDepthFile = FileReference.classpath("shaders/simpleDepth")
		private val simpleColoredFile = FileReference.classpath("shaders/simpleColored")

		fun getAttributes(environment: Environment, material: Material): Attributes {
			val attributes = Attributes()
			attributes.set(environment)
			attributes.set(material)

			return attributes

		}

		fun getAttributes(renderable: Renderable): Attributes {
			val nextEnvironment = renderable.environment
			val nextMaterial = renderable.material

			val attributes = Attributes()
			if (nextEnvironment != null) attributes.set(nextEnvironment)
			if (nextMaterial != null) attributes.set(nextMaterial)

			return attributes
		}

		private fun Color.fastHash(): Int {
			return (((((r * 255f) + g) * 255f + b) * 255f) + a).roundToInt()
		}

		fun createKey(shadowCreator: Boolean, attributes: Attributes, vertexAttributes: VertexAttributes): SimpleShaderKey {
			val diffuseColorAttribute: ColorAttribute? = attributes.getAttribute(ColorAttribute.Diffuse)
			val baseColorAttribute: PBRColorAttribute? = attributes.getAttribute(PBRColorAttribute.BaseColorFactor)
			val diffuseTextureAttribute: TextureAttribute? = attributes.getAttribute(TextureAttribute.Diffuse)
			val normalTextureAttribute: TextureAttribute? = attributes.getAttribute(TextureAttribute.Normal)
			val specularTextureAttribute: TextureAttribute? = attributes.getAttribute(TextureAttribute.Specular)
			val MRTextureAttribute: TextureAttribute? = attributes.getAttribute(PBRTextureAttribute.MetallicRoughnessTexture)
			val shadowTextureAttribute: ShadowTextureAttribute? = attributes.getAttribute(ShadowTextureAttribute.ID)
			val blendingAttribute: BlendingAttribute? = attributes.getAttribute(BlendingAttribute.Type)
			val hasColor = vertexAttributes.findByUsage(VertexAttributes.Usage.ColorUnpacked)
			val hasNormal = vertexAttributes.findByUsage(VertexAttributes.Usage.Normal)
			val shadowDebug: ShadowAttribute? = attributes.getAttribute(ShadowAttribute.DEBUG)
			val shadowPcf: ShadowAttribute? = attributes.getAttribute(ShadowAttribute.PCF)
			val shadowReceiver: ShadowAttribute? = attributes.getAttribute(ShadowAttribute.RECEIVER)
			val colorCorrectionHdr: ColorCorrectionAttribute? = attributes.getAttribute(ColorCorrectionAttribute.HDR)
			val colorCorrectionGamma: ColorCorrectionAttribute? = attributes.getAttribute(ColorCorrectionAttribute.GAMMA)

			val additionalHashCode = (diffuseColorAttribute?.color?.fastHash() ?: 0) + (baseColorAttribute?.color?.fastHash() ?: 0)

			return SimpleShaderKey(
				additionalHashCode,
				shadowDebug?.enabled ?: false,
				shadowCreator,
				hasNormal != null,
				hasColor != null,
				shadowTextureAttribute != null,
				diffuseTextureAttribute != null,
				normalTextureAttribute != null,
				specularTextureAttribute != null || MRTextureAttribute != null,
				diffuseColorAttribute != null || baseColorAttribute != null,
				shadowReceiver?.enabled ?: true,
				shadowPcf?.enabled ?: false,
				colorCorrectionHdr?.enabled ?: false,
				colorCorrectionGamma?.enabled ?: false,
				blendingAttribute?.blended ?: false,
			)
		}

		fun createEntry(key: SimpleShaderKey): ShaderController {
			val defines = mutableSetOf<String>()
			if (key.debugShadow) defines += "DEBUG_SHADOW"
			if (key.hasVertexNormal) defines += "CALCULATE_LIGHT"
			if (key.hasVertexColor) defines += "VERTEX_COLOR"

			if (key.hasNormalTexture) defines += "NORMAL_TEXTURE"
			if (key.hasDiffuseTexture) defines += "DIFFUSE_TEXTURE"
			if (key.hasSpecularTexture) defines += "SPECULAR_TEXTURE"
			if (key.hasDiffuseTexture && key.blend) defines += "ALPHA_BLEND_TEXTURE"
			if (key.hasDiffuseColor) defines += "DIFFUSE_COLOR"
			if (key.hasShadowTexture && key.receiveShadow) defines += "RECEIVE_SHADOW"
			if (key.pcf) defines += "PCF_SHADOW"
			if (key.hdr) defines += "HDR_CORRECTION"
			if (key.gamma) defines += "GAMMA_CORRECTION"

			val basePath = if (key.shadowCreationPhase) simpleDepthFile else simpleColoredFile
			val program = ShaderBuilder.build(basePath, defines)

			return ShaderController(program)
		}
	}
}

