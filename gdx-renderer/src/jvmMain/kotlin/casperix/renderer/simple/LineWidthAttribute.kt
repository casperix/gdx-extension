package casperix.renderer.simple

import com.badlogic.gdx.graphics.g3d.Attribute

data class LineWidthAttribute(val value: Float) : Attribute(ID) {
	companion object {
		val ID = register("lineWidth")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return LineWidthAttribute(value)
	}
}