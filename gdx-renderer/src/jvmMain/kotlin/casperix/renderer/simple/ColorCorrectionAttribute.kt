package casperix.renderer.simple

import com.badlogic.gdx.graphics.g3d.Attribute

data class ColorCorrectionAttribute(val type:Long, val enabled:Boolean) : Attribute(type) {
	companion object {
		val HDR = register("hdrColorCorrection")
		val GAMMA = register("gammaColorCorrection")
	}

	override fun compareTo(other: Attribute): Int {
		val delta = other.type - this.type
		return delta.toInt()
	}

	override fun copy(): Attribute {
		return ColorCorrectionAttribute(type, enabled)
	}
}