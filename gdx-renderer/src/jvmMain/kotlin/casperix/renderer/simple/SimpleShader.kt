package casperix.renderer.simple

import casperix.renderer.core.DepthRenderPassAttribute
import casperix.renderer.core.RenderedAmount
import casperix.renderer.core.overclocked.OverclockedRenderable
import casperix.renderer.shader.ShaderController
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.ShadowAttribute
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Renderable
import com.badlogic.gdx.graphics.g3d.Shader
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DepthTestAttribute
import com.badlogic.gdx.graphics.g3d.attributes.IntAttribute
import com.badlogic.gdx.graphics.g3d.utils.RenderContext
import com.badlogic.gdx.graphics.glutils.ShaderProgram

class SimpleShader(val provider: SimpleShaderProvider, val depthController: ShaderController, val coloredController: ShaderController) : Shader {
	private var currentContext: RenderContext? = null
	private var currentProgram: ShaderProgram? = null
	private var currentMesh: Mesh? = null
	private var currentMaterial: Material? = null
	private var CW = false
	private var discardByMaterial = false
	private val environment = SimpleShaderSetter()

	companion object {

		private var accumulatedBatches = 0
		private var accumulatedElements = 0
		private var accumulatedVertices = 0

		var forceAutoBind = false
		var defaultCW = false
		var checkNegativeScale = false

		fun dropStatistic(): RenderedAmount {
			val result = RenderedAmount(accumulatedVertices, accumulatedElements, accumulatedBatches)
			accumulatedVertices = 0
			accumulatedElements = 0
			accumulatedBatches = 0
			return result
		}
	}

	init {
		environment.setup(depthController, true)
		environment.setup(coloredController, false)
	}

	override fun begin(camera: Camera, context: RenderContext) {
		environment.currentCamera = camera
		currentContext = context
	}

	fun equalsForMain(A: Material?, B: Material?): Boolean {
		return A === B || (A?.id == B?.id)
	}

	fun equalsForShadow(A: Material?, B: Material?): Boolean {
		if (equalsForMain(A, B)) return true
		if (A == null || B == null) return false

		val blendingAttributeForA: BlendingAttribute? = A.getAttribute(BlendingAttribute.Type)
		val hasBlendingForA = blendingAttributeForA != null && blendingAttributeForA.blended
		if (hasBlendingForA) return false

		val blendingAttributeForB: BlendingAttribute? = B.getAttribute(BlendingAttribute.Type)
		val hasBlendingForB = blendingAttributeForB != null && blendingAttributeForB.blended
		if (hasBlendingForB) return false

		return true
	}

	override fun render(renderable: Renderable) {
		val renderContext = currentContext ?: return

		val depthRenderPassAttribute = renderable.environment.get(DepthRenderPassAttribute.ID) as? DepthRenderPassAttribute
		val shadowCreationPhase = depthRenderPassAttribute != null
		val controller = if (shadowCreationPhase) depthController else coloredController
		val meshPart = renderable.meshPart
		val mesh = meshPart.mesh

		val equalsMaterials = if (shadowCreationPhase) {
			equalsForShadow(currentMaterial, renderable.material)
		} else {
			equalsForMain(currentMaterial, renderable.material)
		}
		val equalsMesh = currentMesh == mesh
		val equalsProgram = currentProgram == controller.program

		if (!equalsMaterials) {
			discardByMaterial = false
			currentMaterial = renderable.material
			val commonAttributes = SimpleShaderProvider.getAttributes(renderable)
			bindAttributes(renderContext, commonAttributes, controller, shadowCreationPhase)
		}

		if (discardByMaterial) return

		val overclocked = OverclockedRenderable.wrap(renderable)

		controller.renderableSetters.setters.forEach { it.set(renderContext, overclocked) }


		var preferredCW = defaultCW
		 if (checkNegativeScale && renderable.worldTransform.det() < 0.0) preferredCW = !preferredCW

		if (CW != preferredCW) {
			CW = preferredCW
			Gdx.gl.glFrontFace(if (CW) GL20.GL_CW else GL20.GL_CCW)
		}

		if (!equalsMesh || !equalsProgram) {
			currentMesh?.unbind(currentProgram)
			currentMesh = mesh
			currentProgram = controller.program
			mesh.bind(currentProgram)
		}

		mesh.render(controller.program, meshPart.primitiveType, meshPart.offset, meshPart.size, forceAutoBind)

		accumulatedElements++
		accumulatedVertices += if (mesh.numIndices != 0) mesh.numIndices else mesh.numVertices
		if (!equalsMesh || !equalsProgram || !equalsMaterials) accumulatedBatches++
	}

	private fun bindAttributes(renderContext: RenderContext, attributes: Attributes, controller: ShaderController, shadowCreationPhase: Boolean) {
		controller.program.bind()
		controller.commonSetters.setters.forEach { it.set(renderContext, attributes) }

		val depthTestAttribute: DepthTestAttribute? = attributes.getAttribute(DepthTestAttribute.Type)
		if (depthTestAttribute == null) {
			renderContext.setDepthTest(GL20.GL_LEQUAL, 0f, 1f)
			renderContext.setDepthMask(true)
		} else {
			renderContext.setDepthTest(depthTestAttribute.depthFunc, depthTestAttribute.depthRangeNear, depthTestAttribute.depthRangeFar)
			renderContext.setDepthMask(depthTestAttribute.depthMask)
		}

		if (!shadowCreationPhase) {
			val blendingAttribute: BlendingAttribute? = attributes.getAttribute(BlendingAttribute.Type)
			if (blendingAttribute == null) {
				renderContext.setBlending(false, GL20.GL_ONE, GL20.GL_ONE)
			} else {
				renderContext.setBlending(blendingAttribute.blended, blendingAttribute.sourceFunction, blendingAttribute.destFunction)
				if (blendingAttribute.blended) {
					Gdx.gl.glBlendFuncSeparate(blendingAttribute.sourceFunction, blendingAttribute.destFunction, GL20.GL_ONE, GL20.GL_ONE_MINUS_SRC_ALPHA)
				}
			}
		}

		val lineWidthAttribute = attributes.getAttribute(LineWidthAttribute.ID) as? LineWidthAttribute
		Gdx.gl.glLineWidth(lineWidthAttribute?.value ?: 1f)

		val cullFaceAttribute = (attributes.get(IntAttribute.CullFace) as? IntAttribute)?.value ?: GL20.GL_BACK

		if (shadowCreationPhase) {
			val shadowCaster: ShadowAttribute? = attributes.getAttribute(ShadowAttribute.CASTER)
			if (shadowCaster != null && !shadowCaster.enabled) {
				discardByMaterial = true
				return
			}

			val shadowCullFace = when (cullFaceAttribute) {
				GL20.GL_FRONT_AND_BACK -> GL20.GL_FRONT_AND_BACK
				GL20.GL_FRONT -> GL20.GL_BACK
				GL20.GL_BACK -> GL20.GL_FRONT
				0 -> GL20.GL_FRONT
				else -> cullFaceAttribute
			}
			renderContext.setCullFace(shadowCullFace)
		} else {
			renderContext.setCullFace(cullFaceAttribute)
		}
	}

	override fun dispose() {}

	override fun init() {
		currentMesh = null
	}

	override fun end() {
		currentMesh?.unbind(currentProgram)

		environment.currentCamera = null
		currentProgram = null
		currentMesh = null
		currentContext = null
		currentMaterial = null
	}

	override fun compareTo(other: Shader?): Int {
		if (other == null) return -1
		if (other === this) return 0
		return 1
	}

	override fun canRender(instance: Renderable): Boolean {
		if (instance.shader == this) {
			return true
		}
		val entry = getShaderEntry(false, instance)
		val result = this.coloredController == entry
		return result
	}

	private fun getShaderEntry(shadowCreationPhase: Boolean, renderable: Renderable): ShaderController {
		val attributes = SimpleShaderProvider.getAttributes(renderable)
		val vertexAttributes = renderable.meshPart.mesh.vertexAttributes
		return provider.getController(shadowCreationPhase, attributes, vertexAttributes)
	}
}