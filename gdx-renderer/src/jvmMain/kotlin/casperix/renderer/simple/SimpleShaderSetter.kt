package casperix.renderer.simple

import casperix.renderer.shader.ShaderController
import casperix.renderer.shader.getAttribute
import casperix.renderer.shadow.DiscardAlphaTestAttribute
import casperix.renderer.shadow.ShadowAttribute
import casperix.renderer.shadow.ShadowLightAttribute
import casperix.renderer.shadow.ShadowTextureAttribute
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g3d.Attributes
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DirectionalLightsAttribute
import com.badlogic.gdx.graphics.g3d.attributes.FloatAttribute
import com.badlogic.gdx.graphics.g3d.attributes.TextureAttribute
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector2
import net.mgsx.gltf.scene3d.attributes.PBRColorAttribute
import net.mgsx.gltf.scene3d.attributes.PBRTextureAttribute
import net.mgsx.gltf.scene3d.lights.DirectionalLightEx

class SimpleShaderSetter {
	var currentCamera: Camera? = null

	private val identityMatrix = Matrix4()

	fun setup(controller: ShaderController, shadowCreator: Boolean) {
		controller.renderableSetters.apply {
			matrix4("u_model_matrix") { it.worldTransform }
		}
		controller.commonSetters.apply {
			matrix4("u_light_projection_view_matrix") {
				val shadowLightAttribute: ShadowLightAttribute? = it.getAttribute(ShadowLightAttribute.ID)
				shadowLightAttribute?.lightProjectionView ?: identityMatrix
			}

			if (!shadowCreator) {
				texture("u_albedo_texture") {
					val diffuseTextureAttribute: TextureAttribute? = it.getAttribute(TextureAttribute.Diffuse)
					diffuseTextureAttribute?.textureDescription
				}
				float("u_discard_alpha_test_threshold") {
					val attribute: DiscardAlphaTestAttribute? = it.getAttribute(DiscardAlphaTestAttribute.ID)
					attribute?.threshold ?: 0.01f
				}

				matrix4("u_projection_view_matrix") {
					currentCamera?.combined ?: identityMatrix
				}

				float("u_shadow_bias") {
					val shadowParamsAttribute: ShadowAttribute? = it.getAttribute(ShadowAttribute.BIAS)
					if (shadowParamsAttribute != null && shadowParamsAttribute.enabled) {
						shadowParamsAttribute.value
					} else {
						0f
					}
				}

				float("u_light_intensity") {
					val lightAttributes: DirectionalLightsAttribute? = it.getAttribute(DirectionalLightsAttribute.Type)
					if (lightAttributes == null) {
						null
					} else {
						val light = lightAttributes.lights.first() as? DirectionalLightEx
						light?.intensity ?: 1f
					}
				}

				texture("u_normal_texture") {
					val diffuseTextureAttribute: TextureAttribute? = it.getAttribute(TextureAttribute.Normal)
					diffuseTextureAttribute?.textureDescription
				}
				float("u_shininess") {
					val attribute = it.get(FloatAttribute.Shininess) as? FloatAttribute
					attribute?.value ?: 32f
				}
				float("u_specular_pow") {
					4f
				}
				float("u_specular_factor") {
					64f
				}
				texture("u_specular_texture") {
					val specularTextureAttribute: TextureAttribute? = it.getAttribute(TextureAttribute.Specular)
					specularTextureAttribute?.textureDescription
				}
				texture("u_specular_texture") {
					val specularTextureAttribute: TextureAttribute? = it.getAttribute(PBRTextureAttribute.MetallicRoughnessTexture)
					specularTextureAttribute?.textureDescription
				}
				color("u_specular_color") {
					val attribute = it.get(ColorAttribute.Specular) as? ColorAttribute
					attribute?.color
				}
				color("u_ambient_color") {
					val attribute = it.get(ColorAttribute.AmbientLight) as? ColorAttribute
					applyOpacity(it, attribute?.color)
				}
				color("u_emissive_color") {
					val attribute = it.get(ColorAttribute.Emissive) as? ColorAttribute
					if (attribute == null) {
						Color.BLACK
					} else {
						applyOpacity(it, attribute.color)
					}
				}
				color("u_diffuse_color") {
					val a1: ColorAttribute? = it.getAttribute(ColorAttribute.Diffuse)
					val a2: ColorAttribute? = it.getAttribute(PBRColorAttribute.BaseColorFactor)
					applyOpacity(it, (a1 ?: a2)?.color)
				}

				texture("u_shadow_texture") {
					val shadowTextureAttribute: ShadowTextureAttribute? = it.getAttribute(ShadowTextureAttribute.ID)
					shadowTextureAttribute?.depthTexture
				}

				vector2("u_shadow_texture_size") {
					val shadowTextureAttribute: ShadowTextureAttribute = it.getAttribute(ShadowTextureAttribute.ID) ?: return@vector2 null
					val size = Vector2(shadowTextureAttribute.depthTexture.texture.width.toFloat(), shadowTextureAttribute.depthTexture.texture.height.toFloat())
					size

				}
				vector3("u_camera_position") {
					currentCamera!!.position
				}
				vector3("u_camera_direction") {
					currentCamera!!.direction
				}
				float("u_camera_perspective") {
					//	use 0 for orthographic camera... if want
					1f
				}
				vector3("u_light_direction") {
					val lightAttributes: DirectionalLightsAttribute? = it.getAttribute(DirectionalLightsAttribute.Type)
					if (lightAttributes == null) {
						null
					} else {
						val light = lightAttributes.lights.first()
						light.direction
					}
				}
			}
			validate()
		}
	}

	fun applyOpacity(attributes: Attributes, color: Color?): Color? {
		if (color == null) return null

		val blendingAttribute = attributes.get(BlendingAttribute.Type) as? BlendingAttribute ?: return color
		if (blendingAttribute.opacity == 1f) return color
		val modifiedColor = color.cpy()
		modifiedColor.a *= blendingAttribute.opacity
		return modifiedColor
	}

}