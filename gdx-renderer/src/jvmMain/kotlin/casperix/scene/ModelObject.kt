package casperix.scene

import casperix.gdx.geometry.toMatrix4
import casperix.math.matrix.Matrix4f
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.math.Matrix4

data class ModelObject(val transform: Matrix4, val data: Model) : SceneObject {
	constructor(transform: Matrix4f, data: Model) : this(transform.toMatrix4(), data)
}