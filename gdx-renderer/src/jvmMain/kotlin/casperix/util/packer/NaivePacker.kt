package casperix.util.packer

import casperix.math.vector.Vector2i
import casperix.misc.max

object NaivePacker : Packer {
	override fun pack(canvasSize: Vector2i, areas: List<PackRegion>): Boolean {
		var left = 0
		var top = 0
		var rowHeight = 0

//		val sorted = areas.sortedBy { -it.size.y * 2 - it.size.x  }

		areas.forEach { area ->
			rowHeight = max(rowHeight, area.size.y)

			if (left + area.size.x > canvasSize.x) {
				top += rowHeight
				left = 0
				rowHeight = area.size.y
			}
			if (top + area.size.y > canvasSize.y) {
				return false
			}
			area.position = Vector2i(left, top)
			left += area.size.x
		}

		return true
	}

}