package casperix.util.packer

import casperix.math.vector.Vector2i


interface PackRegion {
	var position: Vector2i
	val size: Vector2i
}

interface Packer {
	fun pack(canvasSize: Vector2i, areas: List<PackRegion>): Boolean
}

