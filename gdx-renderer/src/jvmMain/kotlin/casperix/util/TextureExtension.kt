package casperix.util

import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.TextureData

fun Texture.getPixmap(): Pixmap {
	return textureData.getPixmap()
}

fun TextureData.getPixmap(): Pixmap {
	if (!isPrepared) prepare()
	return consumePixmap()
}

fun Texture.upload() {
	load(textureData)
}