package casperix.util

import casperix.map2d.IntMap2D
import casperix.math.vector.Vector2i
import casperix.misc.toByteArray
import casperix.misc.toByteBuffer
import java.awt.Point
import java.awt.Transparency
import java.awt.image.*
import java.io.File
import javax.imageio.ImageIO

/**
 * 	Wrap AWT functional for simply save IntMap on disk as png-image
 */
object IntMapToPngDecoder {
	fun save(map: IntMap2D, fileName: String) {
		save(map.dimension, map.array, fileName)
	}

	fun save(size: Vector2i, intArray: IntArray, fileName: String) {
		val bytes = intArray.toByteBuffer().toByteArray()
		save(size, bytes, fileName)
	}

	fun save(size: Vector2i, bytes: ByteArray, fileName: String) {
		val width = size.x
		val height = size.y
		val buffer: DataBuffer = DataBufferByte(bytes, bytes.size)

		val raster: WritableRaster = Raster.createInterleavedRaster(buffer, width, height, 4 * width, 4, intArrayOf(3, 2, 1, 0), null as Point?)
		val cm: ColorModel = ComponentColorModel(ColorModel.getRGBdefault().colorSpace, true, true, Transparency.TRANSLUCENT, DataBuffer.TYPE_BYTE)
		val image = BufferedImage(cm, raster, true, null)

		ImageIO.write(image, "png", File(fileName))
	}
}