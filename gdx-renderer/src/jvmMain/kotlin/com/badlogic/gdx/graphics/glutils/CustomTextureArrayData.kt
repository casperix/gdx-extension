package com.badlogic.gdx.graphics.glutils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.Pixmap
import com.badlogic.gdx.graphics.TextureArrayData
import com.badlogic.gdx.graphics.TextureData
import com.badlogic.gdx.utils.GdxRuntimeException

class CustomTextureArrayData(val textureDataList: List<TextureData?>) : TextureArrayData {
	private val format = Pixmap.Format.RGBA8888
	private var prepared = true
	private val depth = textureDataList.size
	override fun isPrepared(): Boolean {
		return prepared
	}

	override fun prepare() {
		if (prepared) return

		var width = -1
		var height = -1
		for (data in textureDataList) {
			data!!.prepare()
			if (width == -1) {
				width = data.width
				height = data.height
				continue
			}
			if (width != data.width || height != data.height) {
				throw GdxRuntimeException("Error whilst preparing TextureArray: TextureArray Textures must have equal dimensions.")
			}
		}
		prepared = true
	}

	override fun consumeTextureArrayData() {
		for (i in textureDataList.indices) {
			if (textureDataList[i]!!.type == TextureData.TextureDataType.Custom) {
				textureDataList[i]!!.consumeCustomData(GL30.GL_TEXTURE_2D_ARRAY)
			} else {
				val texData = textureDataList[i]
				var pixmap = texData!!.consumePixmap()
				var disposePixmap = texData.disposePixmap()
				if (texData.format != pixmap.format) {
					val temp = Pixmap(pixmap.width, pixmap.height, texData.format)
					temp.blending = Pixmap.Blending.None
					temp.drawPixmap(pixmap, 0, 0, 0, 0, pixmap.width, pixmap.height)
					if (texData.disposePixmap()) {
						pixmap.dispose()
					}
					pixmap = temp
					disposePixmap = true
				}
				Gdx.gl30.glTexSubImage3D(GL30.GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, pixmap.width, pixmap.height, 1, pixmap.glInternalFormat, pixmap.glType, pixmap.pixels)
				if (disposePixmap) pixmap.dispose()
			}
		}
	}

	override fun getWidth(): Int {
		return textureDataList[0]!!.width
	}

	override fun getHeight(): Int {
		return textureDataList[0]!!.height
	}

	override fun getDepth(): Int {
		return depth
	}

	override fun getInternalFormat(): Int {
		return Pixmap.Format.toGlFormat(format)
	}

	override fun getGLType(): Int {
		return Pixmap.Format.toGlType(format)
	}

	override fun isManaged(): Boolean {
		for (data in textureDataList) {
			if (!data!!.isManaged) {
				return false
			}
		}
		return true
	}
}