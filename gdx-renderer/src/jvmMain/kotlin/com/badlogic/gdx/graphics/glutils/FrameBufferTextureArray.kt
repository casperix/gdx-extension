package com.badlogic.gdx.graphics.glutils

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.*
import com.badlogic.gdx.utils.GdxRuntimeException

class FrameBufferTextureArray(
	val builder: FrameBufferTextureArrayBuilder
) : GLFrameBuffer<TextureArray>(builder) {
	/** the zero-based index of the active side  */
	private var currentLayer = 0
	private var texture: TextureArray? = null

	constructor(width: Int, height: Int, layersAmount: Int, format: Pixmap.Format, hasDepth: Boolean, hasStencil: Boolean, minFilter: Texture.TextureFilter, magFilter: Texture.TextureFilter, uWrap: Texture.TextureWrap, vWrap: Texture.TextureWrap)
			: this(FrameBufferTextureArrayBuilder(width, height, layersAmount, format, hasDepth, hasStencil, minFilter, magFilter, uWrap, vWrap)) {

	}

	override fun createTexture(attachmentSpec: FrameBufferTextureAttachmentSpec): TextureArray {
		val bufferBuilder = bufferBuilder as FrameBufferTextureArrayBuilder
		val layers = (1..bufferBuilder.layerAmount).map { GLOnlyTextureData(bufferBuilder.width, bufferBuilder.height, 0, attachmentSpec.internalFormat, attachmentSpec.format, attachmentSpec.type) }
		val result = TextureArray(CustomTextureArrayData(layers))
		result.setFilter(bufferBuilder.minFilter, bufferBuilder.magFilter)
		result.setWrap(bufferBuilder.uWrap, bufferBuilder.vWrap)

		this.texture = result
		return result
	}

	override fun disposeColorTexture(colorTexture: TextureArray) {
		colorTexture.dispose()
	}

	override fun attachFrameBufferColorTexture(texture: TextureArray) {
		val bufferBuilder = bufferBuilder as FrameBufferTextureArrayBuilder
		(0 until bufferBuilder.layerAmount).forEach {
			bindSide(it)
		}
	}

	/** Makes the frame buffer current so everything gets drawn to it, must be followed by call to either [.nextSide] or
	 * [.bindSide] to activate the side to render onto.  */
	override fun bind() {
		currentLayer = -1
		super.bind()
	}

	/** Bind the next side of cubemap and return false if no more side. Should be called in between a call to [.begin] and
	 * #end to cycle to each side of the cubemap to render on.  */
	fun bindNextSide(): Boolean {
		if (currentLayer >= builder.layerAmount) {
			throw GdxRuntimeException("No remaining sides.")
		} else if (currentLayer == builder.layerAmount - 1) {
			return false
		}
		currentLayer++
		bindSide(currentLayer)
		return true
	}

	/** Bind the side, making it active to render on. Should be called in between a call to [.begin] and [.end].
	 * @param side The side to bind
	 */
	fun bindSide(layer: Int) {
		val texture = texture ?: return
		val glHandle = texture.textureObjectHandle
		Gdx.gl30.glFramebufferTextureLayer(GL20.GL_FRAMEBUFFER, GL20.GL_COLOR_ATTACHMENT0, glHandle, 0, layer)
//		Gdx.gl20.glFramebufferTexture2D(GL20.GL_FRAMEBUFFER, GL20.GL_COLOR_ATTACHMENT0, 	GL20.GL_TEXTURE_2D, glHandle, layer)
	}
}