uniform mat4 u_model_matrix;
uniform mat4 u_light_projection_view_matrix;

in vec3 a_position;
in vec2 a_texCoord0;

out vec2 v_texture_uv;

void main()
{
    v_texture_uv = a_texCoord0;
    vec4 position = u_model_matrix*vec4(a_position, 1.0);
    gl_Position = u_light_projection_view_matrix*position;
}