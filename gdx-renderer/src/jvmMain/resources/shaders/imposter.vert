struct RangeInfo
{
    float lastFrame;
    float minAngle;
    float maxAngle;
};

struct AngleInfo
{
    float layer;
    float angle;
};

uniform float u_time;
uniform mat4 u_projection_view_transposed;
uniform vec3 u_camera_position;
uniform vec2 u_scale;
uniform float u_wind_strength;
uniform RangeInfo u_vertical;
uniform RangeInfo u_horizontal;

attribute vec3 a_position;
attribute vec2 a_offset;
attribute vec2 a_tex;

out vec2 v_texture_uv;
out float v_layer;

const float PI = 3.14159265;
const float HALF_PI = 3.14159265 / 2.0;
const float DOUBLE_PI = 3.14159265 * 2.0;


AngleInfo getAngleInfo(RangeInfo range_info, float y_basis, float x_basis) {
    float angle =  atan(y_basis, x_basis);
    float angle_normalized = (angle  - range_info.minAngle) / (range_info.maxAngle - range_info.minAngle);

    float frame_value = angle_normalized * range_info.lastFrame;

    float A = floor(frame_value);
    float weight = fract(frame_value);
    float B = ceil(frame_value);
    float C = round(frame_value);

    float layerA = clamp(A, 0.0, range_info.lastFrame);
    float layerB = clamp(B, 0.0, range_info.lastFrame);

    float angleA = (A / range_info.lastFrame) * (range_info.maxAngle - range_info.minAngle) + range_info.minAngle;
    float angleB = (B / range_info.lastFrame) * (range_info.maxAngle - range_info.minAngle) + range_info.minAngle;
    float angleC = (C / range_info.lastFrame) * (range_info.maxAngle - range_info.minAngle) + range_info.minAngle;

    float frame = clamp(C, 0.0, range_info.lastFrame);
    return AngleInfo(frame, angleC);
}

void main()
{
    float PI = 3.1415;
    vec3 Z = vec3(0, 0, 1);
    vec3 objectToCamera =  u_camera_position - a_position;


    //  wind simulation
    float horizontal_angle_dispersion = cos(a_position.x * 0.039 + a_position.y * 0.043 + u_time * 1.2) * u_wind_strength;
    float vertical_angle_dispersion = cos(a_position.x * 0.047 - a_position.y * 0.053 + u_time * 1.2) * u_wind_strength;

    //  calculate virtual angles
    AngleInfo vertical_angle_info =  getAngleInfo(u_vertical, length(objectToCamera.xy), objectToCamera.z);
    AngleInfo horizontal_angle_info =  getAngleInfo(u_horizontal, objectToCamera.y, objectToCamera.x);

    //  color position (y-down on source image)
    v_texture_uv = a_tex;

    float vertical_angle = vertical_angle_info.angle + vertical_angle_dispersion;
    float horizontal_angle = horizontal_angle_info.angle + horizontal_angle_dispersion;

    v_layer = (u_horizontal.lastFrame + 1) * vertical_angle_info.layer + horizontal_angle_info.layer;

    float vsin = sin(vertical_angle);
    vec3 right = vec3(cos(horizontal_angle + PI / 2.0), sin(horizontal_angle+ PI / 2.0), 0.0);
    vec3 cameraDirection = -vec3(
    vsin * cos(horizontal_angle),
    vsin * sin(horizontal_angle),
    cos(vertical_angle)
    );

    vec3 up = -cross(right, cameraDirection);

    //  calculate particle orientation
    vec3 offset_to_observer = -cameraDirection * abs(a_offset.x) * 0.5;
    vec3 particleRight = right;
    vec3 particleUp =up;
    vec3 position = a_position + offset_to_observer + particleRight * a_offset.x + particleUp * a_offset.y;
    vec4 vertexPosition = u_projection_view_transposed * vec4(position, 1);

    gl_Position =  vertexPosition;
}