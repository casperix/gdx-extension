uniform sampler2D u_billboard_shadow_cast_texture;

out vec4 FragColor;

in vec2 v_texture_uv;

void main()
{
    #ifdef SHADOW_CAST_TEXTURE
    vec4 diffuse_color = texture(u_billboard_shadow_cast_texture, v_texture_uv);
    if (diffuse_color.a < 0.5) discard;
    #endif

    FragColor.x = gl_FragCoord.z;
}