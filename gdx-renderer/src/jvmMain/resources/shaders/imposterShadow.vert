struct RangeInfo
{
    float lastFrame;
    float minAngle;
    float maxAngle;
};

uniform mat4 u_projection_view_world_transposed;
uniform vec3 u_camera_position;
uniform vec2 u_scale;
uniform RangeInfo u_vertical;
uniform RangeInfo u_horizontal;

attribute vec3 a_position;
attribute vec2 a_offset;
attribute vec2 a_tex;

out vec2 v_tex;
out float v_layer;
out float v_depth;

const float PI = 3.14159265;
const float HALF_PI = 3.14159265 / 2.0;
const float DOUBLE_PI = 3.14159265 * 2.0;

void main()
{
    vec3 Z = vec3(0, 0, 1);

    vec3 objectToCamera =  u_camera_position - a_position;
    vec3 particleRight = -normalize(cross(objectToCamera, Z));
    vec3 particleUp = normalize(cross(particleRight, objectToCamera));

    vec3 position = a_position + particleRight * a_offset.x + particleUp * a_offset.y;
    vec4 vertexPosition = u_projection_view_world_transposed * vec4(position, 1);

    float cosVerticalAngle = dot(objectToCamera, Z) / length(objectToCamera);
    //  angle from Z to objectToCamera (0--PI)
    float verticalAngle =  acos(cosVerticalAngle);
    //  horizontal angle (0--2PI)
    float horizontalAngle =  PI - atan(objectToCamera.y, objectToCamera.x);

    float verticalAngleNormalized = (verticalAngle  - u_vertical.minAngle) /(u_vertical.maxAngle - u_vertical.minAngle);
    float verticalStep = clamp(round(verticalAngleNormalized * u_vertical.lastFrame), 0.0, u_vertical.lastFrame);

    float horizontalAngleNormalized = (horizontalAngle - u_horizontal.minAngle) / (u_horizontal.maxAngle - u_horizontal.minAngle);
    float horizontalStep = clamp(round(horizontalAngleNormalized * u_horizontal.lastFrame), 0.0, u_horizontal.lastFrame);

    v_tex = a_tex;
    v_layer = (u_horizontal.lastFrame + 1.0) * verticalStep + horizontalStep;

    // why 0.25? If 0.5 shadow is invisible
    v_depth = vertexPosition.z / vertexPosition.w * 0.5 + 0.25;

    gl_Position =  vertexPosition;
}