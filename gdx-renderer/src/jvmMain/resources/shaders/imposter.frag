uniform sampler2DArray u_imposter_texture_array;
uniform float u_discard_alpha_test_threshold;

in float v_layer;
in vec2 v_texture_uv;

void main()
{
    float layer = v_layer;
    vec4 textureColor = texture(u_imposter_texture_array, vec3(v_texture_uv, layer));

    if (textureColor.a < u_discard_alpha_test_threshold) discard;
    gl_FragColor = textureColor;
}