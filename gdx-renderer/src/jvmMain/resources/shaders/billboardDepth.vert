uniform mat4 u_model_matrix;
uniform mat4 u_light_projection_view_matrix;
uniform vec3 u_camera_up;
uniform vec3 u_camera_right;
uniform vec3 u_camera_direction;

in vec3 a_center;
in vec3 a_offset;
in vec2 a_tex;

out vec2 v_texture_uv;

void main()
{
    vec4 center = u_model_matrix * vec4(a_center, 1);
    vec4 world_position = vec4(center.xyz + u_camera_right * a_offset.x + u_camera_up * a_offset.y + u_camera_direction * a_offset.z, center.w);
    vec4 view_position =  u_light_projection_view_matrix * world_position;

    v_texture_uv = a_tex;

    gl_Position = view_position;
}