uniform mat4 u_projection_view_matrix;
uniform mat4 u_model_matrix;
uniform mat4 u_light_projection_view_matrix;
uniform vec3 u_light_direction;
uniform vec3 u_camera_position;

in vec3 a_position;
in vec4 a_color;
in vec3 a_normal;
in vec2 a_texCoord0;
in vec3 a_tangent;

out vec3 v_light_direction;
out vec3 v_position;
out vec3 v_normal;
out vec2 v_texCoord;
/*
CS  -- is Custom Space.
Now CS equal WorldSpace.

I tried to use TangentSpace, but failed.
Probably the point is in the calculation error, leading to unusable results on smooth bends of surfaces
*/
out vec3 v_position_cs;
out vec3 v_camera_position_cs;
out vec3 v_light_direction_cs;
out vec4 v_position_light_space;
out mat3 v_from_tangent_space;

out vec4 v_color;

void main()
{
    vec4 position = u_model_matrix * vec4(a_position, 1.0);
    mat3 normal_matrix = mat3(u_model_matrix);
    vec3 normal = normalize(normal_matrix * a_normal);

    #ifdef NORMAL_TEXTURE
    vec3 tangent = normalize(normal_matrix * a_tangent);

    //  Gram-Schmidt process (?)
    tangent = normalize(tangent - dot(tangent, normal) * normal);

    vec3 binormal = cross(normal, tangent);
    v_from_tangent_space = mat3(tangent, binormal, normal);
    #endif

    v_light_direction_cs = u_light_direction;
    v_camera_position_cs = u_camera_position;
    v_position_cs = position.xyz;

    #ifdef VERTEX_COLOR
    v_color = a_color;
    #endif

    v_position = position.xyz;
    v_normal = normal;
    v_texCoord = a_texCoord0;
    v_position_light_space = u_light_projection_view_matrix * position;
    v_light_direction = u_light_direction;

    gl_Position = u_projection_view_matrix * position;
}