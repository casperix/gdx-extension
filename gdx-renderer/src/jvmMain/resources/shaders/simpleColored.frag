out vec4 FragColor;

uniform float u_shininess;
uniform float u_specular_pow;
uniform float u_specular_factor;

uniform sampler2D u_albedo_texture;
uniform sampler2D u_normal_texture;
uniform sampler2D u_specular_texture;
uniform sampler2D u_shadow_texture;

uniform float u_light_intensity;
uniform vec2 u_shadow_texture_size;
uniform float u_shadow_bias;
uniform float u_discard_alpha_test_threshold;
uniform vec4 u_ambient_color;
uniform vec4 u_emissive_color;
uniform vec4 u_specular_color;

uniform vec4 u_diffuse_color;

const float gammaInverted = 1.0 / 2.2;

in vec3 v_light_direction;
in vec3 v_position;
in vec3 v_normal;
in vec2 v_texCoord;
in vec4 v_color;

in vec3 v_position_cs;
in vec3 v_camera_position_cs;
in vec3 v_light_direction_cs;
in vec4 v_position_light_space;
in mat3 v_from_tangent_space;


float getNearestShadow(vec3 shadowProjection, vec2 offset, float bias)
{
    float closestDepth = texture(u_shadow_texture, offset+shadowProjection.xy).r;
    float currentDepth = shadowProjection.z;
    return currentDepth - bias > closestDepth && currentDepth - bias  <= 1.0  ? 1.0 : 0.0;
}

vec3 getDebugShadowColor(vec3 shadowProjection, float bias)
{
    if (shadowProjection.x < 0.0 || shadowProjection.y < 0.0 || shadowProjection.x > 1.0 || shadowProjection.y > 1.0) {
        return vec3(1, 0, 0);
    } else {
        float closestDepth = texture(u_shadow_texture, shadowProjection.xy).r;
        float currentDepth = shadowProjection.z;
        return currentDepth - bias > closestDepth && currentDepth - bias  <= 1.0  ? vec3(0) : vec3(1);
    }
}


vec3 getCurrentDebugShadowColor() {
    vec3 shadowProjection = v_position_light_space.xyz / v_position_light_space.w* 0.5 + 0.5;
    return getDebugShadowColor(shadowProjection, u_shadow_bias);
}


float getPCFShadow(vec3 shadowProjection, float bias) {
    vec2 t = 1.0 / u_shadow_texture_size;

    float shadow = 0.0;

    shadow += getNearestShadow(shadowProjection, vec2(0, 0) * t, bias);
    shadow += getNearestShadow(shadowProjection, vec2(-1, 0) * t, bias);
    shadow += getNearestShadow(shadowProjection, vec2(1, 0) * t, bias);
    shadow += getNearestShadow(shadowProjection, vec2(0, -1) * t, bias);
    shadow += getNearestShadow(shadowProjection, vec2(0, 1) * t, bias);

    return shadow / 5.0;
}

float getShadow(float bias_factor) {
    #ifdef RECEIVE_SHADOW
    vec3 shadowProjection = v_position_light_space.xyz / v_position_light_space.w* 0.5 + 0.5;
    #ifdef PCF_SHADOW
    return 1.0 - getPCFShadow(shadowProjection, bias_factor);
    #else
    return 1- getNearestShadow(shadowProjection, vec2(0.0), bias_factor);
    #endif//PCF_SHADOW
    #else
    return 1.0;
    #endif//SHADOW
}

vec4 sRGBtoLinear(vec4 srgbIn)
{
    vec3 linOut = pow(srgbIn.xyz,vec3(2.2));
    return vec4(linOut,srgbIn.w);
}

vec3 getSpecularColor(vec3 normal) {
    #ifdef SPECULAR_TEXTURE
    vec4 base_specular_color = texture(u_specular_texture, v_texCoord);
    #else
    vec4 base_specular_color = u_specular_color;
    #endif

    vec3 light_color = vec3(1.0);
    vec3 to_camera_direction = normalize(v_camera_position_cs - v_position_cs);
    vec3 halfway_direction = normalize( to_camera_direction - v_light_direction_cs);
    float specular_factor = pow(max(dot(normal, halfway_direction), 0.0), u_shininess);
    return light_color * pow(base_specular_color.z, u_specular_pow) * u_specular_factor * specular_factor;
}

void main()
{
    float facing_factor = float(gl_FrontFacing) * 2.0 - 1.0;

    #ifdef NORMAL_TEXTURE
    vec3 normal_cs = 2.0 * texture(u_normal_texture, v_texCoord).xyz - 1.0;
    mat3 tangent_space = v_from_tangent_space;
    //  after shader-interpolation we have non normalized matrix components
    tangent_space[0] = normalize(v_from_tangent_space[0]);
    tangent_space[1] = normalize(v_from_tangent_space[1]);
    tangent_space[2] = normalize(v_from_tangent_space[2]);
    normal_cs = tangent_space * normalize(normal_cs * facing_factor);
    vec3 face_normal_cs = tangent_space * vec3(0, 0, facing_factor);
    #else
    vec3 normal_cs = normalize(v_normal * facing_factor);
    vec3 face_normal_cs = normal_cs;
    #endif

    #ifdef DIFFUSE_TEXTURE
    vec4 sourceColor = sRGBtoLinear(texture(u_albedo_texture, v_texCoord));
    #else
    vec4 sourceColor = vec4(1);
    #endif

    #ifdef VERTEX_COLOR
    sourceColor *= v_color;
    #endif
    #ifdef DIFFUSE_COLOR
    sourceColor *= u_diffuse_color;
    #endif


    #ifdef ALPHA_BLEND_TEXTURE
    if (sourceColor.a < u_discard_alpha_test_threshold) discard;
    #endif

    #ifdef CALCULATE_LIGHT

    float bias_factor = u_shadow_bias * (1.0 - dot(face_normal_cs, -v_light_direction_cs));
    float shadow_factor = getShadow(bias_factor);

    //      virtual surface have light.... But geometry facet mabe in shadow
    float face_normal_factor = dot(face_normal_cs, -v_light_direction_cs);
    float has_face_light_factor = 1.0;
    if (face_normal_factor < 0.0) has_face_light_factor = 0.0;

    //  calculate diffuse
    float diffuse_factor = max(0.0, dot(normal_cs, -v_light_direction_cs));
    float light_factor = has_face_light_factor * shadow_factor * diffuse_factor;

    vec3 diffuse =  mix(sourceColor.rgb * u_ambient_color.xyz, sourceColor.rgb * u_light_intensity, light_factor);
    vec3 outputColor = diffuse;
    #ifdef SPECULAR_TEXTURE
    vec3 specular = getSpecularColor(normal_cs);
    outputColor += shadow_factor * specular;
    #else
    vec3 specular = vec3(0.0);
    #endif
    outputColor += u_emissive_color.xyz;

//  uncomment for check diffuse
//    outputColor = outputColor * 0.01 + 0.99 * diffuse;
//  uncomment for check specular
//    outputColor = outputColor * 0.01 + 0.99 * specular;
    // uncomment for check normal
    //    outputColor = outputColor * 0.01 + 0.99 * (normal_cs * 0.5 + vec3(0.5));
// uncomment for check to_camera_direction
//    outputColor = outputColor * 0.01 + 0.99 * v_tangent_space[0];

    #else
    vec3 outputColor = sourceColor.rgb;
    #endif

    #ifdef HDR_CORRECTION
    outputColor = outputColor / (outputColor + vec3(1.0));
    #endif

    #ifdef GAMMA_CORRECTION
    outputColor = pow(outputColor, vec3(gammaInverted));
    #endif

    #ifdef DEBUG_SHADOW
    outputColor = outputColor * 0.5 + 0.5 * getCurrentDebugShadowColor();
    #endif


    FragColor = vec4(outputColor, sourceColor.a);
}
