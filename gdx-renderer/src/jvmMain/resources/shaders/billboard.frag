uniform sampler2D u_diffuse_texture;
uniform sampler2D u_normal_texture;
uniform vec3 u_light_direction_in_camera_space;
uniform float u_discard_alpha_test_threshold;

out vec4 FragColor;

in vec2 v_tex;
in vec4 v_debug_center;
in vec4 v_color;

void main()
{
    #ifdef DIFFUSE_TEXTURE
    vec4 diffuse_color = texture(u_diffuse_texture, v_tex) * v_color;
    #else
    vec4 diffuse_color = v_color;
    #endif

    #ifdef NORMAL_TEXTURE
    vec3 normal = texture(u_normal_texture, v_tex).xyz * 2.0 - 1.0;
    normal *= sign(normal.z);
    #else
    vec3 normal = vec3(0.0, 0.0, 1.0);
    #endif
    float ambient_weight = 0.2;
    float light_factor = ambient_weight + (1.0 - ambient_weight) * max(0.0, dot(normal, u_light_direction_in_camera_space));
    diffuse_color.xyz *= light_factor;

    #ifdef DEBUG_MODE
    float dx = sign(v_debug_center.x);
    float dy = sign(v_debug_center.y);
    float debug_weight = 0.2;
    diffuse_color =diffuse_color * (1-debug_weight)
    + debug_weight * max(dx, 0.0) * vec4(1, 0, 0, 1)
    + debug_weight * max(-dx, 0.0) * vec4(1, 1, 1, 1)
    + debug_weight * max(dy, 0.0) * vec4(0, 0, 1, 1)
    + debug_weight * max(-dy, 0.0) * vec4(1, 1, 1, 1);
    #endif

    #ifdef DISCARD_ALPHA_TEST
    if (diffuse_color.a < u_discard_alpha_test_threshold) discard;
    #endif

    //    gl_FragDepth = ((gl_DepthRange.diff * (u_depth_offset + v_depth)) +    gl_DepthRange.near + gl_DepthRange.far) / 2.0;
    FragColor = diffuse_color;
}
