uniform sampler2D u_albedo_texture;

out vec4 FragColor;
in vec2 v_texture_uv;

void main()
{
    #ifdef ALPHA_BLEND_TEXTURE
    vec4 sourceColor = texture(u_albedo_texture, v_texture_uv);
    if (sourceColor.a < 0.5) discard;
    #endif


    float z = gl_FragCoord.z;
    FragColor.x = z;
}