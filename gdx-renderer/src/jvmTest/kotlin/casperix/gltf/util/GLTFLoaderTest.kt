package casperix.gltf.util

import casperix.renderer.loader.SceneAssetProvider
import org.junit.Before
import org.junit.Test

class GLTFLoaderTest {

	@Before
	fun start() {
		//	todo: load all test with reflection
		GDXTestsExecutor(listOf(::testLoader))
	}

	@Test
	fun testLoader() {
		val testAsset = SceneAssetProvider.getInternalModifiedAsset("test.gltf")
		println(testAsset)
	}
}

