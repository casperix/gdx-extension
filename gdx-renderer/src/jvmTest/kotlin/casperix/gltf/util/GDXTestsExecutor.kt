package casperix.gltf.util

import com.badlogic.gdx.ApplicationAdapter
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration

class GDXTestsExecutor(val tests: List<() -> Unit>) : ApplicationAdapter() {

	init {
		val config = Lwjgl3ApplicationConfiguration()
		Lwjgl3Application(this, config)
	}

	override fun create() {
		tests.forEach { test ->
			try {
				test()
			} catch (error: Throwable) {
				error.printStackTrace()
			}
		}
		Gdx.app.exit()
	}

	override fun dispose() {
	}
}