package casperix.gltf

import casperix.gdx.app.GdxDesktopApplicationLauncher
import casperix.renderer.RenderManager
import casperix.renderer.loader.SceneAssetProvider
import com.badlogic.gdx.files.FileHandle
import com.badlogic.gdx.graphics.g3d.ModelInstance
import org.junit.Before
import org.junit.Test


class SimpleDemo {

	@Before
	fun start() {
		GdxDesktopApplicationLauncher() {
			val renderManager = RenderManager.createPBRender(it)
			val testModel = SceneAssetProvider.getModifiedModel(FileHandle("test.gltf"))
			renderManager.add(ModelInstance(testModel))
		}
	}

	@Test
	fun dummy() {

	}
}

