package casperix.util.packer

import casperix.map2d.IntMap2D
import casperix.math.color.Color
import casperix.math.vector.Vector2i
import casperix.math.vector.nextVector2i
import casperix.util.IntMapToPng
import kotlin.random.Random
import kotlin.test.Test

class PackerTest {

	class ImageEntry(val map: IntMap2D) : PackRegion {
		override var position = Vector2i.ZERO
		override val size = map.dimension
	}

	@Test
	fun pack() {
		val random = Random(1)
		val images = Color.COLORS.map {
			ImageEntry(createColoredPixmap(random.nextVector2i(Vector2i(4), Vector2i(128)), it))
		}

		var atlasSize = Vector2i(2, 2)
		var scaleY = true

		while (true) {
			val packer: Packer = NaivePacker
			if (packer.pack(atlasSize, images)) break

			if (scaleY) {
				atlasSize = Vector2i(atlasSize.x, atlasSize.y * 2)
			} else {
				atlasSize = Vector2i(atlasSize.x * 2, atlasSize.y)
			}
			scaleY = !scaleY
		}

		val atlas = IntMap2D.create(atlasSize) { 0 }

		images.forEach {
			atlas.put(it.position, it.map)
		}

		println("packed to $atlasSize")
		IntMapToPng.save(atlas, "atlas.png")
	}

	private fun createColoredPixmap(size: Vector2i, color: Color): IntMap2D {
		val pixmap = IntMap2D.createByXY(size) {
			color.value
		}
		return pixmap
	}

	private fun IntMap2D.put(position: Vector2i, map: IntMap2D) {
		(0 until map.width).forEach { localX ->
			(0 until map.height).forEach { localY ->
				set(position.x + localX, position.y + localY, map.get(localX, localY))
			}
		}

	}

}