# Gdx Renderer

Add custom renderer, shaders, imposters, etc.
Also improved gltf support (based on gdx-gltf library)
In plans optimize render system for output large amount objects in real time on slow PC
