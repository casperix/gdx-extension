package casperix.gdx.app

import casperix.app.window.WindowWatcher
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration

/**
 * 	Запускает приложение с одним окном
 */
class GdxDesktopApplicationLauncher(config: GdxApplicationConfig = GdxApplicationConfig(), setup: (watcher: WindowWatcher) -> Unit) : GdxApplicationAdapter(setup) {
	init {
		var outputError: Exception? = null

		try {
			val appConfig = Lwjgl3ApplicationConfiguration()
			appConfig.setTitle(config.title)
			appConfig.setMaximized(config.desktop.maximized)
			appConfig.setForegroundFPS(config.desktop.foregroundFPS)
			appConfig.setIdleFPS(config.desktop.idleFPS)
			if (config.appIconPath != null) {
				appConfig.setWindowIcon(config.appIconPath)
			}
			if (!config.desktop.maximized) {
				appConfig.setWindowedMode(config.desktop.size.x, config.desktop.size.y)
			}
			appConfig.setOpenGLEmulation(Lwjgl3ApplicationConfiguration.GLEmulation.GL30, 3, 0)
			appConfig.setBackBufferConfig(8, 8, 8, 8, config.depthBits, config.stencilBits, config.msaa)
			Lwjgl3Application(this, appConfig)
		} catch (error: Exception) {
			outputError = error
			error.printStackTrace()
		} finally {
			System.exit(if (outputError == null) 0 else -1)
		}
	}
}