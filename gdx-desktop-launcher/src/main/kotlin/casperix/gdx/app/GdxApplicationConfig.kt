package casperix.gdx.app

import casperix.math.vector.Vector2i

class GdxApplicationConfig(val title: String = "TestApp", val appIconPath:String? = null, val msaa: Int = 0, val depthBits:Int = 16, val stencilBits:Int = 0, val desktop:GdxDesktopWindowConfig = GdxDesktopWindowConfig())

class GdxDesktopWindowConfig(val foregroundFPS:Int = 0, val idleFPS:Int = 1, val maximized:Boolean = true, val size:Vector2i = Vector2i(640, 480))