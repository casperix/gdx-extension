package casperix.gdx.app

import casperix.app.window.BitmapCursor
import casperix.app.window.Cursor
import casperix.app.window.SystemCursor
import casperix.app.window.WindowWatcher
import casperix.gdx.graphics.toPixmap
import casperix.math.vector.Vector2i
import casperix.signals.concrete.EmptySignal
import casperix.signals.concrete.Signal
import casperix.signals.concrete.StorageSignal
import casperix.signals.then
import com.badlogic.gdx.ApplicationListener
import com.badlogic.gdx.Gdx

open class GdxApplicationAdapter(val setup: (WindowWatcher) -> Unit) : ApplicationListener {
	private var watcher:WindowWatcher? = null

	private val cursorMap = mutableMapOf<BitmapCursor, com.badlogic.gdx.graphics.Cursor>()

	private fun setCursor(cursor: Cursor) {
		if (cursor is SystemCursor) {
			val gdxSystemCursor = when (cursor) {
				SystemCursor.DEFAULT -> com.badlogic.gdx.graphics.Cursor.SystemCursor.Arrow
				SystemCursor.HAND -> com.badlogic.gdx.graphics.Cursor.SystemCursor.Hand
				SystemCursor.MOVE -> com.badlogic.gdx.graphics.Cursor.SystemCursor.AllResize
				SystemCursor.TEXT -> com.badlogic.gdx.graphics.Cursor.SystemCursor.Ibeam
				SystemCursor.WAIT -> com.badlogic.gdx.graphics.Cursor.SystemCursor.Arrow
			}
			Gdx.graphics.setSystemCursor(gdxSystemCursor)
		} else if (cursor is BitmapCursor) {
			val gdxCursor = cursorMap.getOrPut(cursor) {
				Gdx.graphics.newCursor(cursor.image.toPixmap(), cursor.hotSpot.x, cursor.hotSpot.y)
			}
			Gdx.graphics.setCursor(gdxCursor)
		} else {
			throw Error("${cursor::class} is not supported. Use ${SystemCursor::class} or ${BitmapCursor::class}")
		}
	}

	override fun create() {
		//	Implementation dependency hack.
		//	LWJGL call first "resize" after "create"
		//	So... We use first resize for initialize (with working "resize" function)
	}

	private fun initialize(width: Int, height: Int) {
		val watcher =  object : WindowWatcher {
			override val onCursor = StorageSignal<Cursor>(SystemCursor.DEFAULT)
			override val onResize = StorageSignal(Vector2i(width, height))
			override val onUpdate = Signal<Double>()
			override val onPostRender = EmptySignal()
			override val onRender = EmptySignal()
			override val onPreRender = EmptySignal()
			override val onExit = EmptySignal()
		}
		this.watcher = watcher
		watcher.onResize.then(::onResize)
		watcher.onCursor.then(::setCursor)
		setup(watcher)
	}

	private fun onResize(value:Vector2i) {
		Gdx.graphics.setWindowedMode(value.x, value.y)
	}

	override fun resize(width: Int, height: Int) {
		val watcher = watcher

		if (watcher == null) {
			initialize(width, height)
		} else {
			watcher.onResize.set(Vector2i(width, height))
		}
	}

	override fun render() {
		val watcher = watcher ?: return
		val tick = Gdx.graphics.deltaTime.toDouble()
		watcher.onUpdate.set(tick)
		watcher.onPreRender.set()
		watcher.onRender.set()
		watcher.onPostRender.set()
	}

	override fun pause() {

	}

	override fun resume() {

	}

	override fun dispose() {
		val watcher = watcher ?: return
		watcher.onExit.set()
	}
}