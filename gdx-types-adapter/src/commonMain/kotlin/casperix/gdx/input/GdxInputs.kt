package casperix.gdx.input

import casperix.input.*
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2i
import casperix.misc.Disposable
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputAdapter

class GdxInputs(val source: InputDispatcher = DefaultInputDispatcher()) : InputAdapter(), InputDispatcher by source {
	private var pointerMovement = Vector2d.ZERO
	private var lastPointerPosition = Vector2d.ZERO
	private var lastKeyButton = KeyButton.UNKNOWN

	override fun keyDown(keycode: Int): Boolean {
		val event = KeyDown(KeyButton.getByCode(keycode))
		lastKeyButton = event.button
		onKeyDown.set(event)
		return event.captured
	}

	override fun keyUp(keycode: Int): Boolean {
		val event = KeyUp(KeyButton.getByCode(keycode))
		onKeyUp.set(event)
		return event.captured
	}

	override fun touchUp(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
		val event = TouchUp(getVector2d(screenX, screenY), pointer, PointerButton(button), getNormalizedVector2d(screenX, screenY), pointerMovement)
		onTouchUp.set(event)
		return event.captured
	}

	override fun mouseMoved(screenX: Int, screenY: Int): Boolean {
		val event = MouseMove(getVector2d(screenX, screenY), getNormalizedVector2d(screenX, screenY), pointerMovement)
		onMouseMove.set(event)
		return event.captured
	}

	override fun keyTyped(character: Char): Boolean {
		val event = KeyTyped(character, lastKeyButton)
		onKeyTyped.set(event)
		return event.captured
	}

	override fun scrolled(x: Float, y: Float): Boolean {
		val event = MouseWheel(getVector2d(Gdx.input.x, Gdx.input.y), getNormalizedVector2d(Gdx.input.x, Gdx.input.y), Vector2d(x.toDouble(), y.toDouble()))
		onMouseWheel.set(event)
		return event.captured
	}

	override fun touchDragged(screenX: Int, screenY: Int, pointer: Int): Boolean {
		updatePointerMovement(screenX, screenY)

		val event = TouchDragged(getVector2d(screenX, screenY), pointer, getNormalizedVector2d(screenX, screenY), pointerMovement)
		onTouchDragged.set(event)
		return event.captured
	}


	override fun touchDown(screenX: Int, screenY: Int, pointer: Int, button: Int): Boolean {
		val event = TouchDown(getVector2d(screenX, screenY), pointer, PointerButton(button), getNormalizedVector2d(screenX, screenY), pointerMovement)
		onTouchDown.set(event)
		return event.captured
	}

	private fun updatePointerMovement(screenX: Int, screenY: Int) {
		val nextPointerPosition = getVector2d(screenX, screenY)
		pointerMovement = (nextPointerPosition - lastPointerPosition)
		lastPointerPosition = nextPointerPosition
	}

	private fun getVector2d(sourceX: Int, sourceY: Int): Vector2d {
		return Vector2i(sourceX, sourceY).toVector2d()
	}

	private fun getNormalizedVector2d(sourceX: Int, sourceY: Int): Vector2d {
		return Vector2d(
			sourceX.toDouble() / Gdx.graphics.width.toDouble(),
			sourceY.toDouble() / Gdx.graphics.height.toDouble()
		)
	}
}