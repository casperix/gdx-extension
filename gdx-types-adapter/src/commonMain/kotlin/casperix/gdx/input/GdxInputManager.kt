package casperix.gdx.input

import casperix.input.DefaultInputDispatcher
import casperix.input.InputDispatcher
import casperix.input.InputQueue
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.InputProcessor

class GdxInputManager(val source: InputDispatcher = DefaultInputDispatcher()) : InputProcessor by GdxInputs(source) {
	val queue = InputQueue(source)

	companion object {
		fun register(dispatcher: InputDispatcher, priority: Int = 0) {
			val inputEntry = Gdx.input.inputProcessor
			if (inputEntry is GdxInputManager) {
				inputEntry.queue.addDispatcher(dispatcher, priority)
			} else {
				Gdx.input.inputProcessor = GdxInputManager()
				register(dispatcher, priority)
			}
		}

		fun unregister(dispatcher: InputDispatcher) {
			val inputEntry = Gdx.input.inputProcessor
			if (inputEntry is GdxInputManager) {
				inputEntry.queue.removeDispatcher(dispatcher)
			} else {
				Gdx.input.inputProcessor = GdxInputManager()
				unregister(dispatcher)
			}
		}
	}
}

