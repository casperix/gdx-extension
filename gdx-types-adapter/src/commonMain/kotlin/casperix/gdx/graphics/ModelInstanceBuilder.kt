package casperix.gdx.graphics

import com.badlogic.gdx.graphics.g3d.ModelInstance

object ModelInstanceBuilder {
	fun createByGeometry(vararg tasks: GeometryBuilder.Task): ModelInstance {
		val model = GeometryBuilder.build(*tasks)
		return ModelInstance(model)
	}
}