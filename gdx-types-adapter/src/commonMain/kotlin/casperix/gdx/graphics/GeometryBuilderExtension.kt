package casperix.gdx.graphics

import casperix.gdx.geometry.toVector3
import casperix.math.color.Color4f
import casperix.math.geometry.Line
import casperix.math.geometry.Line3f
import casperix.math.vector.Vector3d
import casperix.math.geometry.Octagon3d
import casperix.math.geometry.Triangle
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector3f
import com.badlogic.gdx.graphics.Mesh
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder

fun GeometryBuilder.createCube(material: Material, dimension: Vector3d = Vector3d.ONE, name: String = "cube", onVertex: ((MeshPartBuilder.VertexInfo) -> Unit)? = null): Model {
	return build(name, material) {
		val octagon = Octagon3d(Vector3d.ZERO, dimension).convert {
			val info = MeshPartBuilder.VertexInfo()
			info.setPos(it.toVector3())
			onVertex?.invoke(info)
			info
		}
		addOctagon(it, octagon)
	}
}

fun MeshPartBuilder.addLineTangent(triangle: Line<VertexWithTangentSpace>) {
	val indices = triangle.getVertices().map { vertex(it) }
	index(indices[0], indices[1])
}

@Deprecated(message = "Use addLines")
fun MeshPartBuilder.addLine(line: Line<MeshPartBuilder.VertexInfo>) {
	val indices = line.getVertices().map { vertex(it) }
	index(indices[0], indices[1])
}

@Deprecated(message = "Use addTriangles")
fun MeshPartBuilder.addTriangle(triangle: Triangle<MeshPartBuilder.VertexInfo>) {
	val indices = triangle.getVertices().map { vertex(it) }
	index(indices[0], indices[1], indices[2])
}

fun MeshPartBuilder.addTriangles(vararg triangles: Triangle<MeshPartBuilder.VertexInfo>) {
	triangles.forEach {
		addTriangle(it)
	}
}

fun MeshPartBuilder.addLines(vararg lines: Line<MeshPartBuilder.VertexInfo>) {
	lines.forEach {
		addLine(it)
	}
}

fun GeometryBuilder.createNormals(mesh: Mesh, scale: Float = 2f): Model {
	return build(vertexAttributes = createAttributes(VertexAttributes.Usage.Position or VertexAttributes.Usage.ColorUnpacked), geometryType = GeometryBuilder.GeometryType.LINES) {
		(0 until mesh.numVertices).forEach { vertexId ->
			val vertex = getVertex(mesh, vertexId)
			val normal = Line3f(vertex.position, vertex.position + vertex.normal * scale).convert {
				MeshPartBuilder.VertexInfo().setPos(it.x, it.y, it.z).setCol(0f, 0f, 1f, 1f)
			}
			val binormal = Line3f(vertex.position, vertex.position + vertex.binormal * scale).convert {
				MeshPartBuilder.VertexInfo().setPos(it.x, it.y, it.z).setCol(0f, 1f, 0f, 1f)
			}
			val tangent = Line3f(vertex.position, vertex.position + vertex.tangent * scale).convert {
				MeshPartBuilder.VertexInfo().setPos(it.x, it.y, it.z).setCol(1f, 0f, 0f, 1f)
			}
			addLine(it, normal)
			addLine(it, binormal)
			addLine(it, tangent)
		}
	}

}


fun getVertex(mesh: Mesh, vertexId: Int): VertexWithTangentSpace {
	val vertexOffset = vertexId * mesh.vertexSize / 4
	val verticesBuffer = mesh.verticesBuffer

	if (vertexOffset != 18) throw Error("It is not VertexWithTangentSpace")

	val position = Vector3f(
		verticesBuffer[vertexOffset + 0],
		verticesBuffer[vertexOffset + 1],
		verticesBuffer[vertexOffset + 2],
	)
	val color = Color4f(
		verticesBuffer[vertexOffset + 3],
		verticesBuffer[vertexOffset + 4],
		verticesBuffer[vertexOffset + 5],
		verticesBuffer[vertexOffset + 6],
	)
	val normal = Vector3f(
		verticesBuffer[vertexOffset + 7],
		verticesBuffer[vertexOffset + 8],
		verticesBuffer[vertexOffset + 9],
	)
	val binormal = Vector3f(
		verticesBuffer[vertexOffset + 10],
		verticesBuffer[vertexOffset + 11],
		verticesBuffer[vertexOffset + 12],
	)
	val tangent = Vector3f(
		verticesBuffer[vertexOffset + 13],
		verticesBuffer[vertexOffset + 14],
		verticesBuffer[vertexOffset + 15],
	)
	val texCoord = Vector2f(
		verticesBuffer[vertexOffset + 16],
		verticesBuffer[vertexOffset + 17],
	)

	return VertexWithTangentSpace(position, texCoord, color, normal, binormal, tangent)
}
