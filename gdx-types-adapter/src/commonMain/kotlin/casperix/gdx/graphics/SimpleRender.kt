package casperix.gdx.graphics

import casperix.math.vector.Vector2d
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL30
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.NinePatch
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch
import com.badlogic.gdx.graphics.g2d.TextureRegion
import com.esotericsoftware.spine.Skeleton
import com.esotericsoftware.spine.SkeletonRenderer

class SimpleRender(var name:String = "default", size:Int = 1000) {
	val camera = OrthographicCamera()

	val polygonBatch = PolygonSpriteBatch(size)
	val skeletonRender = SkeletonRenderer()

	init {
		skeletonRender.setPremultipliedAlpha(false) // PMA results in correct blending without outlines.

	}

	fun begin(color: Color?) {
		if (color != null) {
			Gdx.gl.glClear(GL30.GL_COLOR_BUFFER_BIT)
			Gdx.gl.glClearColor(color.r, color.g, color.b, color.a)
		}
		camera.update()
		polygonBatch.getProjectionMatrix().set(camera.combined)
		polygonBatch.begin()
	}

	fun end() {
		polygonBatch.end()
	}

	fun setColor(color: Color) {
		if (color != polygonBatch.color) {
			polygonBatch.end()
			polygonBatch.begin()
			polygonBatch.color = color
		}
	}

	fun draw(skeleton: Skeleton, color: Color = Color.WHITE) {
		setColor(color)
		skeletonRender.draw(polygonBatch, skeleton)
	}

	fun draw(textureRegion: TextureRegion, position: Vector2d, size: Vector2d, color: Color = Color.WHITE, rotation:Double = 0.0) {
		setColor(color)
		val sx = (size.x / textureRegion.regionWidth).toFloat()
		val sy = (size.y / textureRegion.regionHeight).toFloat()
		val width = textureRegion.regionWidth.toFloat()
		val height = textureRegion.regionHeight.toFloat()
		polygonBatch.draw(textureRegion, position.x.toFloat(), position.y.toFloat(), 0f, 0f, width, height, sx, sy, rotation.toFloat())
	}

	fun draw(texture: Texture, position: Vector2d, size: Vector2d, color: Color = Color.WHITE, rotation:Double = 0.0) {
		draw(TextureRegion(texture, texture.width, texture.height), position, size, color, rotation)
	}

	fun draw(ninePatch: NinePatch, position: Vector2d, size:Vector2d, color: Color = Color.WHITE) {
		setColor(color)
		ninePatch.draw(polygonBatch, position.x.toFloat(), position.y.toFloat(), size.x.toFloat(), size.y.toFloat())
	}

	fun drawByCenter(texture: Texture, position: Vector2d, size:Vector2d, color: Color = Color.WHITE, rotation:Double = 0.0) {
		draw(texture, position - size / 2.0, size, color, rotation)
	}

	fun resize(width: Int, height: Int) {
		camera.setToOrtho(false, width.toFloat(), height.toFloat())
	}
}