package casperix.gdx.graphics

import casperix.math.color.Color4f
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector3f

data class VertexWithTangentSpace(val position: Vector3f, val texCoord: Vector2f, val color: Color4f, val normal: Vector3f, val binormal: Vector3f, val tangent: Vector3f)