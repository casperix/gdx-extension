package casperix.gdx.graphics

import casperix.math.axis_aligned.Box2i
import casperix.math.color.Color4f
import casperix.math.geometry.Quad
import casperix.math.geometry.Triangle
import casperix.math.geometry.toQuad2f
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector2i
import casperix.math.vector.Vector3f
import casperix.math.vector.toQuad
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder

private val template = Box2i(Vector2i.ZERO, Vector2i.ONE).toQuad().toQuad2f()

fun MeshPartBuilder.vertex(tangentVertex: VertexWithTangentSpace): Short {
	val floatColor = tangentVertex.color.toColor4f()
	return vertex(
		tangentVertex.position.x, tangentVertex.position.y, tangentVertex.position.z,
		floatColor.red, floatColor.green, floatColor.blue, floatColor.alpha,
		tangentVertex.normal.x, tangentVertex.normal.y, tangentVertex.normal.z,
		tangentVertex.binormal.x, tangentVertex.binormal.y, tangentVertex.binormal.z,
		tangentVertex.tangent.x, tangentVertex.tangent.y, tangentVertex.tangent.z,
		tangentVertex.texCoord.x, tangentVertex.texCoord.y,
	)
}

fun MeshPartBuilder.addQuad(quad: Quad<MeshPartBuilder.VertexInfo>) {
	val indices = quad.getVertices().map { vertex(it) }
	index(indices[0], indices[1], indices[2], indices[0], indices[2], indices[3])
}

fun MeshPartBuilder.addTriangleTangent(triangle: Triangle<VertexWithTangentSpace>) {
	val indices = triangle.getVertices().map { vertex(it) }
	index(indices[0], indices[1], indices[2])
}

fun MeshPartBuilder.addQuadTangent(quad: Quad<VertexWithTangentSpace>) {
	val indices = quad.getVertices().map { vertex(it) }
	index(indices[0], indices[1], indices[2], indices[0], indices[2], indices[3])
}

fun MeshPartBuilder.addPlaneTangent(size:Vector2f) {
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f((basis.x - 0.5f) * size.x, (basis.y - 0.5f) * size.y, 0f),
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(0f, 0f, 1f),
			Vector3f(0f, 1f, 0f),
			Vector3f(1f, 0f, 0f),
		)
	})
}

fun MeshPartBuilder.addCube(size: Vector3f) {
	//Z+
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f((basis.x - 0.5f), (basis.y - 0.5f), 0.5f) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(0f, 0f, 1f),
			Vector3f(0f, 1f, 0f),
			Vector3f(1f, 0f, 0f),
		)
	})
	//Z-
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f((basis.x - 0.5f), -(basis.y - 0.5f), -0.5f) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(0f, 0f, -1f),
			Vector3f(0f, -1f, 0f),
			Vector3f(1f, 0f, 0f),
		)
	})
	//Y+
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f(basis.x - 0.5f, 0.5f, -(basis.y - 0.5f)) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(0f, 1f, 0f),
			Vector3f(0f, 0f, -1f),
			Vector3f(1f, 0f, 0f),
		)
	})
	//Y-
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f(basis.x - 0.5f, -0.5f, basis.y - 0.5f) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(0f, -1f, 0f),
			Vector3f(0f, 0f, 1f),
			Vector3f(1f, 0f, 0f),
		)
	})
	//X+
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f(0.5f, basis.x - 0.5f, basis.y - 0.5f) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(1f, 0f, 0f),
			Vector3f(0f, 0f, 1f),
			Vector3f(0f, 1f, 0f),
		)
	})
	//X-
	addQuadTangent(template.convert { basis ->
		VertexWithTangentSpace(
			Vector3f(-0.5f, basis.x - 0.5f, 0.5f - basis.y) * size,
			Vector2f(basis.x, 1f - basis.y),
			Color4f(1f, 1f, 1f, 1f),
			Vector3f(-1f, 0f, 0f),
			Vector3f(0f, 0f, -1f),
			Vector3f(0f, 1f, 0f),
		)
	})
}