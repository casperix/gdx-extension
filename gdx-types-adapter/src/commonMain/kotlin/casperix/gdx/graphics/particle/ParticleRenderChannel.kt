package casperix.gdx.graphics.particle

import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g3d.ModelBatch
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute
import com.badlogic.gdx.graphics.g3d.attributes.DepthTestAttribute
import com.badlogic.gdx.graphics.g3d.particles.ParticleShader
import com.badlogic.gdx.graphics.g3d.particles.ParticleSystem
import com.badlogic.gdx.graphics.g3d.particles.batches.BillboardParticleBatch

class ParticleRenderChannel {
	val particleSystem = ParticleSystem()
	val modelBatch = ModelBatch()
	val pointSpriteBatch = BillboardParticleBatch(ParticleShader.AlignMode.Screen, true, 100, BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA, 1f), DepthTestAttribute(GL20.GL_ALWAYS, false))

	init {
		particleSystem.add(pointSpriteBatch)
	}
	
	fun update( tick:Float) {
		particleSystem.update(tick)
	}

	fun render(camera: Camera) {
		pointSpriteBatch.setCamera(camera)
		modelBatch.begin(camera)

		particleSystem.begin()
		particleSystem.draw()
		particleSystem.end()

		modelBatch.render(particleSystem)
		modelBatch.end()
	}
}