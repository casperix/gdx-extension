package casperix.gdx.graphics.particle

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffect
import com.badlogic.gdx.graphics.g3d.particles.ParticleEffectLoader

class ParticleSystemManager {
	private val assets = AssetManager()
	private val channels = mutableMapOf<ParticleEffect, ParticleRenderChannel>()

	fun load(fileNameList:Set<String>):Map<String, ParticleEffect> {
		val channelByName = mutableMapOf<String, ParticleRenderChannel>()

		fileNameList.forEach { effectFileName ->
			val channel = ParticleRenderChannel()
			channelByName[effectFileName] = channel

			val loadParam = ParticleEffectLoader.ParticleEffectLoadParameter(channel.particleSystem.getBatches())
			assets.load(effectFileName, ParticleEffect::class.java, loadParam)
		}
		assets.finishLoading()

		val effects = fileNameList.map {
			val asset:ParticleEffect = assets[it]
			Pair(it, asset)
		}.toMap()

		effects.forEach {
			val channel = channelByName[it.key]
			if (channel != null) {
				channels[it.value] = channel
			}
		}

		return effects
	}

	fun destroyAll() {
		channels.forEach {(_, channel)->
			channel.particleSystem.removeAll()
		}
	}


	fun create(template: ParticleEffect):ParticleEffect {
		val channel = channels[template]
				?: throw Error("First you must load effect (use ParticleSystemManager:load)")

		val effect = template.copy()
		effect.init()
		effect.start()

		channel.particleSystem.add(effect)
		return effect
	}

	fun destroy(effect: ParticleEffect) {
		channels.forEach {
			it.value.particleSystem.remove(effect)
		}
	}

	fun update(tick:Float) {
		channels.forEach {
			it.value.update( tick)
		}
	}

	fun render(camera:Camera) {
		channels.forEach {
			it.value.render(camera)
		}
	}
}