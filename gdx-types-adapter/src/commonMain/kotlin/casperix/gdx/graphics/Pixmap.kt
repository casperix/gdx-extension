package casperix.gdx.graphics

import casperix.map2d.IntMap2D
import com.badlogic.gdx.graphics.Pixmap


fun createPixmap(source: IntMap2D): Pixmap {
	return source.toPixmap()
}

fun IntMap2D.toPixmap(): Pixmap {
	val pixmap = Pixmap(width, height, Pixmap.Format.RGBA8888)
	pixmap.pixels.asIntBuffer().put(array)
	return pixmap
}