package casperix.gdx.graphics

import casperix.map2d.IndexedMap2D.Companion.indexFromPosition
import casperix.math.axis_aligned.Box2i
import casperix.math.geometry.Line
import casperix.math.geometry.Octagon
import casperix.math.geometry.Quad
import casperix.math.geometry.Triangle
import casperix.math.vector.Vector2i
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.VertexAttribute
import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.graphics.g3d.utils.shapebuilders.BoxShapeBuilder
import com.badlogic.gdx.graphics.glutils.ShaderProgram

object GeometryBuilder {
	/**
	 * 	Task, describe single mesh
	 */
	data class Task(val name: String = "", val material: Material = Material(), val geometryType: GeometryType = GeometryType.TRIANGLES, val vertexAttributes: VertexAttributes = defaultVertexAttributes, val builder: (MeshPartBuilder) -> Unit)

	enum class GeometryType {
		POINTS,
		LINES,
		TRIANGLES,
//		LINE_LOOP,
//		LINE_STRIP,
//		TRIANGLE_STRIP,
//		TRIANGLE_FAN,
	}

	val defaultVertexAttributes = createAttributes(VertexAttributes.Usage.Normal or VertexAttributes.Usage.Position or VertexAttributes.Usage.TextureCoordinates or VertexAttributes.Usage.ColorUnpacked)

	/**
	 * 	Model with one mesh
	 * 	Example:
	 *	<pre>
	 *	    val mesh = GeometryBuilder.build {
	 *			ConeShapeBuilder.build(it, 1f, 1f, 1f, 10)
	 *		}
	 *		renderManager.add(ModelInstance(mesh))
	 *	</pre>
	 *
	 */
	fun build(partName: String = "", material: Material = Material(), geometryType: GeometryType = GeometryType.TRIANGLES, vertexAttributes: VertexAttributes = defaultVertexAttributes, builder: (MeshPartBuilder) -> Unit): Model {
		return build(Task(partName, material, geometryType, vertexAttributes, builder))
	}

	/**
	 * 	Model with few meshes, described by tasks
	 */
	fun build(tasks: Collection<Task>): Model {
		return build(*tasks.toTypedArray())
	}

	fun build(vararg tasks: Task): Model {
		val modelBuilder = ModelBuilderWithFix()
		modelBuilder.begin()

		tasks.forEach { task ->
			task.run {
				val figureNative = when (geometryType) {
					GeometryType.POINTS -> GL20.GL_POINTS
					GeometryType.LINES -> GL20.GL_LINES
					GeometryType.TRIANGLES -> GL20.GL_TRIANGLES
//					GeometryType.LINE_LOOP -> GL20.GL_LINE_LOOP
//					GeometryType.LINE_STRIP -> GL20.GL_LINE_STRIP
//					GeometryType.TRIANGLE_STRIP -> GL20.GL_TRIANGLE_STRIP
//					GeometryType.TRIANGLE_FAN -> GL20.GL_TRIANGLE_FAN
				}

				val partBuilder = modelBuilder.part(name, figureNative, vertexAttributes, material)
				builder(partBuilder)
			}
		}

		return modelBuilder.end()
	}
	fun createAttributes(usage: Int): VertexAttributes {
		return createAttributes(usage.toLong())
	}

	fun createAttributes(usage: Long): VertexAttributes {
		val attributes = mutableListOf<VertexAttribute>()
		if (usage and VertexAttributes.Usage.Position.toLong() == VertexAttributes.Usage.Position.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.Position, 3, ShaderProgram.POSITION_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.ColorUnpacked.toLong() == VertexAttributes.Usage.ColorUnpacked.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.ColorUnpacked, 4, ShaderProgram.COLOR_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.ColorPacked.toLong() == VertexAttributes.Usage.ColorPacked.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.ColorPacked, 4, ShaderProgram.COLOR_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.Normal.toLong() == VertexAttributes.Usage.Normal.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.Normal, 3, ShaderProgram.NORMAL_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.BiNormal.toLong() == VertexAttributes.Usage.BiNormal.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.BiNormal, 3, ShaderProgram.BINORMAL_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.Tangent.toLong() == VertexAttributes.Usage.Tangent.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.Tangent, 3, ShaderProgram.TANGENT_ATTRIBUTE))
		if (usage and VertexAttributes.Usage.TextureCoordinates.toLong() == VertexAttributes.Usage.TextureCoordinates.toLong()) attributes.add(VertexAttribute(VertexAttributes.Usage.TextureCoordinates, 2, ShaderProgram.TEXCOORD_ATTRIBUTE + "0"))
		return VertexAttributes(*attributes.toTypedArray())
	}

	fun addOctagon(builder: MeshPartBuilder, vertices: Octagon<MeshPartBuilder.VertexInfo>) {
		BoxShapeBuilder.build(builder, vertices.v2, vertices.v0, vertices.v6, vertices.v4, vertices.v3, vertices.v1, vertices.v7, vertices.v5)
	}

	fun addQuad(builder: MeshPartBuilder, quad: Quad<MeshPartBuilder.VertexInfo>) {
		val index = builder.lastIndex() + 1
		quad.getVertices().forEach { info ->
			builder.vertex(info)
		}
		when (builder.primitiveType) {
			GL20.GL_TRIANGLES -> builder.index((index + 0).toShort(), (index + 1).toShort(), (index + 2).toShort(), (index + 0).toShort(), (index + 2).toShort(), (index + 3).toShort())
			GL20.GL_LINES -> builder.index((index + 0).toShort(), (index + 1).toShort(), (index + 1).toShort(), (index + 2).toShort(), (index + 2).toShort(), (index + 3).toShort(), (index + 3).toShort(), (index + 0).toShort())
			else -> throw Error("Cant make quad for primitive: ${builder.primitiveType}")
		}

	}

	fun addTriangle(builder: MeshPartBuilder, triangle: Triangle<MeshPartBuilder.VertexInfo>) {
		val index = builder.lastIndex() + 1
		triangle.getVertices().forEach { position ->
			builder.vertex(position)
		}
		when (builder.primitiveType) {
			GL20.GL_TRIANGLES -> builder.index((index + 0).toShort(), (index + 1).toShort(), (index + 2).toShort())
			GL20.GL_LINES -> builder.index((index + 0).toShort(), (index + 1).toShort(), (index + 1).toShort(), (index + 2).toShort(), (index + 2).toShort(), (index + 0).toShort())
			else -> throw Error("Cant make triangle for primitive: ${builder.primitiveType}")
		}
	}

	fun addLine(builder: MeshPartBuilder, line: Line<MeshPartBuilder.VertexInfo>) {
		val index = builder.lastIndex() + 1
		line.getVertices().forEach { position ->
			builder.vertex(position)
		}
		when (builder.primitiveType) {
			GL20.GL_LINES -> builder.index((index + 0).toShort(), (index + 1).toShort())
			else -> throw Error("Cant make line for primitive: ${builder.primitiveType}")
		}

	}

	fun buildQuad(quad: Quad<MeshPartBuilder.VertexInfo>, material: Material, name: String = "mesh"): Model {
		return build(name, material) { builder ->
			addQuad(builder, quad)
		}
	}

	fun buildGrid(getVertex: (Vector2i) -> MeshPartBuilder.VertexInfo, gridArea: Box2i, wireframe: Boolean, material: Material): Model {
		if (gridArea.volume > 65536)
			throw Error("Can't generate mesh more than 65536 vertices (request: ${gridArea.volume})")

		val facesArea = Box2i(gridArea.min, gridArea.max - Vector2i.ONE)
		val geometryType = if (wireframe) GeometryType.LINES else GeometryType.TRIANGLES

		return build("mesh", material, geometryType) { builder ->
			builder.ensureVertices(gridArea.volume)
			gridArea.iterator().forEach { position ->
				builder.vertex(getVertex(position))
			}

			val indicesPerCell = if (wireframe) 4 else 6
			builder.ensureIndices(facesArea.volume * indicesPerCell)
			facesArea.iterator().forEach {
				val v00 = indexFromPosition(gridArea.dimension.x, it - facesArea.min).toShort()
				val v10 = indexFromPosition(gridArea.dimension.x, it - facesArea.min + Vector2i.X).toShort()
				val v11 = indexFromPosition(gridArea.dimension.x, it - facesArea.min + Vector2i.XY).toShort()
				val v01 = indexFromPosition(gridArea.dimension.x, it - facesArea.min + Vector2i.Y).toShort()

				if (wireframe) {
					builder.index(v00, v10, v00, v01)
				} else {
					builder.index(v00, v10, v11, v00, v11, v01)
				}
			}
		}
	}
}