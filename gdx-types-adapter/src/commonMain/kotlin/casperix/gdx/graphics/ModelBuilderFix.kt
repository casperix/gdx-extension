package casperix.gdx.graphics

import com.badlogic.gdx.graphics.VertexAttributes
import com.badlogic.gdx.graphics.g3d.Material
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder

class ModelBuilderWithFix : ModelBuilder() {
	override fun part(id: String?, primitiveType: Int, attributes: VertexAttributes?, material: Material?): MeshPartBuilder {
		val builder = getBuilder(attributes!!)
		part(builder!!.part(id, primitiveType), material)
		return builder
	}

	private fun getBuilder(attributes: VertexAttributes): MeshBuilder? {
		val f = ModelBuilder::class.java.getDeclaredField("builders")
		f.isAccessible = true
		val builders = f.get(this) as com.badlogic.gdx.utils.Array<MeshBuilder>

		for (mb in builders) if (mb.attributes == attributes && (mb.lastIndex() >= 0 && mb.lastIndex() < Short.MAX_VALUE / 2)) return mb
		val result = MeshBuilder()
		result.begin(attributes)
		builders.add(result)
		return result
	}
}