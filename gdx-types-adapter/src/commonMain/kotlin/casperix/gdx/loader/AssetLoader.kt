package casperix.gdx.loader

import com.badlogic.gdx.assets.loaders.resolvers.LocalFileHandleResolver
import com.badlogic.gdx.graphics.g3d.Model
import com.badlogic.gdx.graphics.g3d.loader.G3dModelLoader
import com.badlogic.gdx.graphics.g3d.loader.ObjLoader
import com.badlogic.gdx.utils.JsonReader
import com.badlogic.gdx.utils.UBJsonReader

object AssetLoader {
	private val resolver = LocalFileHandleResolver()
	private val modelLoaderMap = mapOf(
		Pair(".g3db", G3dModelLoader(UBJsonReader(), resolver)),
		Pair(".g3dj", G3dModelLoader(JsonReader(), resolver)),
		Pair(".obj", ObjLoader()),
	)

	fun loadModel(name: String): Model {
		val model = modelLoaderMap.firstNotNullOfOrNull { (extension, loader) ->
			if (name.endsWith(extension)) {
				val handle = resolver.resolve(name)
				loader.loadModel(handle)
			} else null
		}
		return model ?: throw Error("Unsupported file extension $name")
	}
}
