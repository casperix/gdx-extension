package casperix.gdx.ui

import casperix.gdx.geometry.toVector2
import casperix.math.vector.Vector2d
import casperix.signals.concrete.StorageSignal
import casperix.signals.switch
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Action
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Group
import com.badlogic.gdx.scenes.scene2d.actions.Actions
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.Cell
import com.badlogic.gdx.scenes.scene2d.ui.Container
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener
import com.badlogic.gdx.scenes.scene2d.utils.Drawable


fun MeshPartBuilder.quad(v0: Short, v1: Short, v2: Short, v3: Short) {
	index(v0, v1, v2, v0, v2, v3)
}

fun Actor.createTimer(duration: Float, callback: () -> Unit) {
	val action = object : Action() {
		var frameId = Gdx.graphics.frameId
		override fun act(delta: Float): Boolean {
			//	block loop repeat
			if (frameId == Gdx.graphics.frameId) return false

			callback()
			createTimer(duration, callback)
			return true
		}
	}

	addAction(Actions.delay(duration, action))
}

fun Actor.getActualWidth(): Float {
//	if (this !is Layout)
	return width
//	return prefWidth
}

fun Actor.getActualHeight(): Float {
//	if (this !is Layout)
	return height
//	return prefHeight
}

fun Actor.statePosition(): Vector2? {
	return localToStageCoordinates(Vector2(0f, 0f))
}

fun Actor.setSize(value: Vector2d): Actor {
	setSize(value.x.toFloat(), value.y.toFloat())
	return this
}

fun Actor.setPosition(value: Vector2d): Actor {
	setPosition(value.x.toFloat(), value.y.toFloat())
	return this
}

fun Cell<*>.size(value: Vector2d): Cell<*> {
	size(value.x.toFloat(), value.y.toFloat())
	return this
}

fun Cell<*>.pad(value: ItemGap): Cell<*> {
	pad(value.top.toFloat(), value.left.toFloat(), value.bottom.toFloat(), value.right.toFloat())
	return this
}

fun Cell<*>.space(value: ItemGap): Cell<*> {
	space(value.top.toFloat(), value.left.toFloat(), value.bottom.toFloat(), value.right.toFloat())
	return this
}

fun <T : Button> T.addChangeListener(callback: (self: T) -> Unit) {
	val self = this
	this.addListener(object : ChangeListener() {
		override fun changed(event: ChangeEvent, actor: Actor) {
			callback(self)
		}
	})
}

fun <T : Button> T.addClickListener(callback: (self: T) -> Unit) {
	val self = this
	this.addListener(object : ChangeListener() {
		override fun changed(event: ChangeEvent, actor: Actor) {
			if (isChecked) {
				isChecked = false
				callback(self)
			}
		}
	})
}

fun Button.addSignal(signal: StorageSignal<Boolean>) {
	isChecked = signal.value

	addChangeListener {
		signal.switch()
	}
}

fun <T : Actor> createAligner(parent: Group, source: T, align: Int, prefSize: Vector2d? = null): Container<T> {
	val container = Container(source)

	container.setFillParent(true)
	container.align(align)

	parent.addActor(container)
	if (prefSize != null) {
		container.prefSize(prefSize.x.toFloat(), prefSize.y.toFloat())
	}
	return container
}

fun Drawable.setMinSize(x: Float, y: Float): Drawable {
	this.minWidth = x
	this.minHeight = y
	return this
}

fun Drawable.setMinSize(value: Vector2): Drawable {
	this.minWidth = value.x
	this.minHeight = value.y
	return this
}

fun Drawable.setMinSize(value: Vector2d): Drawable {
	return setMinSize(value.toVector2())
}

