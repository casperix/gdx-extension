package casperix.gdx.ui

import casperix.math.vector.Vector2d
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.*
import com.badlogic.gdx.scenes.scene2d.utils.Drawable

data class ItemGap(val top: Int, val left: Int, val bottom: Int, val right: Int) {
	constructor(gap: Int) : this(gap, gap, gap, gap)
}

data class ToolBarOptions(val isVertical: Boolean = true, val cellSize: Vector2d = Vector2d(100.0, 50.0), val cellSpace: ItemGap = ItemGap(5), val cellPad: ItemGap = ItemGap(5), val buttonMinCheckAmount: Int? = null)


/**
 * 	Simplify create group with similar elements  (buttons in main menu)
 * 	However, you are not limited by the type and size of the elements.
 */
class ToolBar(val style: ToolBarStyle) : Table() {

	constructor(skin: Skin)
			: this(skin.get("default", ToolBarStyle::class.java))

	class ToolBarStyle(val back: Drawable?)

	var options: ToolBarOptions = ToolBarOptions()
		set(value) {
			field = value; update()
		}

	private var buttonGroup: ButtonGroup<Button>? = null

	init {
		update()

		if (style.back != null) {
			background(style.back)
		}
	}

	private fun update() {
		defaults().size(options.cellSize).pad(options.cellPad).space(options.cellSpace)

		buttonGroup?.clear()
		options.buttonMinCheckAmount?.let {
			val buttonGroup = ButtonGroup<Button>()
			buttonGroup.setMinCheckCount(it)
			this.buttonGroup = buttonGroup
		}

		val cells = children.toList()

		clear()

		cells.forEach {
			add(it)
		}
	}

	override fun <T : Actor> add(actor: T): Cell<T> {
		val result = super.add(actor)
		if (!children.isEmpty && options.isVertical) {
			super.row()
		}
		if (actor is Tab<*, *> && actor.switcher is Button) {
			buttonGroup?.add(actor.switcher)
		}
		if (actor is Button) {
			buttonGroup?.add(actor)
		}
		return result
	}
}