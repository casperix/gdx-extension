package casperix.gdx.ui

import casperix.misc.DisposableHolder
import casperix.signals.collection.observableListOf
import casperix.signals.then
import casperix.math.vector.Vector2i
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.scenes.scene2d.ui.Skin
import com.badlogic.gdx.scenes.scene2d.ui.Table
import com.badlogic.gdx.scenes.scene2d.ui.TextButton
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup

data class MenuItem(val actor: Actor, val size: Vector2i? = null)

class UIMenu(stage: Stage, skin: Skin, name: String) : DisposableHolder() {
	val table = Table()
	val group = VerticalGroup()
	val items = observableListOf<MenuItem>()

	val btnMenu = TextButton(name, skin)


	init {
		stage.addActor(table)

		btnMenu.addChangeListener {
			rebuild()
		}
		items.then(components, { rebuild() }, { rebuild() })
	}

	fun clear() {
		items.clear()
	}

	fun add(actor: Actor, size: Vector2i? = null) {
		items.add(MenuItem(actor, size))
	}

	fun rebuild() {
		table.clear()
		table.add(btnMenu)
		if (btnMenu.isChecked) {
			group.clear()
			table.add(group)
			items.forEach { item ->
				group.addActor(item.actor)
			}
		}
	}

}