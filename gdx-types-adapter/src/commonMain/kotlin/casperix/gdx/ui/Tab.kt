package casperix.gdx.ui

import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.scenes.scene2d.Actor
import com.badlogic.gdx.scenes.scene2d.ui.Button
import com.badlogic.gdx.scenes.scene2d.ui.WidgetGroup
import com.badlogic.gdx.utils.Align

class TabOptions(val align: Int = Align.left, val tabPad: Float = 5f, val allowChangeZIndex:Boolean = true)

/**
 * 	show or hide content around self element
 *
 * 	You can create simple Tab with Button (Tab.build)
 * 	or create yourself custom switcher (and manually call show / hide)
 */
class Tab<Switcher:Actor, Content:Actor>(val switcher:Switcher, val content: Content, val options: TabOptions = TabOptions()) : WidgetGroup() {
	val logic = TabLogic(switcher,  content, this)

	init {
		addActor(switcher)
		logic.onShow.then {
			if (it) {
				onShow()
			} else {
				onHide()
			}
		}
	}

	override fun layout() {
		switcher.setSize(getActualWidth(), getActualHeight())
		super.layout()
	}

	private fun onShow() {
		if (options.allowChangeZIndex) {
			setZIndex(Int.MAX_VALUE)
		}

		val position = Vector2(0f, 0f)//(target.statePosition() ?: Vector2(0f, 0f))//.sub((parent.statePosition() ?: Vector2(0f, 0f)))

		if (Align.isLeft(options.align)) {
			content.x = position.x - content.getActualWidth() - options.tabPad
		} else if (Align.isRight(options.align)) {
			content.x = position.x + getActualWidth() + options.tabPad
		} else if (Align.isCenterHorizontal(options.align)) {
			content.x = position.x + (getActualWidth() - content.getActualWidth()) / 2f
		}
		if (Align.isBottom(options.align)) {
			content.y = position.y - content.getActualHeight() - options.tabPad
		} else if (Align.isTop(options.align)) {
			content.y = position.y + getActualHeight() + options.tabPad
		} else if (Align.isCenterVertical(options.align)) {
			content.y = position.y + (getActualHeight() - content.getActualHeight()) / 2f
		}

		addActor(content)
	}

	private fun onHide() {
		content.remove()
	}
}