package casperix.gdx.renderer.camera

import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import com.badlogic.gdx.graphics.Camera
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.math.Matrix4
import com.badlogic.gdx.math.Vector3


fun Camera.project2d(worldPosition: Vector3d, screenYDown:Boolean = true): Vector2d {
	val world3 = Vector3(worldPosition.x.toFloat(), worldPosition.y.toFloat(), worldPosition.z.toFloat())
	val view3 = project(world3)
	val x = view3.x
	val y = if (screenYDown) {
		viewportHeight - 1 - view3.y
	} else {
		view3.y
	}
	return Vector2d(x.toDouble(), y.toDouble())
}

fun OrthographicCamera.update(updateFrustum: Boolean, yDown:Boolean) {
	val tmp = Vector3()

	val yFactor = if (yDown) 1f else -1f
	projection.setToOrtho(
		zoom * -viewportWidth / 2, zoom * (viewportWidth / 2), yFactor * zoom * -(viewportHeight / 2),
		yFactor * zoom * viewportHeight / 2, near, far
	)
	view.setToLookAt(position, tmp.set(position).add(direction), up)
	combined.set(projection)
	Matrix4.mul(combined.`val`, view.`val`)
	if (updateFrustum) {
		invProjectionView.set(combined)
		Matrix4.inv(invProjectionView.`val`)
		frustum.update(invProjectionView)
	}

}