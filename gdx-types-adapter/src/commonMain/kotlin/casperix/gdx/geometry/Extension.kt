package casperix.gdx.geometry

import casperix.math.Quaterniond
import casperix.math.Quaternionf
import casperix.math.Transformf
import casperix.math.axis_aligned.Box2d
import casperix.math.axis_aligned.Box2f
import casperix.math.color.Color
import casperix.math.color.Color4d
import casperix.math.color.Color4f
import casperix.math.geometry.Line3d
import casperix.math.geometry.Plane3d
import casperix.math.geometry.Quad
import casperix.math.geometry.direction
import casperix.math.matrix.Matrix3d
import casperix.math.matrix.Matrix4d
import casperix.math.matrix.Matrix4f
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector2f
import casperix.math.vector.Vector3d
import casperix.math.vector.Vector3f
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder
import com.badlogic.gdx.graphics.g3d.utils.MeshPartBuilder.VertexInfo
import com.badlogic.gdx.math.*
import com.badlogic.gdx.math.collision.Ray

fun Rectangle.toBox2f(): Box2f {
	return Box2f(Vector2f(x, y), Vector2f(x + width, y + height))
}

fun Rectangle.toBox2d(): Box2d {
	return Box2d(Vector2f(x, y).toVector2d(), Vector2f(x + width, y + height).toVector2d())
}

fun Quad<Vector2d>.toPolygon(): Polygon {
	val p = Polygon()
	val vertices = mutableListOf<Float>()
	for (index in 0 until getVertexAmount()) {
		val vertex = getVertex(index)
		vertices.add(vertex.x.toFloat())
		vertices.add(vertex.y.toFloat())
	}
	return Polygon(vertices.toFloatArray())
}

/**
 * 	VECTOR 2D
 */

fun Vector2.toPrecision(value: Int): String {
	return toVector2f().toPrecision(value)
}

fun Vector2d.toVector2(): Vector2 {
	return Vector2(x.toFloat(), y.toFloat())
}

fun Vector2.toVector2d(): Vector2d {
	return Vector2d(x.toDouble(), y.toDouble())
}

fun Vector2.toVector2f(): Vector2f {
	return Vector2f(x, y)
}

fun Vector2f.toVector2(): Vector2 {
	return Vector2(x, y)
}

fun Vector2.set(value: Vector2f): Vector2 {
	return set(value.x, value.y)
}

fun Vector2.set(value: Vector2d): Vector2 {
	return set(value.x.toFloat(), value.y.toFloat())
}

operator fun Vector2.plus(other: Vector2): Vector2 {
	return Vector2(this.x + other.x, this.y + other.y)
}

operator fun Vector2.minus(other: Vector2): Vector2 {
	return Vector2(this.x - other.x, this.y - other.y)
}

operator fun Vector2.times(other: Float): Vector2 {
	return Vector2(this.x * other, this.y * other)
}

operator fun Vector2.div(other: Float): Vector2 {
	return Vector2(this.x / other, this.y / other)
}

/**
 * 	VECTOR 3D
 */
fun Vector3.toPrecision(value: Int): String {
	return toVector3f().toPrecision(value)
}

fun Vector3d.toVector3(): Vector3 {
	return Vector3(x.toFloat(), y.toFloat(), z.toFloat())
}

fun Vector3.toVector3d(): Vector3d {
	return Vector3d(x.toDouble(), y.toDouble(), z.toDouble())
}

fun Vector3.toVector3f(): Vector3f {
	return Vector3f(x, y, z)
}

fun Vector3f.toVector3(): Vector3 {
	return Vector3(x, y, z)
}

fun Vector3.set(value: Vector3f): Vector3 {
	return set(value.x, value.y, value.z)
}

fun Vector3.set(value: Vector3d): Vector3 {
	return set(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

operator fun Vector3.plus(other: Vector3): Vector3 {
	return Vector3(this.x + other.x, this.y + other.y, this.z + other.z)
}

operator fun Vector3.minus(other: Vector3): Vector3 {
	return Vector3(this.x - other.x, this.y - other.y, this.z - other.z)
}

operator fun Vector3.times(other: Float): Vector3 {
	return Vector3(this.x * other, this.y * other, this.z * other)
}

operator fun Vector3.div(other: Float): Vector3 {
	return Vector3(this.x / other, this.y / other, this.z / other)
}

/**
 * 	QUATERNION
 */

fun Quaternion.toQuaterniond(): Quaterniond {
	return Quaterniond(x.toDouble(), y.toDouble(), z.toDouble(), w.toDouble())
}

fun Quaterniond.toQuaternion(): Quaternion {
	return Quaternion(x.toFloat(), y.toFloat(), z.toFloat(), w.toFloat())
}

fun Quaternion.toQuaternionf(): Quaternionf {
	return Quaternionf(x, y, z, w)
}

fun Quaternionf.toQuaternion(): Quaternion {
	return Quaternion(x, y, z, w)
}

fun Quaternion.set(value: Quaternionf): Quaternion {
	return set(value.x, value.y, value.z, value.w)
}

fun Quaternion.set(value: Quaterniond): Quaternion {
	return set(value.x.toFloat(), value.y.toFloat(), value.z.toFloat(), value.w.toFloat())
}

/**
 * 	MISC
 */

fun Line3d.toRay(): Ray {
	return Ray(v0.toVector3(), direction().toVector3())
}

fun Ray.toLine3d(): Line3d {
	val origin = origin.toVector3d()
	return Line3d(origin, origin + direction.toVector3d())
}

fun Plane3d.toPlane(): Plane {
	return Plane(normal.toVector3(), destToOrigin.toFloat())
}

fun Plane.toPlane3d(): Plane3d {
	return Plane3d(normal.toVector3d(), d.toDouble())
}

fun Box2d.toRectangle(): Rectangle {
	return Rectangle(min.x.toFloat(), min.y.toFloat(), (max.x - min.x).toFloat(), (max.y - min.y).toFloat())
}

fun Transformf.toMatrix4(): Matrix4 {
	return Matrix4(position.toVector3(), rotation.toQuaternion(), scale.toVector3())
}

/**
 * 	MATRIX
 */

fun Matrix4.rotate(quaternion: Quaterniond) {
	this.rotate(quaternion.toQuaternion())
}

fun Matrix4.rotate(quaternion: Quaternionf) {
	this.rotate(quaternion.toQuaternion())
}

fun Matrix4.setToTranslation(value: Vector3d) {
	this.setToTranslation(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.setToTranslation(value: Vector3f) {
	this.setToTranslation(value.x, value.y, value.z)
}

fun Matrix4.translate(value: Vector3d) {
	this.translate(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.translate(value: Vector3f) {
	this.translate(value.x, value.y, value.z)
}

fun Matrix4.scale(value: Vector3d) {
	this.scale(value.x.toFloat(), value.y.toFloat(), value.z.toFloat())
}

fun Matrix4.scale(value: Vector3f) {
	this.scale(value.x, value.y, value.z)
}

fun Matrix4.scale(value: Vector3) {
	this.scale(value.x, value.y, value.z)
}

fun Matrix4.scale(value: Float) {
	this.scale(value, value, value)
}

fun Matrix4.scale(value: Double) {
	this.scale(value.toFloat(), value.toFloat(), value.toFloat())
}

fun Matrix4d.toMatrix4(): Matrix4 {
	return Matrix4(FloatArray(data.size) { data[it].toFloat() })
}

fun Matrix4.toMatrix4d(): Matrix4d {
	return Matrix4d(DoubleArray(`val`.size) { `val`[it].toDouble() })
}

fun Matrix4f.toMatrix4(): Matrix4 {
	return Matrix4(FloatArray(data.size) { data[it] })
}

fun Matrix4.toMatrix4f(): Matrix4f {
	return Matrix4f(FloatArray(`val`.size) { `val`[it] })
}

fun Matrix4.set(value: Matrix4f): Matrix4 {
	return set(value.toMatrix4())
}

fun Matrix4.set(value: Matrix4d): Matrix4 {
	return set(value.toMatrix4())
}


fun VertexInfo.setPos(value: Vector3d): VertexInfo {
	setPos(value.toVector3())
	return this
}

fun VertexInfo.setPos(value: Vector3f): VertexInfo {
	setPos(value.toVector3())
	return this
}

fun VertexInfo.setNor(value: Vector3d): VertexInfo {
	setNor(value.toVector3())
	return this
}

fun VertexInfo.setNor(value: Vector3f): VertexInfo {
	setNor(value.toVector3())
	return this
}

fun VertexInfo.setUV(value: Vector2d): VertexInfo {
	setUV(value.toVector2())
	return this
}

fun VertexInfo.setUV(value: Vector2f): VertexInfo {
	setUV(value.toVector2())
	return this
}

fun VertexInfo.setCol(color: Color): VertexInfo {
	val floatColor = color.toColor4f()
	setCol(floatColor.red, floatColor.green, floatColor.blue, floatColor.alpha)
	return this
}

fun Matrix3d.toAffine2(): Affine2 {
	val transform = Affine2()
	transform.m00 = this[0].toFloat()
	transform.m01 = this[1].toFloat()
	transform.m02 = this[2].toFloat()
	transform.m10 = this[3].toFloat()
	transform.m11 = this[4].toFloat()
	transform.m12 = this[5].toFloat()
	return transform
}

fun Affine2.toMatrix3d(): Matrix3d {
	val transform = Matrix3d(
		doubleArrayOf(
			m00.toDouble(),
			m01.toDouble(),
			m02.toDouble(),
			m10.toDouble(),
			m11.toDouble(),
			m12.toDouble(),
			0.0, 0.0, 1.0,
		)
	)
	return transform
}
