package casperix.gdx.geometry

import casperix.math.Transformf
import casperix.math.geometry.Plane3d
import casperix.math.vector.Vector2d
import casperix.math.vector.Vector3d
import casperix.math.axis_aligned.Axis3i
import casperix.math.axis_aligned.Box2d
import casperix.math.axis_aligned.Box3d
import casperix.math.axis_aligned.Box3f
import casperix.math.geometry.*
import com.badlogic.gdx.math.Intersector
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import com.badlogic.gdx.math.collision.Ray

object Intersection {
	private val tmp = Vector3()

	fun distanceLinePoint(line: Line2d, point:Vector2d):Double {
		return Intersector.distanceSegmentPoint(line.v0.toVector2(), line.v1.toVector2(), point.toVector2()).toDouble()
	}

	fun lineWithPlane(line: Line3d, plane: Plane3d): Vector3d? {
		val result = Vector3()
		val value = Intersector.intersectLinePlane(
				line.v0.x.toFloat(), line.v0.y.toFloat(), line.v0.z.toFloat(),
				line.v1.x.toFloat(), line.v1.y.toFloat(), line.v1.z.toFloat(),
				plane.toPlane(),
				result
		)
		if (value >= 0.0) {
			return result.toVector3d()
		}
		return null
	}

	fun transformedBoxWithBox(aTransform:Transformf, A:Box3f, bTransform: Transformf, B: Box3f):Boolean {
		val maxSize = (A.dimension + B.dimension).length()
		val distanceBetween = (B.center - A.center).length()
		if (distanceBetween >= maxSize / 2.0) return false

		val aMatrix = aTransform.getMatrix()
		val aFaces = Axis3i.values().map { 		A.getSideByDirection(it)}.map { it.convert { aMatrix.transform(it) } }

		val bMatrix = aTransform.getMatrix()
		val bFaces = Axis3i.values().map { 		B.getSideByDirection(it)}.map { it.convert { bMatrix.transform(it) } }

		aFaces.forEach { aFace->
			val a1 = aFace.getFace(0)
			val a2 = aFace.getFace(1)
			bFaces.forEach { bFace->
				val b1 = bFace.getFace(0)
				val b2 = bFace.getFace(1)
				if (triangleWithTrianglef(a1, b1)) return true
				if (triangleWithTrianglef(a2, b1)) return true
				if (triangleWithTrianglef(a1, b2)) return true
				if (triangleWithTrianglef(a2, b2)) return true
			}
		}
		return false
	}

	fun triangleWithTriangle(A: Triangle3d, B: Triangle3d):Boolean {
		if (lineWithTriangle(Line3d(A.v0, A.v1), B)) return true
		if (lineWithTriangle(Line3d(A.v1, A.v2), B)) return true
		if (lineWithTriangle(Line3d(A.v2, A.v0), B)) return true
		if (lineWithTriangle(Line3d(B.v0, B.v1), A)) return true
		if (lineWithTriangle(Line3d(B.v1, B.v2), A)) return true
		if (lineWithTriangle(Line3d(B.v2, B.v0), A)) return true
		return false
	}

	fun triangleWithTrianglef(A: Triangle3f, B: Triangle3f):Boolean {
		if (lineWithTrianglef(Line3f(A.v0, A.v1), B)) return true
		if (lineWithTrianglef(Line3f(A.v1, A.v2), B)) return true
		if (lineWithTrianglef(Line3f(A.v2, A.v0), B)) return true
		if (lineWithTrianglef(Line3f(B.v0, B.v1), A)) return true
		if (lineWithTrianglef(Line3f(B.v1, B.v2), A)) return true
		if (lineWithTrianglef(Line3f(B.v2, B.v0), A)) return true
		return false
	}

	fun lineWithTriangle(line: Line3d, triangle: Triangle3d):Boolean {
		return Intersector.intersectRayTriangle(Ray(line.v0.toVector3(), line.direction().toVector3()), triangle.v0.toVector3(), triangle.v1.toVector3(), triangle.v2.toVector3(), tmp)
	}

	fun lineWithTrianglef(line: Line3f, triangle: Triangle3f):Boolean {
		return Intersector.intersectRayTriangle(Ray(line.v0.toVector3(), line.direction().toVector3()), triangle.v0.toVector3(), triangle.v1.toVector3(), triangle.v2.toVector3(), tmp)
	}

	fun lineWithBox(ray:Line2d, box:Box2d):Boolean {
		return Intersector.intersectSegmentRectangle(ray.v0.x.toFloat(), ray.v0.y.toFloat(), ray.v1.x.toFloat(), ray.v1.y.toFloat(), box.toRectangle())
	}

	fun lineWithLine(A:Line2d, B:Line2d):Vector2d? {
		val target  = Vector2()
		if (!Intersector.intersectSegments(A.v0.x.toFloat(), A.v0.y.toFloat(), A.v1.x.toFloat(), A.v1.y.toFloat(), B.v0.x.toFloat(), B.v0.y.toFloat(), B.v1.x.toFloat(), B.v1.y.toFloat(), target)) return null
		return target.toVector2d()
	}
}