package casperix.gdx.file

import casperix.file.FileReference
import casperix.file.FilePathType
import com.badlogic.gdx.Gdx
import com.badlogic.gdx.files.FileHandle

fun FileReference.toGdxFile(): FileHandle {
	return when (type) {
		FilePathType.CLASSPATH -> Gdx.files.classpath(path)
		FilePathType.EXTERNAL -> Gdx.files.external(path)
		FilePathType.INTERNAL -> Gdx.files.internal(path)
		FilePathType.ABSOLUTE -> Gdx.files.absolute(path)
	}
}